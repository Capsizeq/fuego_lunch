import { Action } from '@ngrx/store';
import { Restaurant } from '../../core/objects/restaurant';
import { LatLngBounds, VisibleRegion } from '@ionic-native/google-maps';

export enum RestaurantActionTypes {
    LOAD_RESTAURANT = '[RESTAURANT] Load Restaurant Action',
    LOAD_RESTUARANT_SUCCESS = '[RESTAURANT] Load Restaurant Success',
    LOAD_RESTAURANT_FAILURE = '[RESTAURANT] Load Restaurant Failure',
    LOAD_NEARBY_RESTAURANTS = '[RESTAURANT] Load NearBy Restuarants Action',
    LOAD_NEARBY_RESTAURANTS_SUCCESS = '[RESTAURANT] Load NearBy Restuarants Success',
    LOAD_NEARBY_RESTAURANTS_FAILURE = '[RESTAURANT] Load NearBy Restuarants Failure',
    RESAVE_RESTAURANTS_SUCCESS = '[RESTAURANT] Resave Restuarants Success',
    RESAVE_RESTAURANTS_FAILURE = '[RESTAURANT] Resave Restuarants Failure',
    LOAD_RESTAURANTS_IN_BOUNDS = '[RESTAURANT] Load In Bounds Restuarants Action',
    LOAD_RESTAURANTS_IN_BOUNDS_SUCCESS = '[RESTAURANT] Load In Bounds Restuarants Success',
    LOAD_RESTAURANTS_IN_BOUNDS_FAILURE = '[RESTAURANT] Load In Bounds Restuarants Failure'
}

export class LoadRestaurantAction implements Action {
    readonly type = RestaurantActionTypes.LOAD_RESTAURANT;

    constructor(public payload: number) { }
}

export class LoadRestaurantSuccessAction implements Action {
    readonly type = RestaurantActionTypes.LOAD_RESTUARANT_SUCCESS;

    constructor(public payload: Restaurant) { }
}

export class LoadRestaurantFailureAction implements Action {
    readonly type = RestaurantActionTypes.LOAD_RESTAURANT_FAILURE;
}

export class LoadNearByRestaurantsAction implements Action {
    readonly type = RestaurantActionTypes.LOAD_NEARBY_RESTAURANTS;

    constructor(public payload: {radius: number, latitude: number, longitude: number, idsInMem: number[]}) { }
}

export class LoadNearByRestaurantsSuccessAction implements Action {
    readonly type = RestaurantActionTypes.LOAD_NEARBY_RESTAURANTS_SUCCESS;

    constructor(public payload: Restaurant[]) { }
}

export class LoadNearByRestaurantsFailureAction implements Action {
    readonly type = RestaurantActionTypes.LOAD_NEARBY_RESTAURANTS_FAILURE;
}

export class ResaveRestaurantsSuccessAction implements Action {
    readonly type = RestaurantActionTypes.RESAVE_RESTAURANTS_SUCCESS;

    constructor(public payload: Restaurant[]) { }
}

export class ResaveRestaurantsFailureAction implements Action {
    readonly type = RestaurantActionTypes.RESAVE_RESTAURANTS_FAILURE;
}

export class LoadRestaurantsInBoundsAction implements Action {
    readonly type = RestaurantActionTypes.LOAD_RESTAURANTS_IN_BOUNDS;

    constructor(public payload: VisibleRegion) { }
}

export class LoadRestaurantsInBoundsSuccessAction implements Action {
    readonly type = RestaurantActionTypes.LOAD_RESTAURANTS_IN_BOUNDS_SUCCESS;

    constructor(public payload: Restaurant[]) { }
}

export class LoadRestaurantsInBoundsFailureAction implements Action {
    readonly type = RestaurantActionTypes.LOAD_RESTAURANTS_IN_BOUNDS_FAILURE;
}

export type RestaurantAction
  = LoadRestaurantAction
  | LoadRestaurantSuccessAction
  | LoadRestaurantFailureAction
  | LoadNearByRestaurantsAction
  | LoadNearByRestaurantsSuccessAction
  | LoadNearByRestaurantsFailureAction
  | ResaveRestaurantsSuccessAction
  | ResaveRestaurantsFailureAction
  | LoadRestaurantsInBoundsAction
  | LoadRestaurantsInBoundsSuccessAction
  | LoadRestaurantsInBoundsFailureAction;
