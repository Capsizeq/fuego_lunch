import { ActionReducerMap } from '@ngrx/store';

import { LocationActionTypes, LocationAction } from './location.actions';
import { ApplicationState } from '../app.state';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';


export interface LocationState {
    currentLocation: Geoposition;
}

export const LOCATION_INITIAL_STATE: LocationState = {
    currentLocation: null
}

export function locationReducer(state: LocationState = LOCATION_INITIAL_STATE, action: LocationAction) {
    switch (action.type) {
        case LocationActionTypes.SAVE_LOCATION: {
            return { ...state, currentLocation: action.payload };
        }
        default: {
            return state;
        }
    }
}

export namespace locationQueries {
    export const getCurrentLocation = (state: ApplicationState) => state.locationState.currentLocation;
}
