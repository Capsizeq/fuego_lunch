import { Menu } from '../menu';
import { Restaurant } from '../restaurant';

export class CardInfo {
    id: number;
    title: string;
    image: string;
    num_dollers: number;
    type: string;
    distance: number;
    specials_menu: Menu;

    constructor(restaurant: Restaurant) {
        this.id = restaurant.id;
        this.title = restaurant.name;
        this.image = restaurant.banner_image;
        this.num_dollers = 2;
        this.type = restaurant.description;
        this.distance = restaurant.distance;

        /*
         * TODO: decide if we want to display all of the specials and only the specials in the Browse page
         * right now we are displaying the "specials menu", but not all restuarants will have a speicals menu.
         * So we might want to get a sparate menu returned from the back end that holds all of the specials for
         * the whole restaurant. Flag it as "all_specials".
         */
        restaurant.menus.forEach(menu => {
            if(menu.special_only) this.specials_menu = menu;
        });
    }
}