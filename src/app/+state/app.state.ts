import { ActionReducerMap } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';

import { environment } from '../../environments/environment';
import { TrendingState, trendingReducer } from '../+state/trending/trending.reducer';
import { RestaurantState, restaurantReducer } from './restaurant/restaurant.reducer';
import { LocationState, locationReducer } from './location/location.reducer';
import { BrowseState, browseReducer } from './browse/browse.reducer';

export interface ApplicationState {
  trendingState: TrendingState;
  restaurantState: RestaurantState;
  locationState: LocationState;
  browseState: BrowseState;
}

export const ROOT_REDUCER: ActionReducerMap<ApplicationState> = {
  trendingState: trendingReducer,
  restaurantState: restaurantReducer,
  locationState: locationReducer,
  browseState: browseReducer
}

export const META_REDUCERS = !environment.production ? [storeFreeze] : [];
