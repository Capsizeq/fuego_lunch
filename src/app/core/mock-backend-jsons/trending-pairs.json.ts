export let pairs = [
    {
      "header": "Local Burger Deals",
      "carouselItems": [
        {
          "image": "assets/images/trending/hooters_wings.png",
          "restaurant_name": "Hooters",
          "restaurant_id": 1,
          "menu_item_name": "Wings",
          "menu_item_price": "$7.99"
        },
        {
          "image": "assets/images/trending/black-and-blue-burger.jpeg",
          "restaurant_name": "Fitzgerald's",
          "restaurant_id": 0,
          "menu_item_name": "Black and Bleu Burger",
          "menu_item_price": "$9.99"
        },
        {
          "image": "assets/images/trending/bojangles_chicken.png",
          "restaurant_name": "Bojangles",
          "restaurant_id": 4,
          "menu_item_name": "Chicken Platter",
          "menu_item_price": "Half Off"
        }
      ],
      "listItems": [
        {"restaurant_name": "Fitzgeralds", "restaurant_id": 0, "menu_item_name": "All Burgers", "price": "$7.99"},
        {"restaurant_name": "Hooters", "restaurant_id": 1, "menu_item_name": "All Burgers", "price": "1/2 off"},
        {"restaurant_name": "Duckworths", "restaurant_id": 2, "menu_item_name": "Baconator", "price": "$8.50"},
        {"restaurant_name": "Cowbell", "restaurant_id": 3, "menu_item_name": "Carolina Burger", "price": "$5.49"}
      ]
    },
    {
      "header": "Local Chicken Deals",
      "carouselItems": [
        {
          "image": "assets/images/trending/black-and-blue-burger.jpeg",
          "restaurant_name": "Fitzgerald's",
          "restaurant_id": 0,
          "menu_item_name": "Black and Bleu Burger",
          "menu_item_price": "$9.99"
        },
        {
          "image": "assets/images/trending/hooters_wings.png",
          "restaurant_name": "Hooters",
          "restaurant_id": 1,
          "menu_item_name": "Wings",
          "menu_item_price": "$7.99"
        },
        {
          "image": "assets/images/trending/bojangles_chicken.png",
          "restaurant_name": "Bojangles",
          "restaurant_id": 4,
          "menu_item_name": "Chicken Platter",
          "menu_item_price": "Half Off"
        }
      ],
      "listItems": [
        {"restaurant_name": "Duckworths", "restaurant_id": 2, "menu_item_name": "Baconator", "price": "$8.50"},
        {"restaurant_name": "Fitzgeralds", "restaurant_id": 0, "menu_item_name": "All Burgers", "price": "$7.99"},
        {"restaurant_name": "Hooters", "restaurant_id": 1, "menu_item_name": "All Burgers", "price": "1/2 off"},
        {"restaurant_name": "Cowbell", "restaurant_id": 3, "menu_item_name": "Carolina Burger", "price": "$5.49"}
      ]
    },
    {
      "header": "Local Salad Deals",
      "carouselItems": [
        {
          "image": "assets/images/trending/bojangles_chicken.png",
          "restaurant_name": "Bojangles",
          "restaurant_id": 4,
          "menu_item_name": "Chicken Platter",
          "menu_item_price": "Half Off"
        },
        {
          "image": "assets/images/trending/black-and-blue-burger.jpeg",
          "restaurant_name": "Fitzgerald's",
          "restaurant_id": 0,
          "menu_item_name": "Black and Bleu Burger",
          "menu_item_price": "$9.99"
        },
        {
          "image": "assets/images/trending/hooters_wings.png",
          "restaurant_name": "Hooters",
          "restaurant_id": 1,
          "menu_item_name": "Wings",
          "menu_item_price": "$7.99"
        }
      ],
      "listItems": [
        {"restaurant_name": "Cowbell", "restaurant_id": 3, "menu_item_name": "Carolina Burger", "price": "$5.49"},
        {"restaurant_name": "Fitzgeralds", "restaurant_id": 0, "menu_item_name": "All Burgers", "price": "$7.99"},
        {"restaurant_name": "Duckworths", "restaurant_id": 2, "menu_item_name": "Baconator", "price": "$8.50"},
        {"restaurant_name": "Hooters", "restaurant_id": 1, "menu_item_name": "All Burgers", "price": "1/2 off"}
      ]
    }
  ];