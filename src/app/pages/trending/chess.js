//Chess board
let grid = 8;
let board = "";
let newLine = "\n";

for (let i = 1; i < grid + 1; i++) {
    for(let j = 1; j < grid + 1; j++) {
        if ((i + j) & 1 === 1) {
            board += "#";
        } else {
            board += " ";
        }
    }
    board += newLine;
}
console.log(board);