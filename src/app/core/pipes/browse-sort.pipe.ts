import { Pipe, PipeTransform } from '@angular/core';
import { Restaurant } from '../../core/objects/restaurant';
import { specials } from '../jsons/specials.json';
/*
 * Sort the items on the browse page
 * Takes in an array of restaurants and sorts them based on the browse options attributes.
*/
@Pipe({name: 'browseSort', pure: true})
export class BrowseSortPipe implements PipeTransform {
  transform(restaurants: Restaurant[], sortBy: string, distance: number, searchTerm: string): Restaurant[] {


    let temp = new Array<Restaurant>();
    let specials = new Array<Restaurant>();
    let nonSpecials = new Array<Restaurant>();

    try {
      /*
       * The incoming 'restaurants' is immutable, so we need to make a copy and then modify that and
       * pass that back.
       */
      restaurants.forEach(r => {
        temp.push(r);
      });

      /*
       * For ease of use, lowercase everything for comparison.
       */
      const st = searchTerm.toLowerCase();

      /*
       * Loop through each restaurant and see if the search word is contained in either its:
       * 1) name
       * 2) any of it's tags (which will be a string of space delimited tags)
       */
      temp = temp.filter((r) => {
        let matchFound = false;
        const searchTerms = searchTerm.split(" ");

        /*
         * If it's too far away, then nope to this restaurant
         */
        if(r.distance > distance)
          return false;

        searchTerms.forEach(term => {
          if(!term) {
            term = "";
          }

          term = term.toString().toLowerCase();

          if(searchTerms.length == 1 ||  (term != "" && searchTerms.length > 1)) {
            let name = r.name.toString().toLowerCase().indexOf(term) > -1;
            let tag = r.tags.toString().toLowerCase().indexOf(term) > -1;
            matchFound = name || tag;
            if(matchFound) return;
          }
        });

        return matchFound;
      });

      /*
       * Split the list into two, those that have specials to display, and those that don't.
       * We do still want to display the restaurants that don't have specials so that the user knows that that we know
       * about the restaurant, it just doesn't have a special.
       */

      temp.forEach(r => {
        if(r.specials.length != 0)
          specials.push(r);
        else
          nonSpecials.push(r);
      });


      /*
       * Sort the newly filtered list based on 
       */
      switch(sortBy) {
        case 'High To Low': {
          specials.sort((a,b)=>{return b.expense_rating-a.expense_rating});
          nonSpecials.sort((a,b)=>{return b.expense_rating-a.expense_rating});
          break;
        }
        case 'Low To High': {
          specials.sort((a,b)=>{return a.expense_rating-b.expense_rating});
          nonSpecials.sort((a,b)=>{return a.expense_rating-b.expense_rating});
          break;
        }
        default: {
          specials.sort((a,b)=>{return a.distance-b.distance});
          nonSpecials.sort((a,b)=>{return a.distance-b.distance});
        }
      }

    } catch(err) {
      throw new Error(err);
    } finally {
      /*
       * Tack nonSpecials on to the end of specials so that it displays below it.
       */
      return specials.concat(nonSpecials);
    }
  }
}
