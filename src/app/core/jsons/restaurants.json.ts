import { Restaurant } from "../objects/restaurant";

export const restaurants: Array<Restaurant> = [{
    id: 8,
    franchise_id: 0,
    name: "Fitzgerald's",
    description: "Nice Restaurant",
    tags: "burger uptown nice",
    address: "201 E 5th St, Charlotte, NC 28202",
    address2: "",
    city: "Charlotte",
    state: "NC",
    zip: 28202,
    phone: "(704) 900-8088",
    expense_rating: 3,
    logo_image: "",
    latitude: 35.2269273,
    longitude: -80.84038079999999,
    distance: 1,
    banner_image: "assets/images/banners/fitzgeralds.jpg",
    pin_image: "",
    website: "http://www.fitzgeraldscharlotte.com",
    menu_file_url: "https://clients.yourareacode.com/shared/userfiles/menus/823e2786146707375f60a85fe250bd2e1514906051.pdf",
    menus: [{
      id: 0,
      restaurant_id: 0,
      title: "Fitzgerald's Specials",
      description: "specials menu",
      items: [{
        id: 0,
        menu_id: 0,
        title: "Fitzgerald's Special Item 1",
        is_on_special: true,
        description: "description",
        price: "1.99",
        tag: ""
    },{
        id: 1,
        menu_id: 0,
        title: "Fitzgerald's Special Item 2",
        is_on_special: false,
        description: "description",
        price: "2.99",
        tag: ""
    },{
        id: 2,
        menu_id: 0,
        title: "Fitzgerald's Special Item 3",
        is_on_special: false,
        description: "description",
        price: "3.99",
        tag: ""
    },{
        id: 3,
        menu_id: 0,
        title: "Fitzgerald's Special Item 4",
        is_on_special: true,
        description: "description",
        price: "Half Off",
        tag: ""
    },{
        id: 4,
        menu_id: 0,
        title: "Fitzgerald's Special Item 5",
        is_on_special: true,
        description: "description",
        price: "Half Off",
        tag: ""
    },{
        id: 5,
        menu_id: 0,
        title: "Fitzgerald's Special Item 6",
        is_on_special: true,
        description: "description",
        price: "Half Off",
        tag: ""
    },],
      special_only: true,
      tag: ""
    },
    {
      id: 1,
      restaurant_id: 0,
      title: "Fitzgerald's  Lunch",
      description: "lunch menu",
      items: [{
        id: 4,
        menu_id: 1,
        title: "Fitzgerald's Item 1",
        is_on_special: true,
        description: "description",
        price: "7.99",
        tag: ""
    },{
        id: 5,
        menu_id: 1,
        title: "Fitzgerald's Item 2",
        is_on_special: false,
        description: "description",
        price: "5.99",
        tag: ""
    },{
        id: 6,
        menu_id: 1,
        title: "Fitzgerald's Item 3",
        is_on_special: true,
        description: "description",
        price: "13.99",
        tag: ""
    },{
        id: 7,
        menu_id: 1,
        title: "Fitzgerald's Item 4",
        is_on_special: false,
        description: "description",
        price: "12.99",
        tag: ""
    }],
      special_only: false,
      tag: ""
    }],
    specials: [
        {
            title:"special 1", 
            value: "half off"
        },
        {
            title:"special 2", 
            value: "half off"
        },
        {
            title:"special 3", 
            value: "half off"
        },
        {
            title:"special 4", 
            value: "half off"
        },
        {
            title:"special 5", 
            value: "half off"
        },
        {
            title:"special 6", 
            value: "half off"
        }
    ]
  },{
    id: 1,
    franchise_id: 1,
    name: "Hooters",
    description: "Sports Bar",
    tags: "wings sports bar beer",
    address: "7702 Gateway Ln NW",
    address2: "",
    city: "Concord",
    state: "NC",
    zip: 28027,
    phone: "(704) 979-0130",
    expense_rating: 2,
    logo_image: "",
    banner_image: "assets/images/banners/hooters.jpg",
    pin_image: "",
    latitude: 35.365019,
    longitude: -80.70693110000002,
    distance: 2,
    website: "http://www.fitzgeraldscharlotte.com",
    menu_file_url: "",
    menus: [
      {
        id: 2,
        restaurant_id: 1,
        title: "Hooters Specials",
        description: "specials menu",
        items: [{
          id: 8,
          menu_id: 2,
          title: "Hooters Special Item 1",
          is_on_special: false,
          description: "description",
          price: "1.99",
          tag: ""
      },{
          id: 9,
          menu_id: 2,
          title: "Hooters Special Item 2",
          is_on_special: false,
          description: "description",
          price: "2.99",
          tag: ""
      },{
          id: 10,
          menu_id: 2,
          title: "Hooters Special Item 3",
          is_on_special: true,
          description: "description",
          price: "3.99",
          tag: ""
      },{
          id: 11,
          menu_id: 2,
          title: "Hooters Special Item 4",
          is_on_special: true,
          description: "description",
          price: "Half Off",
          tag: ""
      }],
        special_only: true,
        tag: ""
      },
      {
        id: 3,
        restaurant_id: 1,
        title: "Hooters  Lunch",
        description: "lunch menu",
        items: [{
          id: 12,
          menu_id: 3,
          title: "Hooters Item 1",
          is_on_special: true,
          description: "description",
          price: "7.99",
          tag: ""
      },{
          id: 13,
          menu_id: 3,
          title: "Hooters Item 2",
          is_on_special: true,
          description: "description",
          price: "5.99",
          tag: ""
      },{
          id: 14,
          menu_id: 3,
          title: "Hooters Item 3",
          is_on_special: false,
          description: "description",
          price: "13.99",
          tag: ""
      },{
          id: 15,
          menu_id: 3,
          title: "Hooters Item 4",
          is_on_special: false,
          description: "description",
          price: "12.99",
          tag: ""
      }],
        special_only: false,
        tag: ""
      }
    ],
    specials: []
  },{
    id: 2,
    franchise_id: 2,
    name: "Duckworths's",
    description: "Sports Bar",
    tags: "sports bar beer craft",
    address: "16609 Statesville Rd",
    address2: "",
    city: "Huntersville",
    state: "NC",
    zip: 28078,
    phone: "(704) 237-4387",
    expense_rating: 3,
    logo_image: "",
    banner_image: "assets/images/banners/duckworths.jpg",
    pin_image: "",
    latitude: 35.4401096,
    longitude: -80.86726920000001,
    distance: 3,
    website: "http://www.fitzgeraldscharlotte.com",
    menu_file_url: "",
    menus: [
      {
        id: 4,
        restaurant_id: 2,
        title: "Duckworths Specials",
        description: "specials menu",
        items: [{
          id: 16,
          menu_id: 4,
          title: "Duckworths Special Item 1",
          is_on_special: true,
          description: "description",
          price: "1.99",
          tag: ""
      },{
          id: 17,
          menu_id: 4,
          title: "Duckworths Special Item 2",
          is_on_special: true,
          description: "description",
          price: "2.99",
          tag: ""
      },{
          id: 18,
          menu_id: 4,
          title: "Duckworths Special Item 3",
          is_on_special: true,
          description: "description",
          price: "3.99",
          tag: ""
      },{
          id: 19,
          menu_id: 4,
          title: "Duckworths Special Item 4",
          is_on_special: true,
          description: "description",
          price: "Half Off",
          tag: ""
      }],
        special_only: true,
        tag: ""
      },
      {
        id: 5,
        restaurant_id: 2,
        title: "Duckworths  Lunch",
        description: "lunch menu",
        items: [{
          id: 20,
          menu_id: 5,
          title: "Duckworths Item 1",
          is_on_special: false,
          description: "description",
          price: "7.99",
          tag: ""
      },{
          id: 21,
          menu_id: 5,
          title: "Duckworths Item 2",
          is_on_special: true,
          description: "description",
          price: "5.99",
          tag: ""
      },{
          id: 22,
          menu_id: 5,
          title: "Duckworths Item 3",
          is_on_special: false,
          description: "description",
          price: "13.99",
          tag: ""
      },{
          id: 23,
          menu_id: 5,
          title: "Duckworths Item 4",
          is_on_special: true,
          description: "description",
          price: "12.99",
          tag: ""
      }],
        special_only: false,
        tag: ""
      }
    ],
    specials: [{title:"special 1", value: "half off"}]
  },{
    id: 3,
    franchise_id: 3,
    name: "Cowbell",
    description: "Good Eats Place",
    tags: "beef burger nice wings",
    address: "201 N Tryon St",
    address2: "Suite 1010",
    city: "Charlotte",
    state: "NC",
    zip: 28202,
    phone: "(980) 224-8674",
    expense_rating: 4,
    logo_image: "",
    banner_image: "assets/images/banners/cowbell.jpeg",
    pin_image: "",
    latitude:35.2285912,
    longitude: -80.8416229,
    distance: 4,
    website: "https://eatmorecowbell.com/",
    menu_file_url: "",
    menus: [
      {
        id: 6,
        restaurant_id: 3,
        title: "Cowbell Specials",
        description: "specials menu",
        items: [{
          id: 24,
          menu_id: 6,
          title: "Cowbell Special Item 1",
          is_on_special: true,
          description: "description",
          price: "1.99",
          tag: ""
      },{
          id: 25,
          menu_id: 6,
          title: "Cowbell Special Item 2",
          is_on_special: true,
          description: "description",
          price: "2.99",
          tag: ""
      },{
          id: 26,
          menu_id: 6,
          title: "Cowbell Special Item 3",
          is_on_special: false,
          description: "description",
          price: "3.99",
          tag: ""
      },{
          id: 27,
          menu_id: 6,
          title: "Cowbell Special Item 4",
          is_on_special: true,
          description: "description",
          price: "Half Off",
          tag: ""
      }],
        special_only: true,
        tag: ""
      },
      {
        id: 7,
        restaurant_id: 3,
        title: "Cowbell  Lunch",
        description: "lunch menu",
        items: [{
          id: 28,
          menu_id: 7,
          title: "Cowbell Item 1",
          is_on_special: true,
          description: "description",
          price: "7.99",
          tag: ""
      },{
          id: 29,
          menu_id: 7,
          title: "Cowbell Item 2",
          is_on_special: true,
          description: "description",
          price: "5.99",
          tag: ""
      },{
          id: 30,
          menu_id: 7,
          title: "Cowbell Item 3",
          is_on_special: false,
          description: "description",
          price: "13.99",
          tag: ""
      },{
          id: 31,
          menu_id: 7,
          title: "Cowbell Item 4",
          is_on_special: true,
          description: "description",
          price: "12.99",
          tag: ""
      }],
        special_only: false,
        tag: ""
      }
    ],
    specials: []
  },{
    id: 4,
    franchise_id: 4,
    name: "Bojangles",
    description: "Fast Food",
    tags: "fast food cheap chicken",
    address: "10329 Mallard Creek Rd",
    address2: "",
    city: "Charlotte",
    state: "NC",
    zip: 28262,
    phone: "(704) 503-4648",
    expense_rating: 1,
    logo_image: "",
    banner_image: "assets/images/banners/cowbell.jpeg",
    pin_image: "",
    latitude: 35.341431,
    longitude: -80.76946699999996,
    distance: 5,
    website: "https://www.bojanasdfasdfasdfasdfasdfgles.com/",
    menu_file_url: "",
    menus: [
      {
        id: 8,
        restaurant_id: 4,
        title: "Bojangles Specials",
        description: "specials menu",
        items: [{
          id: 32,
          menu_id: 8,
          title: "Bojangles Special Item 1",
          is_on_special: false,
          description: "description",
          price: "1.99",
          tag: ""
      },{
          id: 33,
          menu_id: 8,
          title: "Bojangles Special Item 2",
          is_on_special: false,
          description: "description",
          price: "2.99",
          tag: ""
      },{
          id: 34,
          menu_id: 8,
          title: "Bojangles Special Item 3",
          is_on_special: true,
          description: "description",
          price: "3.99",
          tag: ""
      },{
          id: 35,
          menu_id: 8,
          title: "Bojangles Special Item 4",
          is_on_special: false,
          description: "description",
          price: "Half Off",
          tag: ""
      }],
        special_only: true,
        tag: ""
      },
      {
        id: 9,
        restaurant_id: 4,
        title: "Bojangles  Lunch",
        description: "lunch menu",
        items: [{
          id: 36,
          menu_id: 9,
          title: "Bojangles Item 1",
          is_on_special: true,
          description: "description",
          price: "7.99",
          tag: ""
      },{
          id: 37,
          menu_id: 9,
          title: "Bojangles Item 2",
          is_on_special: false,
          description: "description",
          price: "5.99",
          tag: ""
      },{
          id: 38,
          menu_id: 9,
          title: "Bojangles Item 3",
          is_on_special: false,
          description: "description",
          price: "13.99",
          tag: ""
      },{
          id: 39,
          menu_id: 9,
          title: "Bojangles Item 4",
          is_on_special: true,
          description: "description",
          price: "12.99",
          tag: ""
      }],
        special_only: false,
        tag: ""
      }
    ],
    specials: [{title:"special 1", value: "half off"}]
  }];