import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';

import { LocationActionTypes, SaveLocationAction } from './location.actions';
import { ApplicationState } from '../app.state';
import { LocationService } from '../../core/services/location.service';
import { Store } from '@ngrx/store';
import { Restaurant } from '../../core/objects/restaurant';
import { LatLng, Spherical } from '@ionic-native/google-maps';
import { conversions } from '../conversions';
import { LoadNearByRestaurantsAction, LoadNearByRestaurantsSuccessAction } from '../restaurant/restaurant.actions';

@Injectable()
export class LocationEffects {

    @Effect() saveLocationSucess$ = this.d.fetch(LocationActionTypes.SAVE_LOCATION, {
        run: (action: SaveLocationAction, state: ApplicationState) => {
            return this.locationService.updateRestaurantsDistanes(action.payload);
        },
        onError: (action: SaveLocationAction, e: any) => {
            // can add error handling here but for now just return null
            return null;
        }
    });

    constructor(
        // private actions$: Actions,
        private locationService: LocationService,
        private d: DataPersistence<ApplicationState>
    ) { }
}
