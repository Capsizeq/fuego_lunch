import { Injectable } from '@angular/core';
import { ApplicationState } from '../app.state';
import { TrendingService } from '../../core/services/trending.service';
import { Store } from '@ngrx/store';

import { BrowseState, browseQueries } from './browse.reducer';
import { SavePaginationIndexAction, SaveRadiusAction } from './browse.actions';
import { LocationFacade } from '../location/location.facade';
import { Geoposition } from '@ionic-native/geolocation';
import { restaurantQueries } from '../restaurant/restaurant.reducer';
import { Restaurant } from '../../core/objects/restaurant';
import { LoadNearByRestaurantsAction } from '../restaurant/restaurant.actions';

@Injectable()
export class BrowseFacade {
    restaurants$ = this.store.select(restaurantQueries.getRestuarants);


    constructor(private store: Store<ApplicationState>,
        private locationFacade: LocationFacade) { }

    saveRadius(radius: number, loc: Geoposition) {
        let inMemoryNearByRestaurantIdList = new Array<number>();

        this.restaurants$.subscribe(restaurants => {
            restaurants.forEach(r => {
                inMemoryNearByRestaurantIdList.push(r.id);
            })
        }).unsubscribe();
        
        this.store.dispatch(new LoadNearByRestaurantsAction({
            "radius": radius, 
            "latitude": loc.coords.latitude, 
            "longitude": loc.coords.longitude,
            "idsInMem": inMemoryNearByRestaurantIdList})
        );
    }

    savePaginationIndex(index: number) {
        this.store.dispatch(new SavePaginationIndexAction(index));
    }
}
