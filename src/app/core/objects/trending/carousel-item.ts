export class CarouselItem {
    image: string;
    restaurant_name: string;
    restaurant_id: number;
    menu_item_name: string;
    price: string;
    ad?: boolean;
}