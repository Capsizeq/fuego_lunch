import { Injectable } from '@angular/core';
import { ApplicationState } from '../app.state';
import { TrendingService } from '../../core/services/trending.service';
import { Store } from '@ngrx/store';

import { LoadRestaurantAction, LoadNearByRestaurantsAction, LoadRestaurantsInBoundsAction } from './restaurant.actions';
import { RestaurantState, restaurantQueries } from './restaurant.reducer';
import { Restaurant } from '../../core/objects/restaurant';
import { last } from '@angular/router/src/utils/collection';
import { LocationFacade } from '../location/location.facade';
import { locationQueries } from '../location/location.reducer';
import { CardInfo } from '../../core/objects/browse/card-info';
import { BrowseFacade } from '../browse/browse.facade';
import { Observable } from 'rxjs/Observable';
import { RestaurantService } from '../../core/services/restaurant.service';
import { Geoposition } from '@ionic-native/geolocation';
import { LatLngBounds, VisibleRegion } from '@ionic-native/google-maps';

@Injectable()
export class RestaurantFacade {
    restaurants$ = this.store.select(restaurantQueries.getRestuarants);
    currentLocation$ = this.store.select(locationQueries.getCurrentLocation);
    loading$ = this.store.select(restaurantQueries.loading);

    constructor(
        private store: Store<ApplicationState>,
        private locationFacade: LocationFacade,
        private browseFacade: BrowseFacade,
        private restaurantService: RestaurantService) { }

    checkForRestaurant(id: number) {
        let rest: Restaurant;

        /*
         * Check to see if the restuarant exists
         */
        this.restaurants$.subscribe((restaurants: Array<Restaurant>) => {
            rest = restaurants.filter((r) => {return r.id === id})[0];
        }).unsubscribe();

        /*
         * If it doesn't go fetch it.
         */
        if (!(!!rest))
            this.store.dispatch(new LoadRestaurantAction(id));

        /*
         * Return the full restaurant array so that if the restaurant didn't exist in memory, then it can be 
         * asynchronously loaded when the restaurant comes back from the network call.
         */
        return this.restaurants$;
    }

    loadNearBy(loc: Geoposition) {
        this.locationFacade.loadLocation(loc);

        return this.restaurants$.map((restaurants) => {
            return restaurants;
        });
    }

    loadInBounds(bounds: VisibleRegion) {
        this.store.dispatch(new LoadRestaurantsInBoundsAction(bounds));

        const restaurantsInRadius = new Array<Restaurant>();
        return this.restaurants$.map((restaurants) => {
            return restaurants;
        });
    }

    getBrowseCards() {
        const restaurantsInRadius = new Array<Restaurant>();

        // return this.restaurants$.map((data: Map<number, Restaurant>) => {
        return this.restaurantService.getNearBy(null, null, null, null).map((data: Restaurant[]) => {
            console.log("returning full array: ", data);
            return data;
        });
    }

    refresh(loc: Geoposition) {
        this.locationFacade.refreshLocation(loc);

        const restaurantsInRadius = new Array<Restaurant>();
        return this.restaurants$.map((restaurants) => {
            return restaurants;
        });
    }

    getLoading() {
        return this.loading$;
    }

}
