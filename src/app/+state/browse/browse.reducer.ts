import { ActionReducerMap } from '@ngrx/store';

import { BrowseActionTypes, BrowseAction } from './browse.actions';
import { ApplicationState } from '../app.state';


export interface BrowseState {
    radius: number;
    paginationIndex: number;
}

export const LOCATION_INITIAL_STATE: BrowseState = {
    radius: 5,
    paginationIndex: 0
}

export function browseReducer(state: BrowseState = LOCATION_INITIAL_STATE, action: BrowseAction) {
    switch (action.type) {
        case BrowseActionTypes.SAVE_PAGINATION_INDEX: {
            return { ...state, paginationIndex: action.payload };
        }
        case BrowseActionTypes.SAVE_RADIUS: {
            return { ...state, radius: action.payload };
        }
        default: {
            return state;
        }
    }
}

export namespace browseQueries {
    export const getPaginationIndex = (state: ApplicationState) => state.browseState.paginationIndex;
    export const getRadius = (state: ApplicationState) => state.browseState.radius;
}
