import { Injectable } from '@angular/core';
import { Restaurant } from '../objects/restaurant';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import { Geoposition } from '@ionic-native/geolocation';
import { LatLngBounds, VisibleRegion } from '@ionic-native/google-maps';
import { restaurants } from '../jsons/restaurants.json';
import { err } from '../jsons/restaurant-timeout-error.json';
import { ApplicationState } from '../../+state/app.state';
import { Store } from '@ngrx/store';
import { restaurantQueries } from '../../+state/restaurant/restaurant.reducer';
import { retry } from 'rxjs/operator/retry';


@Injectable()
export class RestaurantService {
    restuarants$ = this.store.select(restaurantQueries.getRestuarants);
    apiUrl = "https://beta.fuegospecials.com/api";

    constructor(private http: HttpClient,
        private store: Store<ApplicationState>) { }

    getById(id: number): Observable<Restaurant> {
        // return Observable.of(restaurants.filter(r => r.id === id)[0]).delay(2000);
        
        try {
            return this.http.get<Restaurant>(`${this.apiUrl}/restaurant/${id}`)
            .timeout(3000)
            .retry(3)
            .catch(err => {
                let timeout = err;
                timeout.id = id;
                return Observable.of(timeout);
            });
        } catch (err) {
            throw new Error();
        }
    }

    getNearBy(radius: number, latitude: number, longitude: number, idsInMem: number[]) {
        /* let temp = restaurants.filter((r) => { return idsInMem.indexOf(r.id) == -1 })
        return Observable.of(temp); */

        let exemptIds = new Array<number>();
        this.restuarants$.subscribe(restaurants => {
            
            restaurants.forEach(r => {
                exemptIds.push(r.id);
            })
        });

        const body = 
        {
            "latitude": latitude,
            "longitude": longitude,
            "radius": Number(radius),
            "exempt_ids": exemptIds,
            "start":0
        }

        try {
            return this.http.post<Restaurant[]>(`${this.apiUrl}/restaurants/radius`, JSON.stringify(body))
            .timeout(3000)
            .retry(3);
        } catch (err) {
            throw new Error();
        }
    }

    getInBounds(bounds: VisibleRegion) {
        /* let temp = restaurants.filter((r) => {
            return (
                (r.latitude < bounds.northeast.lat && r.latitude > bounds.southwest.lat) 
                &&
                (r.longitude < bounds.northeast.lng && r.longitude > bounds.southwest.lng)
            )}
        );
        temp.forEach(r => {
            console.log(`returning: ${r.name}`)
        })

        return Observable.of(temp); */

        /* let exemptIds = new Array<number>();
        this.restuarants$.subscribe(restaurants => {getInBounds(bounds: VisibleRegion) {
        let temp = restaurants.filter((r) => {
            return (
                (r.latitude < bounds.northeast.lat && r.latitude > bounds.southwest.lat) 
                &&
                (r.longitude < bounds.northeast.lng && r.longitude > bounds.southwest.lng)
            )}
        );
        temp.forEach(r => {
            console.log(`returning: ${r.name}`)
        })

        return Observable.of(temp); */

        let exemptIds = new Array<number>();
        this.restuarants$.subscribe(restaurants => {
            
            restaurants.forEach(r => {
                exemptIds.push(r.id);
            })
        });
        const body = 
        {
            "latitude": 35.227190,
            "longitude": -80.843097,
            "top_right": 
            { 
                "latitude": bounds.northeast.lat, 
                "longitude": bounds.northeast.lng
            },
            "bottom_left": 
            {
                "latitude": bounds.southwest.lat,
                "longitude": bounds.southwest.lng
            },
            "exempt_ids": exemptIds,
            "start":0
        }
        try {
            return this.http.post<Restaurant[]>(`${this.apiUrl}/restaurants/bounds`, JSON.stringify(body))
            .timeout(3000)
            .retry(3);
        } catch(err) {
            throw new Error();
        }

    }

}
