import { CarouselItem } from "./carousel-item";
import { ListItem } from "./list-item";
import { List } from "./list";

export class Trending {
    carouselItems: Array<CarouselItem>;
    lists: Array<List>;
}