import { Injectable } from '@angular/core';
import { SpecialCategory } from '../objects/specials/special-category';
import { Observable } from 'rxjs/Observable';
import { specials } from '../jsons/specials.json';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import 'rxjs/add/operator/retry';

@Injectable()
export class SpecialsService {
  specials: Array<SpecialCategory>;

  constructor(private http: HttpClient) { }

  getCategorizedSpecials(latitude: number, longitude: number, searchTerm: string, radius: number, category: string): Observable<Array<SpecialCategory>> {
    
    let cat = 0;
    console.log();
    switch(category) {
      case "category": {
        cat = 0;
        break;
      }
      case "restaurant": {
        cat = 1;
        break;
      }
      case "special": {
        cat = 2;
        break;
      }
    }

    const body = {
      "search": searchTerm,
      "latitude": latitude,
      "longitude": longitude,
      "radius": Number.parseInt(radius.toString()),
      "category": cat,
      "start": 0
    };

    try {
      return this.http.post<Array<SpecialCategory>>("https://beta.lunchspecialsapp.com/api/specials/radius", body)
      .timeout(3000)
      .retry(3)
      .catch((err: HttpErrorResponse) => {
        // simple logging, but you can do a lot more, see below
        if(err.error.toString().indexOf('404 Not Found') > -1) {

        } else if(err.error.toString().indexOf('502 Bad Gateway') > -1) {
          return  Observable.of([{title: '502', specials: []}]);
        }
        return  Observable.of(new Array<SpecialCategory>());
      });
    } catch (err) {
      return  Observable.of(new Array<SpecialCategory>());
    }

    // return Observable.of(specials).delay(1000);
  }
}