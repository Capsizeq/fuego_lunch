import { ActionReducerMap } from '@ngrx/store';

import { RestaurantActionTypes, RestaurantAction } from './restaurant.actions';
import { ApplicationState } from '../app.state';
import { Restaurant } from '../../core/objects/restaurant';


export interface RestaurantState {
    restaurants: Restaurant[];
    loading: boolean;
}

export const RESTAURANT_INITIAL_STATE: RestaurantState = {
    restaurants: new Array<Restaurant>(),
    loading: false
}

export function restaurantReducer(state: RestaurantState = RESTAURANT_INITIAL_STATE, action: RestaurantAction) {
    switch (action.type) {
        case RestaurantActionTypes.LOAD_RESTUARANT_SUCCESS: {
            let temp = new Map<number, Restaurant>();
            state.restaurants.forEach((r) => {
                temp.set(r.id, r);
            });
            temp.set(action.payload.id, action.payload);
            return { ...state, restaurants: Array.from(temp.values()), loading: false };
        }
        case RestaurantActionTypes.LOAD_RESTAURANTS_IN_BOUNDS_SUCCESS:
        case RestaurantActionTypes.LOAD_NEARBY_RESTAURANTS_SUCCESS: {
            let temp = new Map<number, Restaurant>();

            /*
             * Convert the restsaurants to a map with the `id` being the key.
             */
            state.restaurants.forEach((r) => {
                temp.set(r.id, r);
            });

            /*
             * For each restaurant returned in the response, add it to the map. Maps inherintly overwrite objects that share the same key
             * with the new object that is being brought in. Thus allowing us to update any new attributes of the restaurant since the last
             * time that it was loaded, such as the distance (which is sent to us by the server).
             */
            action.payload.forEach((r) => {
                temp.set(r.id, r);
            })
            const newRestaurantsArray = Array.from(temp.values());
            return { ...state, restaurants: newRestaurantsArray, loading: false };
        }
        case RestaurantActionTypes.RESAVE_RESTAURANTS_SUCCESS: {
            /*
             * Do the same thing as LOAD_NEARBY_RESTAURANTS_SUCCESS, except don't set `loading` back to false
             */
            let temp = new Map<number, Restaurant>();
            state.restaurants.forEach((r) => {
                temp.set(r.id, r);
            });
            action.payload.forEach((r) => {
                temp.set(r.id, r);
            })
            const newRestaurantsArray = Array.from(temp.values());
            return { ...state, restaurants: newRestaurantsArray };
        }
        case RestaurantActionTypes.LOAD_RESTAURANT: {
            return { ...state, loading: true };
        }
        case RestaurantActionTypes.LOAD_NEARBY_RESTAURANTS: {
            return { ...state, loading: true };
        }
        case RestaurantActionTypes.LOAD_RESTAURANT_FAILURE: {
            return { ...state, loading: false };
        }
        case RestaurantActionTypes.LOAD_NEARBY_RESTAURANTS_FAILURE: {
            return { ...state, loading: false };
        }
        case RestaurantActionTypes.LOAD_RESTAURANTS_IN_BOUNDS_FAILURE: {
            return { ...state, loading: false };
        }
        default: {
            return state;
        }
    }
}

export namespace restaurantQueries {
    export const getRestuarants = (state: ApplicationState) => state.restaurantState.restaurants;
    export const loading = (state: ApplicationState) => state.restaurantState.loading;
}
