import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map';

import { TrendingService } from '../../core/services/trending.service';
import { LoadRestaurantAction, LoadRestaurantFailureAction, LoadRestaurantSuccessAction, RestaurantAction, RestaurantActionTypes, LoadNearByRestaurantsAction, LoadNearByRestaurantsSuccessAction, LoadRestaurantsInBoundsSuccessAction, LoadRestaurantsInBoundsAction, LoadNearByRestaurantsFailureAction, LoadRestaurantsInBoundsFailureAction } from './restaurant.actions';
import { ApplicationState } from '../app.state';
import { RestaurantState, RESTAURANT_INITIAL_STATE } from './restaurant.reducer';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Restaurant } from '../../core/objects/restaurant';
import { RestaurantService } from '../../core/services/restaurant.service';

@Injectable()
export class RestaurantEffects {

    private getAuthStateSuccessMessage = 'Authenication Complete!';
    private getAuthStateErrorMessage = 'Failed to authenticate.';

    @Effect() loadRestaurant$ = this.d.fetch(RestaurantActionTypes.LOAD_RESTAURANT, {
        run: (action: LoadRestaurantAction, state: ApplicationState) => {
            return this.restaurantService.getById(action.payload)
            .map(
                (data: Restaurant) => {
                    if (data != undefined) {
                        return new LoadRestaurantSuccessAction(data)
                    } else {
                        return new LoadRestaurantFailureAction()
                    }
                },
                (err: any) => new LoadRestaurantFailureAction()
            )

        },
        onError: (action: LoadRestaurantAction, e: any) => {
            // can add error handling here but for now just return null
            return new LoadRestaurantFailureAction();
        }
    });

    @Effect() loadNearby$ = this.d.fetch(RestaurantActionTypes.LOAD_NEARBY_RESTAURANTS, {
        run: (action: LoadNearByRestaurantsAction, state: ApplicationState) => {
            return this.restaurantService.getNearBy(
                    action.payload.radius, 
                    action.payload.latitude, 
                    action.payload.longitude, 
                    action.payload.idsInMem)
            .map(
                (data: Restaurant[]) => new LoadNearByRestaurantsSuccessAction(data),
                (err: any) => new LoadNearByRestaurantsFailureAction()
            );
        },
        onError: (action: LoadNearByRestaurantsAction, e: any) => {
            // can add error handling here but for now just return null
            return new LoadNearByRestaurantsFailureAction();
        }
    });

    @Effect() loadInBounds$ = this.d.fetch(RestaurantActionTypes.LOAD_RESTAURANTS_IN_BOUNDS, {
        run: (action: LoadRestaurantsInBoundsAction, state: ApplicationState) => {
            return this.restaurantService.getInBounds(action.payload)
            .map(
                (data: Restaurant[]) => new LoadRestaurantsInBoundsSuccessAction(data),
                (err: any) => new LoadRestaurantsInBoundsFailureAction()
            );
        },
        onError: (action: LoadRestaurantsInBoundsAction, e: any) => {
            // can add error handling here but for now just return null
            return new LoadRestaurantsInBoundsFailureAction();
        }
    });

    // (data: Map<number, Restaurant>) => new LoadRestaurantSuccessAction(data)

    constructor(
        private actions$: Actions,
        private restaurantService: RestaurantService,
        private d: DataPersistence<ApplicationState>,
    ) { }
}
