import { MenuItem } from '../menu-item';

export class SpecialCategory {
    title: string;
    specials: Array<MenuItem>;
}