export class conversions {

    static METER_TO_MILE: number = 0.000621371;
    static METER_TO_HALF_MILE: number = 804.67;

    static toWholeMile(meters: number): number {
        console.log(meters, " meters converts to ", (meters * this.METER_TO_MILE), " miles");
        return meters * this.METER_TO_MILE;
    }
} 