import { Injectable } from '@angular/core';
import { ApplicationState } from '../app.state';
import { TrendingService } from '../../core/services/trending.service';
import { Store } from '@ngrx/store';

import { LocationState, locationQueries } from './location.reducer';
import { LocationService } from '../../core/services/location.service';
import { LatLng, Spherical } from '@ionic-native/google-maps';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { conversions } from '../conversions';
import { SaveLocationAction } from './location.actions';

@Injectable()
export class LocationFacade {
    currentLocation$ = this.store.select(locationQueries.getCurrentLocation);


    constructor(
        private store: Store<ApplicationState>,
        private geolocation: Geolocation,
        private locationService: LocationService) { }

    loadLocation(loc: Geoposition) {
        this.currentLocation$.subscribe((curLoc) => {
            /*
             * If `loc` is null then that means that this is the first time that the
             * location has been loaded, so just save it.
             */
            if(curLoc == null)
                this.store.dispatch(new SaveLocationAction(loc));
            else {
                const lastLatLng = new LatLng(curLoc.coords.latitude, curLoc.coords.longitude);
                const curLatLng = new LatLng(loc.coords.latitude, loc.coords.longitude);
                
                /*
                 * Don't redo any of the location saving action unless we've traveled more than 1/4 mile.
                 * This is because location saves also update all restaurant distances in store, which is battery
                 * consuming if we're doing it all the time.
                 */
                //console.log("dist since last thing: ", conversions.toWholeMile(Spherical.computeDistanceBetween(curLatLng, lastLatLng)));
                if(true)//(conversions.toWholeMile(Spherical.computeDistanceBetween(curLatLng, lastLatLng)) > .25)
                    this.store.dispatch(new SaveLocationAction(loc));
            }
            
        }).unsubscribe();
    }

    refreshLocation(loc: Geoposition) {
        this.store.dispatch(new SaveLocationAction(loc));
    }
}
