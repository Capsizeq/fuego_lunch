export const environment = { 
    production: true,
    version: "5.6.0",
    serverUrl: 'not_local',
    useMockBackend: false
}