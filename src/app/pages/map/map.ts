import { Component, ViewChild, ElementRef, Renderer } from '@angular/core';
import { NavController, MenuController, DomController, Platform, LoadingController, Loading } from 'ionic-angular';
import { GoogleMaps, GoogleMapOptions, LatLng, GoogleMapsEvent, LatLngBounds, GoogleMap, VisibleRegion, Marker } from '@ionic-native/google-maps';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { CallNumber } from '@ionic-native/call-number';
import { MapPlace } from '../../core/objects/map-place';
import { RestaurantFacade } from '../../+state/restaurant/restaurant.facade';
import { Restaurant } from '../../core/objects/restaurant';
import { RestaurantPage } from '../restaurant/restaurant';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Device } from '@ionic-native/device';
import { DomSanitizer } from '@angular/platform-browser';
import { splash } from '../../../assets/svgs/splash.svg';
import { googleAnalyticsTrackingId } from '../../core/objects/google-analytics-tracking-id';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { tradeAndTryonLocation } from '../../core/jsons/charlotte-location';
import { SpecialsPage } from '../specials/specials';
import { mapStyles } from '../../core/jsons/map-styles';

@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class MapPage {
  @ViewChild('searchbar') searchbar;
  @ViewChild('map') mapReference: ElementRef;
  @ViewChild('desc') descReference: ElementRef;

  private map: GoogleMap;
  private activeRestaurant: Restaurant;
  private menuOptions: any;
  private renderedIds: number[];
  private initialLoad: boolean;
  private pageSubscription: Subscription;
  private camMovementSubscription: Subscription;
  private loadingSpinner: Loading;
  private lastLatLng: LatLng;
  private loadingSvg: any;
  private headerSearchTerm: string;


  constructor(
    private ga: GoogleAnalytics,
    public navCtrl: NavController,
    private geolocation: Geolocation,
    private renderer: Renderer,
    private restaurantsFacade: RestaurantFacade,
    private menuCtrl: MenuController,
    private platform: Platform,
    private device: Device,
    private sanitizer: DomSanitizer,
    public loadingCtrl: LoadingController,
    private callNumber: CallNumber) {

      this.ga.startTrackerWithId(googleAnalyticsTrackingId)
        .then(() => {
          console.log('Google analytics is ready now');
              this.ga.trackView("Map Page");
              // Tracker is ready
              // You can now track pages or set additional information such as AppVersion or UserId
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));
      
      this.renderedIds = new Array<number>();
      this.initialLoad = true;

      this.loadingSvg = this.sanitizer.bypassSecurityTrustHtml(splash);
      this.loadingSpinner = this.loadingCtrl.create({
        spinner: 'hide',
        content: this.loadingSvg
      });

      setInterval(() => {
        this.activeRestaurant;
      }, 100);
  }

  ngAfterViewInit () {
    this.showLoadingSvg();
    this.loadMap();
  }

  ionViewDidEnter() {
    /*
    * Enable correct menu
    */
    this.menuCtrl.enable(false, 'specialsmenu');
    this.menuCtrl.enable(false, 'browsemenu');
    this.menuCtrl.enable(true, 'mapmenu');

    console.log("platform: ", this.device.platform);
    if(this.device.platform != "Android" && !this.initialLoad) {
      this.showLoadingSvg();
      this.loadMap();
    }
    this.initialLoad = false;
  }

  ionViewWillLeave() {
    /*
     * Destroy the map so that it can be re-instanciated on view load.
     * This is iOS specific because the map plugin is buggy on iOS.
     */
    if(this.device.platform != "Android") {
      this.map.destroy();
      this.renderedIds = new Array<number>();
    }
  }

  loadMap() {
    this.platform.ready().then(() => {
      /*
        * Set initial map properties
        */
      let mapOptions: GoogleMapOptions = {
        // mapType: 'MAP_TYPE_ROADMAP',
        camera: {
          target: {lat: (this.lastLatLng != null ? this.lastLatLng.lat : 35.227190), lng: (this.lastLatLng != null ? this.lastLatLng.lat : -80.843097)},
          zoom: 13,
          tilt: 0,
          bearing: 0,
          duration: 5000
        },
        controls: {
          myLocationButton: false,
          indoorPicker: false
        },
        gestures: {
          scroll: true,
          tilt: false,
          rotate: true,
          zoom: true
        },
        preferences: {
          zoom: {
            minZoom: 0,
            maxZoom: 110,
          },
          building: false
        },
        styles: mapStyles
      };

      /*
        * Create the map with the init properties.
        * 
        * We do this before we actually because in the event the location isn't found or they don't
        *   allow location, then at least the app doesn't look busted and not show a 
        *   map. This way it looks more polished.
        */
      this.map = GoogleMaps.create(this.mapReference.nativeElement, mapOptions);

      this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
        this.hideLoadingSvg();
        /*
        * Once your position is attained then move the camera to be centered over the phone's location.
        */
        this.geolocation.getCurrentPosition().then((loc) => {
          this.moveCamera(loc);
        }).catch(err => {
          this.moveCamera(tradeAndTryonLocation);
        });

        this.camMovementSubscription = this.map.on(GoogleMapsEvent.CAMERA_MOVE_END).subscribe((endRes) => {
          let latLngBounds: VisibleRegion = this.map.getVisibleRegion();
          this.renderRestaurants(latLngBounds);
        });
      });
    });
  }

  moveCamera(loc: Geoposition) {
    this.lastLatLng = new LatLng(loc.coords.latitude, loc.coords.longitude);

    this.map.animateCamera({
      target: {lat: loc.coords.latitude, lng: loc.coords.longitude},
      zoom: 13,
      tilt: 0,
      bearing: 0,
      duration: 500
    }).then(() => {

      /*
       * Add a marker for where you are.
       */
      this.map.addMarker({
        icon: {
          url: "assets/images/marker.png",
          size: {
            width: 32,
            height: 32,
          },
          anchor: [16,16]
        },
        position: {
          lat: loc.coords.latitude,
          lng: loc.coords.longitude
        }
      });

      let latLngBounds: VisibleRegion = this.map.getVisibleRegion();
      this.renderRestaurants(latLngBounds);
    });
  }

  renderRestaurants(latLngBounds: VisibleRegion) {
    this.pageSubscription = this.restaurantsFacade.loadInBounds(latLngBounds).subscribe((restaurants) => {
      let temp = new Array<Restaurant>();
        
      temp = restaurants.filter(r => {
        return this.renderedIds.indexOf(r.id) == -1;
      });
    
      let bounds: VisibleRegion = this.map.getVisibleRegion();

      temp.forEach((r) => {
        if (this.isInsideBounds(r, bounds)) {
          this.renderedIds.push(r.id);

          /*
            * Put each of the returned resaurants onto the map as pins
            */
          this.map.addMarker({
            icon: {
              url: "assets/images/pin_icon.png",
              size: {
                width: 32,
                height: 32
              }
            },
            position: {
              lat: r.latitude,
              lng: r.longitude
            }
          }).then((marker: Marker) => {
            marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe((params) => {
              this.ga.trackEvent('map-detail', r.name, 'Map')
              .then(() => {
                this.activeRestaurant = r;
                this.menuCtrl.open();
              });
            });
          });
        }
      });
    });
  }

  restaurant(id: number) {
    this.ga.trackEvent('Map', 'page-load', 'Restaurant', id)
    .then(() => {
      this.navCtrl.push(RestaurantPage, {"restaurant_id": id});
    });
  }

  isInsideBounds(r: Restaurant, bounds: VisibleRegion) {
    return ((r.latitude < bounds.northeast.lat && r.latitude > bounds.southwest.lat) 
    &&
    (r.longitude < bounds.northeast.lng && r.longitude > bounds.southwest.lng));
  }

  dirs(id: number, address: string) {
    this.ga.trackEvent('Map', 'directions', "Restaurant", id)
    .then(() => {
      window.open("geo:0,0?q="+ address, "_system");
    });
  }

  showLoadingSvg() {
    this.loadingSpinner.present();
  }

  hideLoadingSvg() {
    this.loadingSpinner.dismiss();
  }

  tele(id: number, phone: string) {
    this.ga.trackEvent('Restaurant', 'intent', 'Phone', id)
    .then(() => {
      this.callNumber.callNumber(phone, true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
    });
  }

  site(id: number, website: string) {
    this.ga.trackEvent('Restaurant', 'intent', 'Website', id)
    .then(() => {
      window.open(website, '_system', 'location=yes');
    });
  }

}
