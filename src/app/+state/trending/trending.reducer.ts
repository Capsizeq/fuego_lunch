import { ActionReducerMap } from '@ngrx/store';

import { TrendingAction, TrendingActionTypes } from './trending.actions';
import { ApplicationState } from '../app.state';
import { Trending } from '../../core/objects/trending/trending';


export interface TrendingState {
    trending: Trending;
    loading: boolean;
}

export const TRENDING_INITAL_STATE: TrendingState = {
    trending: null,
    loading: false
}

export function trendingReducer(state: TrendingState = TRENDING_INITAL_STATE, action: TrendingAction) {
    switch (action.type) {
        case TrendingActionTypes.LOAD_TRENDING: {
            return { ...state, loading: true }
        }
        case TrendingActionTypes.LOAD_TRENDING_SUCCESS: {
            return { ...state, trending: action.payload, loading: false  };
        }
        case TrendingActionTypes.LOAD_TRENDING_FAILURE: {
            return { ...state, trending: null, loading: false  };
        }
        default: {
            return state;
        }
    }
}

export namespace trendingQueries {
    export const getTrending = (state: ApplicationState) => state.trendingState.trending;
    export const getLoading = (state: ApplicationState) => state.trendingState.loading;
}
