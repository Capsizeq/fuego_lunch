import { Injectable } from '@angular/core';
import { ApplicationState } from '../app.state';
import { TrendingService } from '../../core/services/trending.service';
import { Store } from '@ngrx/store';

import { LoadTrendingAction, LoadTrendingSuccessAction } from './trending.actions';
import { TrendingState, trendingQueries } from './trending.reducer';

@Injectable()
export class TrendingFacade {
    trending$ = this.store.select(trendingQueries.getTrending);
    loading$ = this.store.select(trendingQueries.getLoading);

    constructor(private store: Store<ApplicationState>, 
        private trendingService: TrendingService) {
        this.loadPairs();
    }

    loadPairs() {
        this.store.dispatch(new LoadTrendingAction());
    }

    getWeekdays() {
        return this.trendingService.getWeekdays();
    }

}
