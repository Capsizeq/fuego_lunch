import { Component } from '@angular/core';
import { Platform, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

/* Pages */
import { TabsPage } from './pages/tabs/tabs';
import { SplashPage } from './pages/splash/splash';
import { Cordova } from '@ionic-native/core';
import { Navbar } from 'ionic-angular/components/toolbar/navbar';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { googleAnalyticsTrackingId } from './core/objects/google-analytics-tracking-id';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  private rootPage:any = TabsPage;
  private navBar: any;

  constructor(platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen, 
    modalCtrl: ModalController,
    private ga: GoogleAnalytics) {
      platform.ready().then(() => {
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        statusBar.styleDefault();

        let splash = modalCtrl.create(SplashPage);
        splash.present();

        // statusBar.styleLightContent();
        // let status bar overlay webview
        statusBar.styleDefault();
        statusBar.backgroundColorByHexString("#f2f0ed");

        this.ga.startTrackerWithId(googleAnalyticsTrackingId)
        .then(() => {
          console.log('Google analytics is ready now');
              this.ga.trackView('App Home');
              // Tracker is ready
              // You can now track pages or set additional information such as AppVersion or UserId
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));
      });
  }
}
