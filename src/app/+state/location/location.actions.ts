import { Action } from '@ngrx/store';
import { Geoposition } from '@ionic-native/geolocation';

export enum LocationActionTypes {
    SAVE_LOCATION = '[LOCATION] Save Location Action',
    SAVE_LOCATION_SUCCESS = '[LOCATION] Save Location Success',
    SAVE_LOCATION_FAILURE = '[LOCATION] Save Location Failure',
    SAVE_RADIUS = '[LOCATION] Save radius action'
}

export class SaveLocationAction implements Action {
    readonly type = LocationActionTypes.SAVE_LOCATION;

    constructor(public payload: Geoposition) { }
}

export class SaveLocationSuccessAction implements Action {
    readonly type = LocationActionTypes.SAVE_LOCATION_SUCCESS;
}

export class SaveLocationFailureAction implements Action {
    readonly type = LocationActionTypes.SAVE_LOCATION_FAILURE;
}

export type LocationAction
  = SaveLocationAction
  | SaveLocationSuccessAction
  | SaveLocationFailureAction;
