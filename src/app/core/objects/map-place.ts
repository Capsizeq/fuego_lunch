export class MapPlace {
    name: string;
    address: string;
    phone: string;
    lat: number;
    lng: number;
    img: string;
    specials: any;

    constructor() {}
};