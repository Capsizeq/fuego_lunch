import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
// import { NavComponent } from './nav/nav.component';
import { FormsModule } from '@angular/forms';

@NgModule({
	declarations: [
		//NavComponent
	],
	imports: [
		FormsModule
	],
	exports: [
		//NavComponent
	]
})
export class ComponentsModule {}
