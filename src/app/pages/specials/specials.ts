import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController, Platform, ViewController, Events } from 'ionic-angular';
import { RestaurantService } from '../../core/services/restaurant.service';
import { Geolocation } from '@ionic-native/geolocation';
import { SpecialCategory } from '../../core/objects/specials/special-category';
import { Restaurant } from '../../core/objects/restaurant';
import { SpecialsService } from '../../core/services/specials.service';
import { RestaurantPage } from '../restaurant/restaurant';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Loading } from 'ionic-angular/components/loading/loading';
import { splash } from '../../../assets/svgs/splash.svg';
import { DomSanitizer } from '@angular/platform-browser';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { googleAnalyticsTrackingId } from '../../core/objects/google-analytics-tracking-id';
import { tradeAndTryonLocation } from '../../core/jsons/charlotte-location';

/**
 * Generated class for the SpecialsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-specials',
  templateUrl: 'specials.html',
})
export class SpecialsPage {
  private specialGroupsList: Observable<Array<SpecialCategory>>;
  private searchTerm: string;
  private searchedFor: string;
  private distance: number;
  private groupBy: string;
  private searchSub: Subscription;
  private loadingSpinner: Loading;
  private loadingSvg: any;
  private loadingSub: Subscription;
  private hideGroupByBtn: boolean;
  private hideDistanceBtn: boolean;


  constructor(
    private ga: GoogleAnalytics,
    private platform: Platform,
    public navCtrl: NavController, 
    public navParams: NavParams,
    private restaurantService: RestaurantService,
    private specialsService: SpecialsService,
    private sanitizer: DomSanitizer,
    public viewCtrl: ViewController,
    private geolocation: Geolocation,
    private menuCtrl: MenuController,
    private events: Events) {

      /* this.ga.startTrackerWithId(googleAnalyticsTrackingId)
      .then(() => {
        console.log('Google analytics is ready now');
            this.ga.trackView("Specials Page");
            // Tracker is ready
            // You can now track pages or set additional information such as AppVersion or UserId
      })
      .catch(e => console.log('Error starting GoogleAnalytics', e)); */

      /*
       * Init variables
       */
      this.searchTerm = "";
      this.distance = 5;
      this.groupBy = 'category';
      this.hideGroupByBtn = true;
      this.hideDistanceBtn = true;
      this.searchSub = new Subscription();

      this.loadingSvg = this.sanitizer.bypassSecurityTrustHtml(splash);

      events.subscribe('search:term', (searchTerm: string) => {
        this.searchTerm = searchTerm;
        this.searchedFor = searchTerm;
        this.search();
      });
  }

  ionViewWillEnter() {
    /*
     * Enable correct menu
     */
    this.menuCtrl.enable(true, 'specialsmenu');
    this.menuCtrl.enable(false, 'browsemenu');
    this.menuCtrl.enable(false, 'mapmenu');
  }

  ionViewDidEnter() {
    /* this.searchTerm = this.navParams.get('search_term') != undefined ? this.navParams.get('search_term') : "";
    console.log(this.navParams.data); */
  }

  ionViewWillLeave() {
    this.menuCtrl.close();
    this.viewCtrl.dismiss();
  }

  restaurant(id: number) {
    // Going to the Restaurant Page, and logging it with GA.
    this.ga.trackEvent('Specials', 'page-load', 'Restaurant', id)
    .then(() => {
      this.navCtrl.push(RestaurantPage, {"restaurant_id": id});
    });
  }

  changeDistance(event: any) {
    this.hideDistanceBtn = false;
    this.ga.trackEvent('Specials', 'filter-change', 'distance', this.distance)
    .then(() => {
      if(this.searchTerm != "") {
        this.search();
      }
    });
  }

  changeGroupBy(event: any) {
    this.hideGroupByBtn = false;
    this.ga.trackEvent('Specials', 'filter-change', 'distance', this.distance)
    .then(() => {
      if(this.searchTerm != "") {
        this.search();
      }
    });
  }

  search() {
    if(this.searchTerm == "") {
      this.specialGroupsList = Observable.of(new Array<SpecialCategory>());
    } else {
      this.ga.trackEvent('Specials', 'search', this.searchTerm)
      .then(() => {
        this.searchedFor = this.searchTerm;
        this.geolocation.getCurrentPosition().then((loc) => {
          this.specialGroupsList = this.specialsService.getCategorizedSpecials(loc.coords.latitude, loc.coords.longitude, this.searchTerm, this.distance, this.groupBy);
        }).catch(err => {
          this.specialGroupsList = this.specialsService.getCategorizedSpecials(tradeAndTryonLocation.coords.latitude, tradeAndTryonLocation.coords.longitude, this.searchTerm, this.distance, this.groupBy);
        });
      });
    }
  }

  disableDistanceFilterBtn() {
    this.hideDistanceBtn = true;
    this.distance = 5;
  }

  disableGroupByFilterBtn() {
    this.hideGroupByBtn = true;
    this.groupBy = "category";
  }

}
