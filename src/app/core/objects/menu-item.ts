export class MenuItem {
    id: number;
    menu_id: number;
    title: string;
    description: string;
    price: string;
    tag: string;
    is_on_special = false;
}