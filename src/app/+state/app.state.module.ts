import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { TrendingEffects } from './trending/trending.effects';
import { RestaurantEffects } from './restaurant/restaurant.effects';
import { TrendingFacade } from './trending/trending.facade';
import { RestaurantFacade } from './restaurant/restaurant.facade';
import { LocationEffects } from './location/location.effects';
import { BrowseEffects } from './browse/browse.effects';
import { LocationFacade } from './location/location.facade';
import { BrowseFacade } from './browse/browse.facade';

@NgModule({
    imports: [
        EffectsModule.forRoot([
            TrendingEffects,
            RestaurantEffects,
            LocationEffects,
            BrowseEffects
        ]),
    ],
    providers: [
        TrendingFacade,
        RestaurantFacade,
        LocationFacade,
        BrowseFacade
    ]
})

export class AppStateModule {}
