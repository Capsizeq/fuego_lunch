import { Component, ViewChild, ElementRef, Renderer } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { TrendingService } from '../../core/services/trending.service';
import { TrendingFacade } from '../../+state/trending/trending.facade';
import { Observable } from 'rxjs/Observable';
import { Trending } from '../../core/objects/trending/trending';
import { googleAnalyticsTrackingId } from '../../core/objects/google-analytics-tracking-id';
import { RestaurantPage } from '../restaurant/restaurant';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { SpecialsPage } from '../specials/specials';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Device } from '@ionic-native/device';

/**
 * Generated class for the TrendingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trending',
  templateUrl: 'trending.html',
})
export class TrendingPage {
  @ViewChild('searchbar') searchbar;

  weekday: Array<string>;
  trending$: Observable<Trending>;
  loading$: Observable<boolean>;
  headerSearchTerm: string;
  focused: string;
  
  constructor(
    private ga: GoogleAnalytics,
    private platform: Platform,
    public navCtrl: NavController, 
    public navParams: NavParams,
    private device: Device,
    private trendingFacade: TrendingFacade,
    private diagnostic: Diagnostic,
    private renderer: Renderer) {
  
      this.trending$ = this.trendingFacade.trending$;
      this.loading$ = this.trendingFacade.loading$;
      this.weekday = this.trendingFacade.getWeekdays();

      this.focused = "";

      this.ga.startTrackerWithId(googleAnalyticsTrackingId)
        .then(() => {
          console.log('Google analytics is ready now');
              this.ga.trackView("Trending Page");
              // Tracker is ready
              // You can now track pages or set additional information such as AppVersion or UserId
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));
  }

  ionViewDidEnter() {
    this.getPermission();
  }

  get day() {
    let d = new Date();
    return this.weekday[d.getDay()];
  }

  doRefresh(refresher) {
    this.ga.trackEvent('Trending', 'refresh', 'Refresher')
    .then(() => {
      refresher.complete();
    });
  }

  toGoRestaurant(id: number) {
    // Going to the Restaurant Page, and logging it with GA.
   this.ga.trackEvent('Trending', 'page-load', 'Restaurant', id)
    .then(() => {
      this.navCtrl.push(RestaurantPage, {"restaurant_id": id});
    });
  }

  getPermission() {
    this.diagnostic.getPermissionAuthorizationStatus(this.diagnostic.permission.ACCESS_FINE_LOCATION).then((status) => {
      console.log(`AuthorizationStatus`);
      console.log(status);
      if (status != this.diagnostic.permissionStatus.GRANTED) {
        this.diagnostic.requestRuntimePermission(this.diagnostic.permission.ACCESS_FINE_LOCATION).then((data) => {
          console.log(`getFineLocationAuthorizationStatus`);
          console.log(data);
        })
      } else {
        console.log("We have the permission");
      }
    }, (statusError) => {
      console.log(`statusError`);
      console.log(statusError);
    });
  }

}
