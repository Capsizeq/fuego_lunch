import { Component, ViewChild, ElementRef } from '@angular/core';

import { MapPage } from '../map/map';
import { BrowsePage } from '../browse/browse';
import { TrendingPage } from '../trending/trending';
import { NavController, Events, Tabs } from 'ionic-angular';
import { SpecialsPage } from '../specials/specials';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  @ViewChild('myTabs') tabRef: Tabs;

  trending = TrendingPage;
  map = MapPage;
  browse = BrowsePage;
  search = SpecialsPage;
  searchTerm: string;

  constructor(private navCtrl: NavController,
    private events: Events) {

      events.subscribe('nav:click', (name: string) => {

        if(name.indexOf("map") > -1) {
          this.searchTerm = "";
          this.events.publish("search:term", this.searchTerm);
          this.tabRef.select(0);
        } else if(name.indexOf("trending") > -1) {
          this.searchTerm = "";
          this.events.publish("search:term", this.searchTerm);
          this.tabRef.select(1);
        } else if(name.indexOf("browse") > -1) {
          this.searchTerm = "";
          this.events.publish("search:term", this.searchTerm);
          this.tabRef.select(2);
        }
      });
      
      events.subscribe("search:navigate", (searchTerm: string) => {
        this.searchTerm = searchTerm;
        this.tabRef.select(3).then(() => {
          this.events.publish("search:term", this.searchTerm);
        });
      });
  }

  tabSwitched() {
    
  }
}
