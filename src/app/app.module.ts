import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { GoogleMaps } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';

/* Pages */
import { MapPage } from './pages/map/map';
import { BrowsePage } from './pages/browse/browse';
import { TabsPage } from './pages/tabs/tabs';
import { SplashPage } from './pages/splash/splash';
import { TrendingPage } from './pages/trending/trending';
import { RestaurantPage } from './pages/restaurant/restaurant';
import { SpecialsPage } from './pages/specials/specials';

/* Ionic Stuff */
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

/* ngrx Stuff */
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ROOT_REDUCER, META_REDUCERS } from './+state/app.state';
import { AppStateModule } from './+state/app.state.module';
import { CoreModule } from './core/core.module';
import { NxModule } from '@nrwl/nx';

/* Error Handling */
import { Pro } from '@ionic/pro';
import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { IonicErrorHandler } from 'ionic-angular';


import { Device } from '@ionic-native/device';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { CallNumber } from '@ionic-native/call-number';
import { CustomHttpInterceptor } from './core/http/http.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Diagnostic } from '@ionic-native/diagnostic';


import { ComponentsModule } from './components/components.module';
import { HeaderComponent } from './pages/header/header.component';

Pro.init('bdfe7d4a', {
  appVersion: '0.0.1'
})

@Injectable()
export class MyErrorHandler implements ErrorHandler {
  ionicErrorHandler: IonicErrorHandler;

  constructor(injector: Injector) {
    try {
      this.ionicErrorHandler = injector.get(IonicErrorHandler);
    } catch(e) {
      // Unable to get the IonicErrorHandler provider, ensure
      // IonicErrorHandler has been added to the providers list below
    }
  }

  handleError(err: any): void {
    Pro.monitoring.handleNewError(err);
    // Remove this if you want to disable Ionic's auto exception handling
    // in development mode.
    this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
  }
}

@NgModule({
  declarations: [
    MyApp,
    MapPage,
    BrowsePage,
    TrendingPage,
    TabsPage,
    SplashPage,
    RestaurantPage,
    SpecialsPage,
    HeaderComponent
  ],
  imports: [
    AppStateModule,
    BrowserModule,
    CoreModule,
    ComponentsModule,
    IonicModule.forRoot(MyApp),
    NxModule.forRoot(),
    StoreModule.forRoot(ROOT_REDUCER, {metaReducers: META_REDUCERS}),
    StoreDevtoolsModule.instrument({maxAge: 25})
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MapPage,
    BrowsePage,
    TrendingPage,
    RestaurantPage,
    SpecialsPage,
    TabsPage,
    SplashPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    GoogleMaps,
    Geolocation,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    IonicErrorHandler,
    Device,
    GoogleAnalytics,
    CallNumber,
    Diagnostic,
    // { provide: ErrorHandler, useClass: MyErrorHandler },
    // { provide: DEFAULT_TIMEOUT, useValue: defaultTimeout },
    { provide: HTTP_INTERCEPTORS, useClass: CustomHttpInterceptor, multi: true }
  ]
})
export class AppModule {}
