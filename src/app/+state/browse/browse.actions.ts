import { Action } from '@ngrx/store';

export enum BrowseActionTypes {
    SAVE_RADIUS = '[LOCATION] Save radius action',
    SAVE_PAGINATION_INDEX = '[BROWSE] Save pagination index action'
}

export class SaveRadiusAction implements Action {
    readonly type = BrowseActionTypes.SAVE_RADIUS;

    constructor(public payload: number) { }
}

export class SavePaginationIndexAction implements Action {
    readonly type = BrowseActionTypes.SAVE_PAGINATION_INDEX;

    constructor(public payload: number) { }
}

export type BrowseAction
  = SaveRadiusAction
  | SavePaginationIndexAction;
