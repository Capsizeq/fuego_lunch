import { Component } from '@angular/core';
import { IonicPage, ViewController, Platform, ToastController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Trending } from '../../core/objects/trending/trending';
import { TrendingFacade } from '../../+state/trending/trending.facade';
import { Observable } from 'rxjs/Observable';
import { googleAnalyticsTrackingId } from '../../core/objects/google-analytics-tracking-id';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { DomSanitizer } from '@angular/platform-browser';
import { splash } from '../../../assets/svgs/splash.svg';
import { Subscription } from 'rxjs/Subscription';
import { Device } from '@ionic-native/device';
/**
 * Generated class for the SplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-splash',
  templateUrl: 'splash.html'
})
export class SplashPage {
  private trending$: Observable<Trending>;
  private loadingSvg: any;
  private server502: boolean;
  private server404: boolean;
  private sub: Subscription;
  private backgroundUrl: string;
  private counter=0;

  constructor(
    private ga: GoogleAnalytics,
    public viewCtrl: ViewController,
    public splashScreen: SplashScreen,
    private trendingFacade: TrendingFacade,
    private device: Device,
    private sanitizer: DomSanitizer,
    private platform: Platform,
    private toastCtrl: ToastController) {
      this.backgroundUrl = "assets/images/splash/android/drawable-port-xxhdpi-screen.png";

      this.platform.ready().then((readySource) => {

        if(this.device.platform == "Android") {
          if(this.platform.width() <= 240) {
            this.backgroundUrl = "assets/images/splash/android/drawable-port-ldpi-screen.png";
          } else if(this.platform.width() <= 320) {
            this.backgroundUrl = "assets/images/splash/android/drawable-port-mdpi-screen.png";
          } else if(this.platform.width() <= 480) {
            this.backgroundUrl = "assets/images/splash/android/drawable-port-hdpi-screen.png";
          } else if(this.platform.width() <= 720) {
            this.backgroundUrl = "assets/images/splash/android/drawable-port-xhdpi-screen.png";
          } else if(this.platform.width() <= 960) {
            this.backgroundUrl = "assets/images/splash/android/drawable-port-xxhdpi-screen.png";
          } else {
            this.backgroundUrl = "assets/images/splash/android/drawable-port-xxxhdpi-screen.png";
          }
        } else {
          this.backgroundUrl = "assets/images/splash/ios/Default@2x~universal~anyany.png";
        }

        /* platform.registerBackButtonAction(() => {
          if (this.counter == 0) {
            this.counter++;
            this.presentToast();
            setTimeout(() => { this.counter = 0 }, 3000)
          } else {
            // console.log("exitapp");
            platform.exitApp();
          }
        }, 0) */
        
      });

      this.server404 = false;
      this.server502 = false;

      this.trending$ = this.trendingFacade.trending$;

      this.loadingSvg = this.sanitizer.bypassSecurityTrustHtml(splash);
  }

  ionViewWillEnter() {
    // this.viewCtrl.showBackButton(false);
  }

  ionViewDidEnter() {

    this.splashScreen.hide();

    this.sub = this.trending$.subscribe(trending => {
      if(trending != null) {
        if(trending.lists[0].header == "502") {
          this.server502 = true;
        } else if (trending.lists[0].header == "404") {
          this.server404 = true;
        } else {
          this.viewCtrl.dismiss();
        }
      }
    });
    
  }

  ionViewWillLeave() {
    this.sub.unsubscribe();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Press again to exit",
      duration: 3000,
      position: "middle"
    });
    toast.present();
  }

}
