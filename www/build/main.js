webpackJsonp([4],{

/***/ 125:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocationFacade; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__location_reducer__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_location_service__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__location_actions__ = __webpack_require__(126);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LocationFacade = /** @class */ (function () {
    function LocationFacade(store, geolocation, locationService) {
        this.store = store;
        this.geolocation = geolocation;
        this.locationService = locationService;
        this.currentLocation$ = this.store.select(__WEBPACK_IMPORTED_MODULE_2__location_reducer__["a" /* locationQueries */].getCurrentLocation);
    }
    LocationFacade.prototype.loadLocation = function (loc) {
        var _this = this;
        this.currentLocation$.subscribe(function (curLoc) {
            /*
             * If `loc` is null then that means that this is the first time that the
             * location has been loaded, so just save it.
             */
            if (curLoc == null)
                _this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_6__location_actions__["b" /* SaveLocationAction */](loc));
            else {
                var lastLatLng = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["c" /* LatLng */](curLoc.coords.latitude, curLoc.coords.longitude);
                var curLatLng = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["c" /* LatLng */](loc.coords.latitude, loc.coords.longitude);
                /*
                 * Don't redo any of the location saving action unless we've traveled more than 1/4 mile.
                 * This is because location saves also update all restaurant distances in store, which is battery
                 * consuming if we're doing it all the time.
                 */
                //console.log("dist since last thing: ", conversions.toWholeMile(Spherical.computeDistanceBetween(curLatLng, lastLatLng)));
                if (true) //(conversions.toWholeMile(Spherical.computeDistanceBetween(curLatLng, lastLatLng)) > .25)
                    _this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_6__location_actions__["b" /* SaveLocationAction */](loc));
            }
        }).unsubscribe();
    };
    LocationFacade.prototype.refreshLocation = function (loc) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_6__location_actions__["b" /* SaveLocationAction */](loc));
    };
    LocationFacade = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_3__core_services_location_service__["a" /* LocationService */]])
    ], LocationFacade);
    return LocationFacade;
}());

//# sourceMappingURL=location.facade.js.map

/***/ }),

/***/ 126:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocationActionTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return SaveLocationAction; });
/* unused harmony export SaveLocationSuccessAction */
/* unused harmony export SaveLocationFailureAction */
var LocationActionTypes;
(function (LocationActionTypes) {
    LocationActionTypes["SAVE_LOCATION"] = "[LOCATION] Save Location Action";
    LocationActionTypes["SAVE_LOCATION_SUCCESS"] = "[LOCATION] Save Location Success";
    LocationActionTypes["SAVE_LOCATION_FAILURE"] = "[LOCATION] Save Location Failure";
    LocationActionTypes["SAVE_RADIUS"] = "[LOCATION] Save radius action";
})(LocationActionTypes || (LocationActionTypes = {}));
var SaveLocationAction = /** @class */ (function () {
    function SaveLocationAction(payload) {
        this.payload = payload;
        this.type = LocationActionTypes.SAVE_LOCATION;
    }
    return SaveLocationAction;
}());

var SaveLocationSuccessAction = /** @class */ (function () {
    function SaveLocationSuccessAction() {
        this.type = LocationActionTypes.SAVE_LOCATION_SUCCESS;
    }
    return SaveLocationSuccessAction;
}());

var SaveLocationFailureAction = /** @class */ (function () {
    function SaveLocationFailureAction() {
        this.type = LocationActionTypes.SAVE_LOCATION_FAILURE;
    }
    return SaveLocationFailureAction;
}());

//# sourceMappingURL=location.actions.js.map

/***/ }),

/***/ 127:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__state_restaurant_restaurant_actions__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__state_location_location_reducer__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__state_restaurant_restaurant_reducer__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__state_conversions__ = __webpack_require__(351);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__state_browse_browse_reducer__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_geolocation__ = __webpack_require__(49);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var LocationService = /** @class */ (function () {
    function LocationService(geolocation, store) {
        this.geolocation = geolocation;
        this.store = store;
        this.currentLocation$ = this.store.select(__WEBPACK_IMPORTED_MODULE_3__state_location_location_reducer__["a" /* locationQueries */].getCurrentLocation);
        this.radius$ = this.store.select(__WEBPACK_IMPORTED_MODULE_7__state_browse_browse_reducer__["a" /* browseQueries */].getRadius);
        this.restuarants$ = this.store.select(__WEBPACK_IMPORTED_MODULE_4__state_restaurant_restaurant_reducer__["a" /* restaurantQueries */].getRestuarants);
    }
    LocationService.prototype.updateRestaurantsDistanes = function (loc) {
        var _this = this;
        this.restuarants$.subscribe(function (restaurants) {
            var tempRestaurants = new Array();
            var inMemoryNearByRestaurantIdList = new Array();
            /*
            * For each restaurant, update it's distance from the user's phone
            */
            restaurants.forEach(function (r) {
                var restaurantLatLng = new __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["c" /* LatLng */](r.latitude, r.longitude);
                var currentLatLng = new __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["c" /* LatLng */](loc.coords.latitude, loc.coords.longitude);
                /*
                * The computeDistanceBetween function returns a value in meters, so we convert to miles
                */
                var distance = __WEBPACK_IMPORTED_MODULE_6__state_conversions__["a" /* conversions */].toWholeMile(__WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["d" /* Spherical */].computeDistanceBetween(restaurantLatLng, currentLatLng));
                tempRestaurants.push(__assign({}, r, { distance: distance }));
                inMemoryNearByRestaurantIdList.push(r.id);
            });
            _this.radius$.subscribe(function (radius) {
                _this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__state_restaurant_restaurant_actions__["a" /* LoadNearByRestaurantsAction */]({ "radius": radius,
                    "latitude": loc.coords.latitude,
                    "longitude": loc.coords.longitude,
                    "idsInMem": inMemoryNearByRestaurantIdList }));
            }).unsubscribe();
            _this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__state_restaurant_restaurant_actions__["j" /* ResaveRestaurantsSuccessAction */](tempRestaurants));
        }).unsubscribe();
    };
    LocationService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_8__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */]])
    ], LocationService);
    return LocationService;
}());

//# sourceMappingURL=location.service.js.map

/***/ }),

/***/ 129:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BrowseFacade; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__browse_actions__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__location_location_facade__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__restaurant_restaurant_reducer__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__restaurant_restaurant_actions__ = __webpack_require__(60);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var BrowseFacade = /** @class */ (function () {
    function BrowseFacade(store, locationFacade) {
        this.store = store;
        this.locationFacade = locationFacade;
        this.restaurants$ = this.store.select(__WEBPACK_IMPORTED_MODULE_4__restaurant_restaurant_reducer__["a" /* restaurantQueries */].getRestuarants);
    }
    BrowseFacade.prototype.saveRadius = function (radius, loc) {
        var inMemoryNearByRestaurantIdList = new Array();
        this.restaurants$.subscribe(function (restaurants) {
            restaurants.forEach(function (r) {
                inMemoryNearByRestaurantIdList.push(r.id);
            });
        }).unsubscribe();
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_5__restaurant_restaurant_actions__["a" /* LoadNearByRestaurantsAction */]({
            "radius": radius,
            "latitude": loc.coords.latitude,
            "longitude": loc.coords.longitude,
            "idsInMem": inMemoryNearByRestaurantIdList
        }));
    };
    BrowseFacade.prototype.savePaginationIndex = function (index) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__browse_actions__["b" /* SavePaginationIndexAction */](index));
    };
    BrowseFacade = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_3__location_location_facade__["a" /* LocationFacade */]])
    ], BrowseFacade);
    return BrowseFacade;
}());

//# sourceMappingURL=browse.facade.js.map

/***/ }),

/***/ 134:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrendingFacade; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_services_trending_service__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__trending_actions__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__trending_reducer__ = __webpack_require__(138);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TrendingFacade = /** @class */ (function () {
    function TrendingFacade(store, trendingService) {
        this.store = store;
        this.trendingService = trendingService;
        this.trending$ = this.store.select(__WEBPACK_IMPORTED_MODULE_4__trending_reducer__["b" /* trendingQueries */].getTrending);
        this.loading$ = this.store.select(__WEBPACK_IMPORTED_MODULE_4__trending_reducer__["b" /* trendingQueries */].getLoading);
        this.loadPairs();
    }
    TrendingFacade.prototype.loadPairs = function () {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_3__trending_actions__["a" /* LoadTrendingAction */]());
    };
    TrendingFacade.prototype.getWeekdays = function () {
        return this.trendingService.getWeekdays();
    };
    TrendingFacade = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_1__core_services_trending_service__["a" /* TrendingService */]])
    ], TrendingFacade);
    return TrendingFacade;
}());

//# sourceMappingURL=trending.facade.js.map

/***/ }),

/***/ 135:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrendingService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__jsons_trending_trending502_json__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__jsons_trending_trending404_json__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__jsons_trending_trendingEmpty_json__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_switchMap__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_switchMap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_switchMap__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var TrendingService = /** @class */ (function () {
    function TrendingService(http) {
        this.http = http;
        this.apiUrl = "https://beta.fuegospecials.com/api";
    }
    TrendingService.prototype.getTrending = function () {
        // return Observable.of(trending);
        try {
            return this.http.get(this.apiUrl + "/trending")
                .catch(function (err) {
                console.log("error loading trending");
                // simple logging, but you can do a lot more, see below
                console.error('An error occurred:', err.error.contina);
                if (err.error.toString().indexOf('404 Not Found') > -1) {
                    console.log("404 loading trending");
                    return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].of(__WEBPACK_IMPORTED_MODULE_4__jsons_trending_trending404_json__["a" /* trending404 */]);
                }
                else if (err.error.toString().indexOf('502 Bad Gateway') > -1) {
                    console.log("502 loading trending");
                    return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].of(__WEBPACK_IMPORTED_MODULE_3__jsons_trending_trending502_json__["a" /* trending502 */]);
                }
                return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].of(__WEBPACK_IMPORTED_MODULE_5__jsons_trending_trendingEmpty_json__["a" /* trendingEmpty */]);
            });
        }
        catch (err) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].of(__WEBPACK_IMPORTED_MODULE_5__jsons_trending_trendingEmpty_json__["a" /* trendingEmpty */]);
        }
    };
    TrendingService.prototype.getWeekdays = function () {
        var weekdays = new Array(7);
        weekdays[0] = "Sunday";
        weekdays[1] = "Monday";
        weekdays[2] = "Tuesday";
        weekdays[3] = "Wednesday";
        weekdays[4] = "Thursday";
        weekdays[5] = "Friday";
        weekdays[6] = "Saturday";
        return weekdays;
    };
    TrendingService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */]])
    ], TrendingService);
    return TrendingService;
}());

//# sourceMappingURL=trending.service.js.map

/***/ }),

/***/ 137:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return TrendingActionTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadTrendingAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return LoadTrendingSuccessAction; });
/* unused harmony export LoadTrendingFailureAction */
var TrendingActionTypes;
(function (TrendingActionTypes) {
    TrendingActionTypes["LOAD_TRENDING"] = "[TRENDING] Load Trending Action";
    TrendingActionTypes["LOAD_TRENDING_SUCCESS"] = "[TRENDING] Load Trending Success";
    TrendingActionTypes["LOAD_TRENDING_FAILURE"] = "[TRENDING] Load Trending Failure";
})(TrendingActionTypes || (TrendingActionTypes = {}));
var LoadTrendingAction = /** @class */ (function () {
    function LoadTrendingAction() {
        this.type = TrendingActionTypes.LOAD_TRENDING;
    }
    return LoadTrendingAction;
}());

var LoadTrendingSuccessAction = /** @class */ (function () {
    function LoadTrendingSuccessAction(payload) {
        this.payload = payload;
        this.type = TrendingActionTypes.LOAD_TRENDING_SUCCESS;
    }
    return LoadTrendingSuccessAction;
}());

var LoadTrendingFailureAction = /** @class */ (function () {
    function LoadTrendingFailureAction() {
        this.type = TrendingActionTypes.LOAD_TRENDING_FAILURE;
    }
    return LoadTrendingFailureAction;
}());

//# sourceMappingURL=trending.actions.js.map

/***/ }),

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TRENDING_INITAL_STATE; });
/* harmony export (immutable) */ __webpack_exports__["c"] = trendingReducer;
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return trendingQueries; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__trending_actions__ = __webpack_require__(137);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};

var TRENDING_INITAL_STATE = {
    trending: null,
    loading: false
};
function trendingReducer(state, action) {
    if (state === void 0) { state = TRENDING_INITAL_STATE; }
    switch (action.type) {
        case __WEBPACK_IMPORTED_MODULE_0__trending_actions__["c" /* TrendingActionTypes */].LOAD_TRENDING: {
            return __assign({}, state, { loading: true });
        }
        case __WEBPACK_IMPORTED_MODULE_0__trending_actions__["c" /* TrendingActionTypes */].LOAD_TRENDING_SUCCESS: {
            return __assign({}, state, { trending: action.payload, loading: false });
        }
        case __WEBPACK_IMPORTED_MODULE_0__trending_actions__["c" /* TrendingActionTypes */].LOAD_TRENDING_FAILURE: {
            return __assign({}, state, { trending: null, loading: false });
        }
        default: {
            return state;
        }
    }
}
var trendingQueries;
(function (trendingQueries) {
    trendingQueries.getTrending = function (state) { return state.trendingState.trending; };
    trendingQueries.getLoading = function (state) { return state.trendingState.loading; };
})(trendingQueries || (trendingQueries = {}));
//# sourceMappingURL=trending.reducer.js.map

/***/ }),

/***/ 139:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tradeAndTryonLocation; });
var tradeAndTryonLocation = {
    coords: {
        accuracy: 0,
        altitude: 0,
        altitudeAccuracy: 0,
        heading: 0,
        latitude: 35.3555525,
        longitude: -80.7697531,
        speed: 0
    },
    timestamp: 0,
    accuracy: ""
};
//# sourceMappingURL=charlotte-location.js.map

/***/ }),

/***/ 165:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrendingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__state_trending_trending_facade__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_objects_google_analytics_tracking_id__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__restaurant_restaurant__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_analytics__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_diagnostic__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_device__ = __webpack_require__(82);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the TrendingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TrendingPage = /** @class */ (function () {
    function TrendingPage(ga, platform, navCtrl, navParams, device, trendingFacade, diagnostic, renderer) {
        var _this = this;
        this.ga = ga;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.device = device;
        this.trendingFacade = trendingFacade;
        this.diagnostic = diagnostic;
        this.renderer = renderer;
        this.trending$ = this.trendingFacade.trending$;
        this.loading$ = this.trendingFacade.loading$;
        this.weekday = this.trendingFacade.getWeekdays();
        this.focused = "";
        this.ga.startTrackerWithId(__WEBPACK_IMPORTED_MODULE_3__core_objects_google_analytics_tracking_id__["a" /* googleAnalyticsTrackingId */])
            .then(function () {
            console.log('Google analytics is ready now');
            _this.ga.trackView("Trending Page");
            // Tracker is ready
            // You can now track pages or set additional information such as AppVersion or UserId
        })
            .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
    }
    TrendingPage.prototype.ionViewDidEnter = function () {
        this.getPermission();
    };
    Object.defineProperty(TrendingPage.prototype, "day", {
        get: function () {
            var d = new Date();
            return this.weekday[d.getDay()];
        },
        enumerable: true,
        configurable: true
    });
    TrendingPage.prototype.doRefresh = function (refresher) {
        this.ga.trackEvent('Trending', 'refresh', 'Refresher')
            .then(function () {
            refresher.complete();
        });
    };
    TrendingPage.prototype.toGoRestaurant = function (id) {
        var _this = this;
        // Going to the Restaurant Page, and logging it with GA.
        this.ga.trackEvent('Trending', 'page-load', 'Restaurant', id)
            .then(function () {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__restaurant_restaurant__["a" /* RestaurantPage */], { "restaurant_id": id });
        });
    };
    TrendingPage.prototype.getPermission = function () {
        var _this = this;
        this.diagnostic.getPermissionAuthorizationStatus(this.diagnostic.permission.ACCESS_FINE_LOCATION).then(function (status) {
            console.log("AuthorizationStatus");
            console.log(status);
            if (status != _this.diagnostic.permissionStatus.GRANTED) {
                _this.diagnostic.requestRuntimePermission(_this.diagnostic.permission.ACCESS_FINE_LOCATION).then(function (data) {
                    console.log("getFineLocationAuthorizationStatus");
                    console.log(data);
                });
            }
            else {
                console.log("We have the permission");
            }
        }, function (statusError) {
            console.log("statusError");
            console.log(statusError);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('searchbar'),
        __metadata("design:type", Object)
    ], TrendingPage.prototype, "searchbar", void 0);
    TrendingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-trending',template:/*ion-inline-start:"C:\Users\Pidgeon\Desktop\Codes\src\app\pages\trending\trending.html"*/'<ion-header>\n\n    <ion-navbar color="offWhite">\n\n        <f-header></f-header>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="trending-container outer-content">\n\n    <ng-container *ngIf="trending$ | async; let trending; else loading">\n\n        <ion-card>\n\n            <ion-slides autoplay="3000" paginationType="fraction" pager="true" loop="true">\n\n                <ng-container *ngFor="let slide of trending.carouselItems">\n\n                    <ion-slide [ngStyle]="{\'background-image\': \'url(\' + (slide.image != \'\'  ? slide.image : \'assets/images/banners/default.jpg\') + \')\'}" (click)="toGoRestaurant(slide.restaurant_id)">\n\n                        <ng-container *ngIf="slide.ad == true">\n\n                            <div class="ad">\n\n                                <span>Ad</span>\n\n                            </div>\n\n                        </ng-container>\n\n                        <div class="details">\n\n                            <span class="restaurant-name">{{slide.restaurant_name}}</span>\n\n                            <span class="menu-item-name">\n\n                                {{slide.menu_item_name}}\n\n                                <span class="middot">&#8226;</span>\n\n                                {{slide.price}}\n\n                            </span>\n\n                        </div>\n\n                    </ion-slide>\n\n                </ng-container>\n\n            </ion-slides>\n\n        </ion-card>\n\n        <ion-content class="list-scrolling-content">\n\n            <ion-refresher (ionRefresh)="doRefresh($event)">\n\n                <ion-refresher-content></ion-refresher-content>\n\n            </ion-refresher>\n\n            <div class="list-container" *ngFor="let list of trending.lists">\n\n                <div class="list-header">\n\n                    <span>\n\n                        {{list.header}}\n\n                    </span>\n\n                </div>\n\n                <div class="list">\n\n                    <ng-container *ngFor="let item of list.listItems; let odd = odd">\n\n                        <ion-card class="special-card">\n\n                            <div class="special" (click)="toGoRestaurant(item.restaurant_id)">\n\n                                <span class="special-img"></span>\n\n                                <div class="special-content">\n\n                                    <span class="restaurant-name">\n\n                                        {{item.restaurant_name}}\n\n                                    </span>\n\n                                    <span class="item-name">\n\n                                        {{item.menu_item_name}}\n\n                                    </span>\n\n                                    <span class="price">\n\n                                        {{item.price}}\n\n                                    </span>\n\n                                </div>\n\n                            </div>\n\n                        </ion-card>\n\n                    </ng-container>\n\n                </div>\n\n            </div>\n\n        </ion-content>\n\n    </ng-container>\n\n    <ng-template #loading><div class="loading-noms">Loading Noms...</div></ng-template>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Pidgeon\Desktop\Codes\src\app\pages\trending\trending.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_native_google_analytics__["a" /* GoogleAnalytics */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_device__["a" /* Device */],
            __WEBPACK_IMPORTED_MODULE_2__state_trending_trending_facade__["a" /* TrendingFacade */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_diagnostic__["a" /* Diagnostic */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["X" /* Renderer */]])
    ], TrendingPage);
    return TrendingPage;
}());

//# sourceMappingURL=trending.js.map

/***/ }),

/***/ 166:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SplashPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_splash_screen__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__state_trending_trending_facade__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_analytics__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_svgs_splash_svg__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_device__ = __webpack_require__(82);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the SplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SplashPage = /** @class */ (function () {
    function SplashPage(ga, viewCtrl, splashScreen, trendingFacade, device, sanitizer, platform, toastCtrl) {
        var _this = this;
        this.ga = ga;
        this.viewCtrl = viewCtrl;
        this.splashScreen = splashScreen;
        this.trendingFacade = trendingFacade;
        this.device = device;
        this.sanitizer = sanitizer;
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.counter = 0;
        this.backgroundUrl = "assets/images/splash/android/drawable-port-xxhdpi-screen.png";
        this.platform.ready().then(function (readySource) {
            if (_this.device.platform == "Android") {
                if (_this.platform.width() <= 240) {
                    _this.backgroundUrl = "assets/images/splash/android/drawable-port-ldpi-screen.png";
                }
                else if (_this.platform.width() <= 320) {
                    _this.backgroundUrl = "assets/images/splash/android/drawable-port-mdpi-screen.png";
                }
                else if (_this.platform.width() <= 480) {
                    _this.backgroundUrl = "assets/images/splash/android/drawable-port-hdpi-screen.png";
                }
                else if (_this.platform.width() <= 720) {
                    _this.backgroundUrl = "assets/images/splash/android/drawable-port-xhdpi-screen.png";
                }
                else if (_this.platform.width() <= 960) {
                    _this.backgroundUrl = "assets/images/splash/android/drawable-port-xxhdpi-screen.png";
                }
                else {
                    _this.backgroundUrl = "assets/images/splash/android/drawable-port-xxxhdpi-screen.png";
                }
            }
            else {
                _this.backgroundUrl = "assets/images/splash/ios/Default@2x~universal~anyany.png";
            }
            /* platform.registerBackButtonAction(() => {
              if (this.counter == 0) {
                this.counter++;
                this.presentToast();
                setTimeout(() => { this.counter = 0 }, 3000)
              } else {
                // console.log("exitapp");
                platform.exitApp();
              }
            }, 0) */
        });
        this.server404 = false;
        this.server502 = false;
        this.trending$ = this.trendingFacade.trending$;
        this.loadingSvg = this.sanitizer.bypassSecurityTrustHtml(__WEBPACK_IMPORTED_MODULE_6__assets_svgs_splash_svg__["a" /* splash */]);
    }
    SplashPage.prototype.ionViewWillEnter = function () {
        // this.viewCtrl.showBackButton(false);
    };
    SplashPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.splashScreen.hide();
        this.sub = this.trending$.subscribe(function (trending) {
            if (trending != null) {
                if (trending.lists[0].header == "502") {
                    _this.server502 = true;
                }
                else if (trending.lists[0].header == "404") {
                    _this.server404 = true;
                }
                else {
                    _this.viewCtrl.dismiss();
                }
            }
        });
    };
    SplashPage.prototype.ionViewWillLeave = function () {
        this.sub.unsubscribe();
    };
    SplashPage.prototype.presentToast = function () {
        var toast = this.toastCtrl.create({
            message: "Press again to exit",
            duration: 3000,
            position: "middle"
        });
        toast.present();
    };
    SplashPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-splash',template:/*ion-inline-start:"C:\Users\Pidgeon\Desktop\Codes\src\app\pages\splash\splash.html"*/'<ion-content>\n\n  <div class="splash-background" \n\n  [ngStyle]="{\n\n    \'background\': \'url(\'+ backgroundUrl +\')\',\n\n    \'background-size\': \'100% 100%\'}">\n\n    <ng-container  *ngIf="!server502 && !server404; else error">\n\n    </ng-container>\n\n    <ng-template #error>\n\n      <ng-container *ngIf="server502">\n\n        <div class="error-msg">\n\n          <span>Server Offline</span>\n\n          <span>Please Be Patient</span>\n\n        </div>\n\n      </ng-container>\n\n      <ng-container *ngIf="server404">\n\n          <div class="error-msg">Server Offline</div>\n\n          <div class="error-msg">Please Be Patient</div>\n\n      </ng-container>\n\n    </ng-template>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Pidgeon\Desktop\Codes\src\app\pages\splash\splash.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_analytics__["a" /* GoogleAnalytics */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_3__state_trending_trending_facade__["a" /* TrendingFacade */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_device__["a" /* Device */],
            __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */]])
    ], SplashPage);
    return SplashPage;
}());

//# sourceMappingURL=splash.js.map

/***/ }),

/***/ 177:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 177;

/***/ }),

/***/ 222:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"pages/restaurant/restaurant.module": [
		492,
		3
	],
	"pages/specials/specials.module": [
		494,
		2
	],
	"pages/splash/splash.module": [
		495,
		1
	],
	"pages/trending/trending.module": [
		493,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 222;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 233:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export LOCATION_INITIAL_STATE */
/* harmony export (immutable) */ __webpack_exports__["b"] = browseReducer;
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return browseQueries; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__browse_actions__ = __webpack_require__(234);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};

var LOCATION_INITIAL_STATE = {
    radius: 5,
    paginationIndex: 0
};
function browseReducer(state, action) {
    if (state === void 0) { state = LOCATION_INITIAL_STATE; }
    switch (action.type) {
        case __WEBPACK_IMPORTED_MODULE_0__browse_actions__["a" /* BrowseActionTypes */].SAVE_PAGINATION_INDEX: {
            return __assign({}, state, { paginationIndex: action.payload });
        }
        case __WEBPACK_IMPORTED_MODULE_0__browse_actions__["a" /* BrowseActionTypes */].SAVE_RADIUS: {
            return __assign({}, state, { radius: action.payload });
        }
        default: {
            return state;
        }
    }
}
var browseQueries;
(function (browseQueries) {
    browseQueries.getPaginationIndex = function (state) { return state.browseState.paginationIndex; };
    browseQueries.getRadius = function (state) { return state.browseState.radius; };
})(browseQueries || (browseQueries = {}));
//# sourceMappingURL=browse.reducer.js.map

/***/ }),

/***/ 234:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BrowseActionTypes; });
/* unused harmony export SaveRadiusAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return SavePaginationIndexAction; });
var BrowseActionTypes;
(function (BrowseActionTypes) {
    BrowseActionTypes["SAVE_RADIUS"] = "[LOCATION] Save radius action";
    BrowseActionTypes["SAVE_PAGINATION_INDEX"] = "[BROWSE] Save pagination index action";
})(BrowseActionTypes || (BrowseActionTypes = {}));
var SaveRadiusAction = /** @class */ (function () {
    function SaveRadiusAction(payload) {
        this.payload = payload;
        this.type = BrowseActionTypes.SAVE_RADIUS;
    }
    return SaveRadiusAction;
}());

var SavePaginationIndexAction = /** @class */ (function () {
    function SavePaginationIndexAction(payload) {
        this.payload = payload;
        this.type = BrowseActionTypes.SAVE_PAGINATION_INDEX;
    }
    return SavePaginationIndexAction;
}());

//# sourceMappingURL=browse.actions.js.map

/***/ }),

/***/ 240:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpecialsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout__ = __webpack_require__(241);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_of__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_of__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_empty__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_empty___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_empty__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_retry__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_retry___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_retry__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SpecialsService = /** @class */ (function () {
    function SpecialsService(http) {
        this.http = http;
    }
    SpecialsService.prototype.getCategorizedSpecials = function (latitude, longitude, searchTerm, radius, category) {
        var cat = 0;
        console.log();
        switch (category) {
            case "category": {
                cat = 0;
                break;
            }
            case "restaurant": {
                cat = 1;
                break;
            }
            case "special": {
                cat = 2;
                break;
            }
        }
        var body = {
            "search": searchTerm,
            "latitude": latitude,
            "longitude": longitude,
            "radius": Number.parseInt(radius.toString()),
            "category": cat,
            "start": 0
        };
        try {
            return this.http.post("https://beta.lunchspecialsapp.com/api/specials/radius", body)
                .timeout(3000)
                .retry(3)
                .catch(function (err) {
                // simple logging, but you can do a lot more, see below
                if (err.error.toString().indexOf('404 Not Found') > -1) {
                }
                else if (err.error.toString().indexOf('502 Bad Gateway') > -1) {
                    return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].of([{ title: '502', specials: [] }]);
                }
                return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].of(new Array());
            });
        }
        catch (err) {
            return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].of(new Array());
        }
        // return Observable.of(specials).delay(1000);
    };
    SpecialsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClient */]])
    ], SpecialsService);
    return SpecialsService;
}());

//# sourceMappingURL=specials.service.js.map

/***/ }),

/***/ 287:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__map_map__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__browse_browse__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__trending_trending__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__specials_specials__ = __webpack_require__(90);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TabsPage = /** @class */ (function () {
    function TabsPage(navCtrl, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.events = events;
        this.trending = __WEBPACK_IMPORTED_MODULE_3__trending_trending__["a" /* TrendingPage */];
        this.map = __WEBPACK_IMPORTED_MODULE_1__map_map__["a" /* MapPage */];
        this.browse = __WEBPACK_IMPORTED_MODULE_2__browse_browse__["a" /* BrowsePage */];
        this.search = __WEBPACK_IMPORTED_MODULE_5__specials_specials__["a" /* SpecialsPage */];
        events.subscribe('nav:click', function (name) {
            if (name.indexOf("map") > -1) {
                _this.searchTerm = "";
                _this.events.publish("search:term", _this.searchTerm);
                _this.tabRef.select(0);
            }
            else if (name.indexOf("trending") > -1) {
                _this.searchTerm = "";
                _this.events.publish("search:term", _this.searchTerm);
                _this.tabRef.select(1);
            }
            else if (name.indexOf("browse") > -1) {
                _this.searchTerm = "";
                _this.events.publish("search:term", _this.searchTerm);
                _this.tabRef.select(2);
            }
        });
        events.subscribe("search:navigate", function (searchTerm) {
            _this.searchTerm = searchTerm;
            _this.tabRef.select(3).then(function () {
                _this.events.publish("search:term", _this.searchTerm);
            });
        });
    }
    TabsPage.prototype.tabSwitched = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('myTabs'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["l" /* Tabs */])
    ], TabsPage.prototype, "tabRef", void 0);
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Users\Pidgeon\Desktop\Codes\src\app\pages\tabs\tabs.html"*/'<ion-tabs #myTabs selectedIndex="1">\n\n  <ion-tab [root]="map" tabTitle="Map" tabIcon="map"></ion-tab>\n\n  <ion-tab [root]="trending" tabTitle="Trending" tabIcon="trendingicon"></ion-tab>\n\n  <ion-tab [root]="browse" tabTitle="Near By" tabIcon="pizza"></ion-tab>\n\n  <ion-tab [root]="search" tabTitle="Search" tabIcon="search"></ion-tab>\n\n</ion-tabs>\n\n'/*ion-inline-end:"C:\Users\Pidgeon\Desktop\Codes\src\app\pages\tabs\tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["a" /* Events */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 288:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_call_number__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__state_restaurant_restaurant_facade__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__restaurant_restaurant__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_device__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__assets_svgs_splash_svg__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__core_objects_google_analytics_tracking_id__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_google_analytics__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__core_jsons_charlotte_location__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__core_jsons_map_styles__ = __webpack_require__(382);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var MapPage = /** @class */ (function () {
    function MapPage(ga, navCtrl, geolocation, renderer, restaurantsFacade, menuCtrl, platform, device, sanitizer, loadingCtrl, callNumber) {
        var _this = this;
        this.ga = ga;
        this.navCtrl = navCtrl;
        this.geolocation = geolocation;
        this.renderer = renderer;
        this.restaurantsFacade = restaurantsFacade;
        this.menuCtrl = menuCtrl;
        this.platform = platform;
        this.device = device;
        this.sanitizer = sanitizer;
        this.loadingCtrl = loadingCtrl;
        this.callNumber = callNumber;
        this.ga.startTrackerWithId(__WEBPACK_IMPORTED_MODULE_10__core_objects_google_analytics_tracking_id__["a" /* googleAnalyticsTrackingId */])
            .then(function () {
            console.log('Google analytics is ready now');
            _this.ga.trackView("Map Page");
            // Tracker is ready
            // You can now track pages or set additional information such as AppVersion or UserId
        })
            .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
        this.renderedIds = new Array();
        this.initialLoad = true;
        this.loadingSvg = this.sanitizer.bypassSecurityTrustHtml(__WEBPACK_IMPORTED_MODULE_9__assets_svgs_splash_svg__["a" /* splash */]);
        this.loadingSpinner = this.loadingCtrl.create({
            spinner: 'hide',
            content: this.loadingSvg
        });
        setInterval(function () {
            _this.activeRestaurant;
        }, 100);
    }
    MapPage.prototype.ngAfterViewInit = function () {
        this.showLoadingSvg();
        this.loadMap();
    };
    MapPage.prototype.ionViewDidEnter = function () {
        /*
        * Enable correct menu
        */
        this.menuCtrl.enable(false, 'specialsmenu');
        this.menuCtrl.enable(false, 'browsemenu');
        this.menuCtrl.enable(true, 'mapmenu');
        console.log("platform: ", this.device.platform);
        if (this.device.platform != "Android" && !this.initialLoad) {
            this.showLoadingSvg();
            this.loadMap();
        }
        this.initialLoad = false;
    };
    MapPage.prototype.ionViewWillLeave = function () {
        /*
         * Destroy the map so that it can be re-instanciated on view load.
         * This is iOS specific because the map plugin is buggy on iOS.
         */
        if (this.device.platform != "Android") {
            this.map.destroy();
            this.renderedIds = new Array();
        }
    };
    MapPage.prototype.loadMap = function () {
        var _this = this;
        this.platform.ready().then(function () {
            /*
              * Set initial map properties
              */
            var mapOptions = {
                // mapType: 'MAP_TYPE_ROADMAP',
                camera: {
                    target: { lat: (_this.lastLatLng != null ? _this.lastLatLng.lat : 35.227190), lng: (_this.lastLatLng != null ? _this.lastLatLng.lat : -80.843097) },
                    zoom: 13,
                    tilt: 0,
                    bearing: 0,
                    duration: 5000
                },
                controls: {
                    myLocationButton: false,
                    indoorPicker: false
                },
                gestures: {
                    scroll: true,
                    tilt: false,
                    rotate: true,
                    zoom: true
                },
                preferences: {
                    zoom: {
                        minZoom: 0,
                        maxZoom: 110,
                    },
                    building: false
                },
                styles: __WEBPACK_IMPORTED_MODULE_13__core_jsons_map_styles__["a" /* mapStyles */]
            };
            /*
              * Create the map with the init properties.
              *
              * We do this before we actually because in the event the location isn't found or they don't
              *   allow location, then at least the app doesn't look busted and not show a
              *   map. This way it looks more polished.
              */
            _this.map = __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["a" /* GoogleMaps */].create(_this.mapReference.nativeElement, mapOptions);
            _this.map.one(__WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["b" /* GoogleMapsEvent */].MAP_READY).then(function () {
                _this.hideLoadingSvg();
                /*
                * Once your position is attained then move the camera to be centered over the phone's location.
                */
                _this.geolocation.getCurrentPosition().then(function (loc) {
                    _this.moveCamera(loc);
                }).catch(function (err) {
                    _this.moveCamera(__WEBPACK_IMPORTED_MODULE_12__core_jsons_charlotte_location__["a" /* tradeAndTryonLocation */]);
                });
                _this.camMovementSubscription = _this.map.on(__WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["b" /* GoogleMapsEvent */].CAMERA_MOVE_END).subscribe(function (endRes) {
                    var latLngBounds = _this.map.getVisibleRegion();
                    _this.renderRestaurants(latLngBounds);
                });
            });
        });
    };
    MapPage.prototype.moveCamera = function (loc) {
        var _this = this;
        this.lastLatLng = new __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["c" /* LatLng */](loc.coords.latitude, loc.coords.longitude);
        this.map.animateCamera({
            target: { lat: loc.coords.latitude, lng: loc.coords.longitude },
            zoom: 13,
            tilt: 0,
            bearing: 0,
            duration: 500
        }).then(function () {
            /*
             * Add a marker for where you are.
             */
            _this.map.addMarker({
                icon: {
                    url: "assets/images/marker.png",
                    size: {
                        width: 32,
                        height: 32,
                    },
                    anchor: [16, 16]
                },
                position: {
                    lat: loc.coords.latitude,
                    lng: loc.coords.longitude
                }
            });
            var latLngBounds = _this.map.getVisibleRegion();
            _this.renderRestaurants(latLngBounds);
        });
    };
    MapPage.prototype.renderRestaurants = function (latLngBounds) {
        var _this = this;
        this.pageSubscription = this.restaurantsFacade.loadInBounds(latLngBounds).subscribe(function (restaurants) {
            var temp = new Array();
            temp = restaurants.filter(function (r) {
                return _this.renderedIds.indexOf(r.id) == -1;
            });
            var bounds = _this.map.getVisibleRegion();
            temp.forEach(function (r) {
                if (_this.isInsideBounds(r, bounds)) {
                    _this.renderedIds.push(r.id);
                    /*
                      * Put each of the returned resaurants onto the map as pins
                      */
                    _this.map.addMarker({
                        icon: {
                            url: "assets/images/pin_icon.png",
                            size: {
                                width: 32,
                                height: 32
                            }
                        },
                        position: {
                            lat: r.latitude,
                            lng: r.longitude
                        }
                    }).then(function (marker) {
                        marker.on(__WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["b" /* GoogleMapsEvent */].MARKER_CLICK).subscribe(function (params) {
                            _this.ga.trackEvent('map-detail', r.name, 'Map')
                                .then(function () {
                                _this.activeRestaurant = r;
                                _this.menuCtrl.open();
                            });
                        });
                    });
                }
            });
        });
    };
    MapPage.prototype.restaurant = function (id) {
        var _this = this;
        this.ga.trackEvent('Map', 'page-load', 'Restaurant', id)
            .then(function () {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__restaurant_restaurant__["a" /* RestaurantPage */], { "restaurant_id": id });
        });
    };
    MapPage.prototype.isInsideBounds = function (r, bounds) {
        return ((r.latitude < bounds.northeast.lat && r.latitude > bounds.southwest.lat)
            &&
                (r.longitude < bounds.northeast.lng && r.longitude > bounds.southwest.lng));
    };
    MapPage.prototype.dirs = function (id, address) {
        this.ga.trackEvent('Map', 'directions', "Restaurant", id)
            .then(function () {
            window.open("geo:0,0?q=" + address, "_system");
        });
    };
    MapPage.prototype.showLoadingSvg = function () {
        this.loadingSpinner.present();
    };
    MapPage.prototype.hideLoadingSvg = function () {
        this.loadingSpinner.dismiss();
    };
    MapPage.prototype.tele = function (id, phone) {
        var _this = this;
        this.ga.trackEvent('Restaurant', 'intent', 'Phone', id)
            .then(function () {
            _this.callNumber.callNumber(phone, true)
                .then(function () { return console.log('Launched dialer!'); })
                .catch(function () { return console.log('Error launching dialer'); });
        });
    };
    MapPage.prototype.site = function (id, website) {
        this.ga.trackEvent('Restaurant', 'intent', 'Website', id)
            .then(function () {
            window.open(website, '_system', 'location=yes');
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('searchbar'),
        __metadata("design:type", Object)
    ], MapPage.prototype, "searchbar", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], MapPage.prototype, "mapReference", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('desc'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], MapPage.prototype, "descReference", void 0);
    MapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-map',template:/*ion-inline-start:"C:\Users\Pidgeon\Desktop\Codes\src\app\pages\map\map.html"*/'<ion-menu type="overlay" class="right-menu" side="left" [content]="mapcontent" id="mapmenu">\n\n  <ng-container *ngIf="activeRestaurant">\n\n    <ion-header>\n\n      <ion-navbar color="offWhite">\n\n        <ion-title>{{activeRestaurant.name}}</ion-title>\n\n      </ion-navbar>\n\n    </ion-header>\n\n    <div class="restaurant-header"\n\n      [ngStyle]="{\'background-image\': \'url(\' + (activeRestaurant.banner_image != \'\'  ? activeRestaurant.banner_image : \'assets/images/banners/default.jpg\') + \')\'}">\n\n    </div>\n\n    <ion-fab (click)="restaurant(activeRestaurant.id)"  class="home-icon">\n\n      <button class="restaurant-icon" ion-fab>\n\n        <div class="svg-wrapper"></div>\n\n      </button>\n\n    </ion-fab>\n\n    <ion-list class="details">\n\n      <ion-item class="description">\n\n        <!-- <ion-icon name="call" item-start color="palletLight"></ion-icon> -->\n\n        <ng-container *ngIf="activeRestaurant.description && activeRestaurant.description != \'\'; else nodescription">\n\n          <span class="description-body">\n\n            {{activeRestaurant.description}}\n\n          </span>\n\n        </ng-container>\n\n        <ng-template #nodescription>\n\n            <span class="description-body">\n\n              No Description\n\n            </span>\n\n        </ng-template>\n\n      </ion-item>\n\n      <ion-item class="phone">\n\n          <ion-icon name="call" item-start color="palletLight"></ion-icon>\n\n          <ng-container *ngIf="activeRestaurant.phone && activeRestaurant.phone != \'\'; else nophone">\n\n            <a href="#" (click)="tele(activeRestaurant.id, activeRestaurant.phone)">\n\n              <span class="phone-body">\n\n                {{activeRestaurant.phone}}\n\n              </span>\n\n            </a>\n\n          </ng-container>\n\n          <ng-template #nophone>\n\n            <span class="phone-body">\n\n              N/A\n\n            </span>\n\n          </ng-template>\n\n      </ion-item>\n\n      <ion-item class="website">\n\n          <ion-icon name="browsers" item-start color="palletLight"></ion-icon>\n\n          <ng-container *ngIf="activeRestaurant.website && activeRestaurant.website != \'\'; else nowebsite">\n\n            <a href="#" (click)="site(activeRestaurant.id, activeRestaurant.website)">\n\n                <span class="website-body">\n\n                    {{activeRestaurant.website}}\n\n                </span>\n\n            </a>\n\n          </ng-container>\n\n          <ng-template #nowebsite>\n\n            <span class="website-body">\n\n              N/A\n\n            </span>\n\n          </ng-template>\n\n      </ion-item>\n\n      <ion-item class="address">\n\n          <ion-icon name="pin" item-start color="palletLight"></ion-icon>\n\n          <a href="#" (click)="dirs(activeRestaurant.id, activeRestaurant.address + \' \' + activeRestaurant.address2 + \' \' + activeRestaurant.city + \' \' + activeRestaurant.state + \', \' + activeRestaurant.zip)">\n\n            <span class="address-1-body">\n\n              {{activeRestaurant.address}}\n\n            </span>\n\n            <span class="address-2-body">\n\n              {{activeRestaurant.address2}}\n\n            </span>\n\n            <span class="address-3-body">\n\n              {{activeRestaurant.city}} {{activeRestaurant.state}}, {{activeRestaurant.zip}}\n\n            </span>\n\n          </a>\n\n      </ion-item>\n\n    </ion-list>\n\n  </ng-container>\n\n  <ng-template #loadingactive>\n\n    Loading Restaurant\n\n  </ng-template>\n\n</ion-menu>\n\n\n\n<ion-header no-border>\n\n    <ion-navbar color="offWhite">\n\n        <f-header></f-header>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="outer-content" #mapcontent>\n\n  <div #map id="map" (click)="toggleExpand()"></div>\n\n  <div #desc id="desc" [ngClass]="{\'expanded\': expanded, \'collapsed\': !expanded}" (click)="toggleExpand()"></div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Pidgeon\Desktop\Codes\src\app\pages\map\map.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_11__ionic_native_google_analytics__["a" /* GoogleAnalytics */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["X" /* Renderer */],
            __WEBPACK_IMPORTED_MODULE_5__state_restaurant_restaurant_facade__["a" /* RestaurantFacade */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_device__["a" /* Device */],
            __WEBPACK_IMPORTED_MODULE_8__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_call_number__["a" /* CallNumber */]])
    ], MapPage);
    return MapPage;
}());

//# sourceMappingURL=map.js.map

/***/ }),

/***/ 289:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BrowsePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__restaurant_restaurant__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__state_restaurant_restaurant_facade__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__state_browse_browse_facade__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_jsons_charlotte_location__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_objects_google_analytics_tracking_id__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_google_analytics__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__specials_specials__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__assets_svgs_splash_svg__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_platform_browser__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var BrowsePage = /** @class */ (function () {
    function BrowsePage(ga, navCtrl, restaurantFacade, browseFacade, platform, geolocation, sanitizer, menuCtrl) {
        var _this = this;
        this.ga = ga;
        this.navCtrl = navCtrl;
        this.restaurantFacade = restaurantFacade;
        this.browseFacade = browseFacade;
        this.platform = platform;
        this.geolocation = geolocation;
        this.sanitizer = sanitizer;
        this.menuCtrl = menuCtrl;
        /*
         * Init varibles
         */
        this.selectedId = -1;
        this.noResults = true;
        this.distance = 5;
        this.sortBy = "Proximity";
        this.searchTerm = "";
        this.nameSearch = true;
        this.descriptionSearch = false;
        this.tagSearch = false;
        this.hideDistanceBtn = true;
        this.hideSortBtn = true;
        this.loadingSvg = this.sanitizer.bypassSecurityTrustHtml(__WEBPACK_IMPORTED_MODULE_10__assets_svgs_splash_svg__["a" /* splash */]);
        this.platform.ready().then(function () {
            _this.ga.startTrackerWithId(__WEBPACK_IMPORTED_MODULE_7__core_objects_google_analytics_tracking_id__["a" /* googleAnalyticsTrackingId */])
                .then(function () {
                console.log('Google analytics is ready now');
                _this.ga.trackView("Browse Page");
                _this.loading$ = _this.restaurantFacade.loading$;
                _this.geolocation.getCurrentPosition().then(function (loc) {
                    _this.restaurants$ = _this.restaurantFacade.loadNearBy(loc);
                }).catch(function (err) {
                    _this.restaurants$ = _this.restaurantFacade.loadNearBy(__WEBPACK_IMPORTED_MODULE_6__core_jsons_charlotte_location__["a" /* tradeAndTryonLocation */]);
                });
            })
                .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
        });
    }
    BrowsePage.prototype.ionViewWillEnter = function () {
        /*
         * Enable correct menu
         */
        this.menuCtrl.enable(false, 'specialsmenu');
        this.menuCtrl.enable(true, 'browsemenu');
        this.menuCtrl.enable(false, 'mapmenu');
    };
    BrowsePage.prototype.ionViewWillLeave = function () {
        this.menuCtrl.close();
    };
    BrowsePage.prototype.selectCard = function (id) {
        var _this = this;
        this.ga.trackEvent('Browse', 'card select', 'Restaurant', id)
            .then(function () {
            if (_this.selectedId == id) {
                _this.selectedId = -1;
            }
            else {
                _this.selectedId = id;
            }
        });
    };
    BrowsePage.prototype.restaurant = function (id) {
        var _this = this;
        this.ga.trackEvent('Browse', 'page-load', 'Restaurant', id)
            .then(function () {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__restaurant_restaurant__["a" /* RestaurantPage */], { "restaurant_id": id });
        });
    };
    BrowsePage.prototype.doRefresh = function (refresher) {
        /* this.ga.trackEvent('Browse', 'refresh', 'Refresher')
        .then(() => {
          this.geolocation.getCurrentPosition().then((loc) => {
            this.restaurants$ = this.restaurantFacade.refresh(loc);
          }).catch(err => {
            this.restaurants$ = this.restaurantFacade.refresh(tradeAndTryonLocation);
          });
          
          this.loading$.subscribe((loading: boolean) => {
            if(!loading)
              refresher.complete();
          });
        }); */
        refresher.complete();
    };
    BrowsePage.prototype.distanceChange = function (val) {
        var _this = this;
        this.ga.trackEvent('Browse', 'filter', 'distance', val)
            .then(function () {
            _this.hideDistanceBtn = false;
            _this.geolocation.getCurrentPosition().then(function (loc) {
                _this.browseFacade.saveRadius(_this.distance, loc);
            }).catch(function (err) {
                _this.browseFacade.saveRadius(_this.distance, __WEBPACK_IMPORTED_MODULE_6__core_jsons_charlotte_location__["a" /* tradeAndTryonLocation */]);
            });
        });
    };
    BrowsePage.prototype.sortByChange = function (event) {
        var _this = this;
        this.ga.trackEvent('Browse', 'filter', this.sortBy)
            .then(function () {
            _this.hideSortBtn = false;
        });
    };
    BrowsePage.prototype.disableDistanceFilterBtn = function () {
        this.hideDistanceBtn = true;
        this.distance = 5;
    };
    BrowsePage.prototype.disableSortFilterBtn = function () {
        this.hideSortBtn = true;
        this.sortBy = "Proximity";
    };
    BrowsePage.prototype.filterItems = function (event) {
        var _this = this;
        this.ga.trackEvent('Browse', 'search', this.searchTermBinding)
            .then(function () {
            _this.searchTerm = _this.searchTermBinding;
        });
    };
    BrowsePage.prototype.onSearchInput = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__specials_specials__["a" /* SpecialsPage */], { "search_term": this.headerSearchTerm });
    };
    BrowsePage.prototype.onSearchSelected = function () {
        this.searchbar.setFocus();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('searchbar'),
        __metadata("design:type", Object)
    ], BrowsePage.prototype, "searchbar", void 0);
    BrowsePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-browse',template:/*ion-inline-start:"C:\Users\Pidgeon\Desktop\Codes\src\app\pages\browse\browse.html"*/'<ion-menu class="right-menu" side="right" [content]="browsecontent" type="overlay" id="browsemenu">\n\n  <ion-header>\n\n    <ion-navbar>\n\n      <ion-title>Sorting Options</ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  <ion-content class="outer-content">\n\n    <ion-list>\n\n      <ion-list-header>\n\n          Distance\n\n      </ion-list-header>\n\n      <ion-list radio-group (ngModelChange)="distanceChange($event)" [(ngModel)]="distance">\n\n        <ion-item>\n\n          <ion-label>1 mi</ion-label>\n\n          <ion-radio value="1"></ion-radio>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label>5 mi</ion-label>\n\n          <ion-radio value="5"></ion-radio>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label>10 mi</ion-label>\n\n          <ion-radio value="10"></ion-radio>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label>20 mi</ion-label>\n\n          <ion-radio value="20"></ion-radio>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label>30 mi</ion-label>\n\n          <ion-radio value="30"></ion-radio>\n\n        </ion-item>\n\n      </ion-list>\n\n      <ion-item></ion-item>\n\n      <ion-list-header>\n\n        Sort\n\n      </ion-list-header>\n\n      <ion-list radio-group (ngModelChange)="sortByChange($event)" [(ngModel)]="sortBy">\n\n        <ion-item>\n\n          <ion-icon ios="logo-usd" md="logo-usd" item-start color="palletLight"></ion-icon>\n\n          <ion-label>Low To High</ion-label>\n\n          <ion-radio value="Low To High"></ion-radio>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-icon ios="logo-usd" md="logo-usd" item-start color="palletLight"></ion-icon>\n\n          <ion-label>High To Low</ion-label>\n\n          <ion-radio value="High To Low"></ion-radio>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-icon name="navigate" item-start color="palletLight"></ion-icon>\n\n          <ion-label>Proximity</ion-label>\n\n          <ion-radio value="Proximity"></ion-radio>\n\n        </ion-item>\n\n      </ion-list>\n\n      <ion-item></ion-item>\n\n    </ion-list>\n\n  </ion-content>\n\n</ion-menu>\n\n\n\n<ion-header no-border>\n\n  <ion-navbar class="frosted-navbar">\n\n    <f-header></f-header>\n\n  </ion-navbar>\n\n  <div class="options-container">\n\n    <ion-searchbar placeholder="Filter Restaurants" [(ngModel)]="searchTermBinding" (ngModelChange)="filterItems($event)" \n\n      [debounce]="500">\n\n    </ion-searchbar>\n\n    <a class="options" menuToggle href="#" [ngClass]="{\'hide\': searchTerm.length != 0 }">\n\n      <ion-icon class="options" name="options"></ion-icon>\n\n    </a>\n\n  </div>\n\n</ion-header>\n\n\n\n<ion-content id="#outer-content" class="outer-content" fullscreen #browsecontent>\n\n  <div class="filters-list" [ngClass]="{\'hide\': hideSortBtn && hideDistanceBtn }">\n\n    <div class="filters-list-title-container">\n\n      <span class="filters-list-title">Filters: </span>\n\n    </div>\n\n    <button class="filter-btn filter-distance-btn" (click)="disableDistanceFilterBtn()" \n\n    [ngClass]="{\'hide\': hideDistanceBtn }" ion-button round outline>\n\n      {{distance}} mi \n\n      <button class="filter-btn-close" ion-button round>x</button>\n\n    </button>\n\n    <button class="filter-btn filter-sort-btn" (click)="disableSortFilterBtn()" \n\n    [ngClass]="{\'hide\': hideSortBtn }" ion-button round outline>\n\n      {{sortBy}} \n\n      <button class="filter-btn-close" ion-button round>x</button>\n\n    </button>\n\n  </div>\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n\n    <ion-refresher-content></ion-refresher-content>\n\n  </ion-refresher>\n\n  <ng-container *ngIf="restaurants$ | async | browseSort:sortBy:distance:searchTerm; let restaurants;">\n\n    <ion-list>\n\n      <ng-container *ngIf="restaurants.length > 0; else pleasewait">\n\n        <ion-card *ngFor="let card of restaurants" tappable>\n\n          <div class="card-body">\n\n              <div class="background" [ngStyle]="{\'background-image\': \'linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), \n\n                url(\' + (card.banner_image != \'\'  ? card.banner_image : \'assets/images/banners/default.jpg\') + \')\'}"\n\n                [ngClass]="{\'grayscale\': card.specials.length == 0}"></div>\n\n              <div [ngClass]="{\'active\': card.id == selectedId}" class="gradient" (click)="selectCard(card.id)"></div>\n\n              <div class="card-title">{{card.name}}</div>\n\n              <div class="card-type">{{card.description}}</div>\n\n              <div class="card-dollars">\n\n                <img src="assets/images/dollar_sign_{{card.expense_rating}}.png"/>\n\n              </div>\n\n              <div class="card-distance">{{card.distance | distancePipe}} mi</div>\n\n              <ion-fab (click)="restaurant(card.id)" class="restaurant-fab"\n\n                [ngClass]="{\'collapsed\': card.id != selectedId, \'expanded\': card.id == selectedId }">\n\n                <button class="restaurant-icon" ion-fab>\n\n                  <div class="svg-wrapper"></div>\n\n                </button>\n\n              </ion-fab>\n\n          </div>\n\n          <ng-container *ngIf="card.specials; let specials">\n\n            <div [ngClass]="{\'collapsed\': card.id != selectedId, \'expanded\': card.id == selectedId, \'expanded-plus\': specials.length > 4 }">\n\n              <ng-container *ngIf="specials.length > 0; else nospecials">\n\n                <ion-list class="specials-list">\n\n                  <ng-container *ngFor="let special of specials; let odd = odd; let i= index;">\n\n                    <ng-container *ngIf="i < 4">\n\n                      <ion-item>\n\n                        <div class="special" [ngClass]="{\'light-item\': !odd, \'first-item\': i == 0}">\n\n                          <span class="special-img"></span>\n\n                          <div class="special-content">\n\n                              <span class="item-name">\n\n                                  {{special.title}}\n\n                              </span>\n\n                              <span class="middot">&#8226;</span>\n\n                              <span class="price">\n\n                                  {{special.value}}\n\n                              </span>\n\n                          </div>\n\n                        </div>\n\n                      </ion-item>\n\n                    </ng-container>\n\n                    <ng-container *ngIf="i == 4">\n\n                      <ion-item class="view-all-specials">\n\n                          <span (click)="restaurant(card.id)">\n\n                              View All Specials\n\n                          </span>\n\n                      </ion-item>\n\n                    </ng-container>\n\n                  </ng-container>\n\n                </ion-list>\n\n              </ng-container>\n\n              <ng-template #nospecials>\n\n                <div class="no-specials-container">\n\n                  <span>No Specials</span>\n\n                  <span>To Display</span>\n\n                </div>\n\n              </ng-template>\n\n            </div>\n\n          </ng-container>\n\n        </ion-card>\n\n      </ng-container>\n\n      <ng-template #pleasewait>\n\n        <ng-container *ngIf="loading$ | async; let loading; else noresults">\n\n          <ng-container *ngIf="loading; else noresults">\n\n              <div class="loading-svg" [innerHtml]="loadingSvg" ></div>\n\n          </ng-container>\n\n        </ng-container>\n\n        <ng-template #noresults>\n\n          <ng-container *ngIf="restaurants.length == 0">\n\n            <ng-container *ngIf="searchTerm == \'\'">\n\n              <div class="no-results-container">\n\n                <span>No Results</span>\n\n              </div>\n\n            </ng-container>\n\n            <ng-container *ngIf="searchTerm != \'\'">\n\n              <div class="no-results-container">\n\n                <span>No Matches</span>\n\n              </div>\n\n            </ng-container>\n\n          </ng-container>\n\n        </ng-template>\n\n      </ng-template>\n\n    </ion-list>\n\n  </ng-container>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Pidgeon\Desktop\Codes\src\app\pages\browse\browse.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_8__ionic_native_google_analytics__["a" /* GoogleAnalytics */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__state_restaurant_restaurant_facade__["a" /* RestaurantFacade */],
            __WEBPACK_IMPORTED_MODULE_5__state_browse_browse_facade__["a" /* BrowseFacade */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_11__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */]])
    ], BrowsePage);
    return BrowsePage;
}());

//# sourceMappingURL=browse.js.map

/***/ }),

/***/ 312:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(319);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 319:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export MyErrorHandler */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_map_map__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_browse_browse__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_splash_splash__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_trending_trending__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_restaurant_restaurant__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_specials_specials__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_status_bar__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_splash_screen__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ngrx_store__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ngrx_store_devtools__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__state_app_state__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__state_app_state_module__ = __webpack_require__(391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__core_core_module__ = __webpack_require__(482);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__nrwl_nx__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_pro__ = __webpack_require__(485);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_pro___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_21__ionic_pro__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_device__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_google_analytics__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_call_number__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__core_http_http_interceptor__ = __webpack_require__(486);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__angular_common_http__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_diagnostic__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__components_components_module__ = __webpack_require__(487);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_header_header_component__ = __webpack_require__(488);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/* Pages */







/* Ionic Stuff */


/* ngrx Stuff */






/* Error Handling */











__WEBPACK_IMPORTED_MODULE_21__ionic_pro__["Pro"].init('bdfe7d4a', {
    appVersion: '0.0.1'
});
var MyErrorHandler = /** @class */ (function () {
    function MyErrorHandler(injector) {
        try {
            this.ionicErrorHandler = injector.get(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */]);
        }
        catch (e) {
            // Unable to get the IonicErrorHandler provider, ensure
            // IonicErrorHandler has been added to the providers list below
        }
    }
    MyErrorHandler.prototype.handleError = function (err) {
        __WEBPACK_IMPORTED_MODULE_21__ionic_pro__["Pro"].monitoring.handleNewError(err);
        // Remove this if you want to disable Ionic's auto exception handling
        // in development mode.
        this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
    };
    MyErrorHandler = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Injector */]])
    ], MyErrorHandler);
    return MyErrorHandler;
}());

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_map_map__["a" /* MapPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_browse_browse__["a" /* BrowsePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_trending_trending__["a" /* TrendingPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_splash_splash__["a" /* SplashPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_restaurant_restaurant__["a" /* RestaurantPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_specials_specials__["a" /* SpecialsPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_header_header_component__["a" /* HeaderComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_18__state_app_state_module__["a" /* AppStateModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_19__core_core_module__["a" /* CoreModule */],
                __WEBPACK_IMPORTED_MODULE_28__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: 'pages/restaurant/restaurant.module#RestaurantPageModule', name: 'RestaurantPage', segment: 'restaurant', priority: 'low', defaultHistory: [] },
                        { loadChildren: 'pages/trending/trending.module#TrendingPageModule', name: 'TrendingPage', segment: 'trending', priority: 'low', defaultHistory: [] },
                        { loadChildren: 'pages/specials/specials.module#SpecialsPageModule', name: 'SpecialsPage', segment: 'specials', priority: 'low', defaultHistory: [] },
                        { loadChildren: 'pages/splash/splash.module#SplashPageModule', name: 'SplashPage', segment: 'splash', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_20__nrwl_nx__["b" /* NxModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_15__ngrx_store__["j" /* StoreModule */].forRoot(__WEBPACK_IMPORTED_MODULE_17__state_app_state__["b" /* ROOT_REDUCER */], { metaReducers: __WEBPACK_IMPORTED_MODULE_17__state_app_state__["a" /* META_REDUCERS */] }),
                __WEBPACK_IMPORTED_MODULE_16__ngrx_store_devtools__["a" /* StoreDevtoolsModule */].instrument({ maxAge: 25 })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_map_map__["a" /* MapPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_browse_browse__["a" /* BrowsePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_trending_trending__["a" /* TrendingPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_restaurant_restaurant__["a" /* RestaurantPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_specials_specials__["a" /* SpecialsPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_splash_splash__["a" /* SplashPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["a" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_device__["a" /* Device */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_google_analytics__["a" /* GoogleAnalytics */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_call_number__["a" /* CallNumber */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_diagnostic__["a" /* Diagnostic */],
                // { provide: ErrorHandler, useClass: MyErrorHandler },
                // { provide: DEFAULT_TIMEOUT, useValue: defaultTimeout },
                { provide: __WEBPACK_IMPORTED_MODULE_26__angular_common_http__["a" /* HTTP_INTERCEPTORS */], useClass: __WEBPACK_IMPORTED_MODULE_25__core_http_http_interceptor__["a" /* CustomHttpInterceptor */], multi: true }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 351:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return conversions; });
var conversions = /** @class */ (function () {
    function conversions() {
    }
    conversions.toWholeMile = function (meters) {
        console.log(meters, " meters converts to ", (meters * this.METER_TO_MILE), " miles");
        return meters * this.METER_TO_MILE;
    };
    conversions.METER_TO_MILE = 0.000621371;
    conversions.METER_TO_HALF_MILE = 804.67;
    return conversions;
}());

//# sourceMappingURL=conversions.js.map

/***/ }),

/***/ 354:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return trending502; });
var trending502 = {
    carouselItems: [
        {
            image: "",
            restaurant_name: "",
            restaurant_id: -1,
            menu_item_name: "",
            price: ""
        }
    ],
    lists: [
        {
            header: "502",
            listItems: [
                { restaurant_name: "", restaurant_id: -1, menu_item_name: "", price: "" }
            ]
        }
    ]
};
//# sourceMappingURL=trending502.json.js.map

/***/ }),

/***/ 355:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return trending404; });
var trending404 = {
    carouselItems: [
        {
            image: "",
            restaurant_name: "",
            restaurant_id: -1,
            menu_item_name: "",
            price: ""
        }
    ],
    lists: [
        {
            header: "404",
            listItems: [
                { restaurant_name: "", restaurant_id: -1, menu_item_name: "", price: "" }
            ]
        }
    ]
};
//# sourceMappingURL=trending404.json.js.map

/***/ }),

/***/ 356:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return trendingEmpty; });
var trendingEmpty = {
    carouselItems: [
        {
            image: "",
            restaurant_name: "",
            restaurant_id: -1,
            menu_item_name: "",
            price: ""
        }
    ],
    lists: [
        {
            header: "",
            listItems: [
                { restaurant_name: "", restaurant_id: -1, menu_item_name: "", price: "" }
            ]
        }
    ]
};
//# sourceMappingURL=trendingEmpty.json.js.map

/***/ }),

/***/ 381:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_splash_splash__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_analytics__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_objects_google_analytics_tracking_id__ = __webpack_require__(63);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/* Pages */




var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, modalCtrl, ga) {
        var _this = this;
        this.ga = ga;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            var splash = modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__pages_splash_splash__["a" /* SplashPage */]);
            splash.present();
            // statusBar.styleLightContent();
            // let status bar overlay webview
            statusBar.styleDefault();
            statusBar.backgroundColorByHexString("#f2f0ed");
            _this.ga.startTrackerWithId(__WEBPACK_IMPORTED_MODULE_7__core_objects_google_analytics_tracking_id__["a" /* googleAnalyticsTrackingId */])
                .then(function () {
                console.log('Google analytics is ready now');
                _this.ga.trackView('App Home');
                // Tracker is ready
                // You can now track pages or set additional information such as AppVersion or UserId
            })
                .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Users\Pidgeon\Desktop\Codes\src\app\app.html"*/'<ion-nav #mycontent [root]="rootPage"></ion-nav>\n\n'/*ion-inline-end:"C:\Users\Pidgeon\Desktop\Codes\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_analytics__["a" /* GoogleAnalytics */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 382:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return mapStyles; });
var mapStyles = [
    {
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#616161"
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#f5f5f5"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#bdbdbd"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#eeeeee"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#757575"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e5e5e5"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#9e9e9e"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#757575"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dadada"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#E2340D"
            },
            {
                "saturation": 15
            },
            {
                "lightness": 80
            },
            {
                "weight": 2.5
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#E2340D"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#616161"
            }
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#E2340D"
            },
            {
                "lightness": 45
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#E2340D"
            }
        ]
    },
    {
        "featureType": "road.local",
        "stylers": [
            {
                "color": "#E2340D"
            },
            {
                "lightness": 30
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#9e9e9e"
            }
        ]
    },
    {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e5e5e5"
            }
        ]
    },
    {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#eeeeee"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#c9c9c9"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#9e9e9e"
            }
        ]
    }
];
//# sourceMappingURL=map-styles.js.map

/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ROOT_REDUCER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return META_REDUCERS; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ngrx_store_freeze__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ngrx_store_freeze___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_ngrx_store_freeze__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environment__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__state_trending_trending_reducer__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__restaurant_restaurant_reducer__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__location_location_reducer__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__browse_browse_reducer__ = __webpack_require__(233);






var ROOT_REDUCER = {
    trendingState: __WEBPACK_IMPORTED_MODULE_2__state_trending_trending_reducer__["c" /* trendingReducer */],
    restaurantState: __WEBPACK_IMPORTED_MODULE_3__restaurant_restaurant_reducer__["b" /* restaurantReducer */],
    locationState: __WEBPACK_IMPORTED_MODULE_4__location_location_reducer__["b" /* locationReducer */],
    browseState: __WEBPACK_IMPORTED_MODULE_5__browse_browse_reducer__["b" /* browseReducer */]
};
var META_REDUCERS = !__WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].production ? [__WEBPACK_IMPORTED_MODULE_0_ngrx_store_freeze__["storeFreeze"]] : [];
//# sourceMappingURL=app.state.js.map

/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    production: false,
    version: '(dev)',
    serverUrl: 'local',
    useMockBackend: true
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 391:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppStateModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_effects__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__trending_trending_effects__ = __webpack_require__(470);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__restaurant_restaurant_effects__ = __webpack_require__(479);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__trending_trending_facade__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__restaurant_restaurant_facade__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__location_location_effects__ = __webpack_require__(480);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__browse_browse_effects__ = __webpack_require__(481);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__location_location_facade__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__browse_browse_facade__ = __webpack_require__(129);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var AppStateModule = /** @class */ (function () {
    function AppStateModule() {
    }
    AppStateModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["c" /* EffectsModule */].forRoot([
                    __WEBPACK_IMPORTED_MODULE_2__trending_trending_effects__["a" /* TrendingEffects */],
                    __WEBPACK_IMPORTED_MODULE_3__restaurant_restaurant_effects__["a" /* RestaurantEffects */],
                    __WEBPACK_IMPORTED_MODULE_6__location_location_effects__["a" /* LocationEffects */],
                    __WEBPACK_IMPORTED_MODULE_7__browse_browse_effects__["a" /* BrowseEffects */]
                ]),
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__trending_trending_facade__["a" /* TrendingFacade */],
                __WEBPACK_IMPORTED_MODULE_5__restaurant_restaurant_facade__["a" /* RestaurantFacade */],
                __WEBPACK_IMPORTED_MODULE_8__location_location_facade__["a" /* LocationFacade */],
                __WEBPACK_IMPORTED_MODULE_9__browse_browse_facade__["a" /* BrowseFacade */]
            ]
        })
    ], AppStateModule);
    return AppStateModule;
}());

//# sourceMappingURL=app.state.module.js.map

/***/ }),

/***/ 470:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrendingEffects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_effects__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__nrwl_nx__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_trending_service__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__trending_actions__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__trending_reducer__ = __webpack_require__(138);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var TrendingEffects = /** @class */ (function () {
    function TrendingEffects(actions$, trendingService, d) {
        var _this = this;
        this.actions$ = actions$;
        this.trendingService = trendingService;
        this.d = d;
        this.getAuthStateSuccessMessage = 'Authenication Complete!';
        this.getAuthStateErrorMessage = 'Failed to authenticate.';
        this.doAuth$ = this.d.fetch(__WEBPACK_IMPORTED_MODULE_5__trending_actions__["c" /* TrendingActionTypes */].LOAD_TRENDING, {
            run: function (action, state) {
                var trendingState = __WEBPACK_IMPORTED_MODULE_6__trending_reducer__["a" /* TRENDING_INITAL_STATE */];
                // Run the back-end initial trending paris grab
                return _this.trendingService.getTrending()
                    // set the Trending Trending list to the store.
                    .map(function (data) { return new __WEBPACK_IMPORTED_MODULE_5__trending_actions__["b" /* LoadTrendingSuccessAction */](data); });
            },
            onError: function (action, e) {
                // can add error handling here but for now just return null
                return null;
            }
        });
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], TrendingEffects.prototype, "doAuth$", void 0);
    TrendingEffects = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["a" /* Actions */],
            __WEBPACK_IMPORTED_MODULE_4__core_services_trending_service__["a" /* TrendingService */],
            __WEBPACK_IMPORTED_MODULE_2__nrwl_nx__["a" /* DataPersistence */]])
    ], TrendingEffects);
    return TrendingEffects;
}());

//# sourceMappingURL=trending.effects.js.map

/***/ }),

/***/ 479:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestaurantEffects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_effects__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__nrwl_nx__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__restaurant_actions__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_services_restaurant_service__ = __webpack_require__(79);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RestaurantEffects = /** @class */ (function () {
    // (data: Map<number, Restaurant>) => new LoadRestaurantSuccessAction(data)
    function RestaurantEffects(actions$, restaurantService, d) {
        var _this = this;
        this.actions$ = actions$;
        this.restaurantService = restaurantService;
        this.d = d;
        this.getAuthStateSuccessMessage = 'Authenication Complete!';
        this.getAuthStateErrorMessage = 'Failed to authenticate.';
        this.loadRestaurant$ = this.d.fetch(__WEBPACK_IMPORTED_MODULE_4__restaurant_actions__["k" /* RestaurantActionTypes */].LOAD_RESTAURANT, {
            run: function (action, state) {
                return _this.restaurantService.getById(action.payload)
                    .map(function (data) {
                    if (data != undefined) {
                        return new __WEBPACK_IMPORTED_MODULE_4__restaurant_actions__["f" /* LoadRestaurantSuccessAction */](data);
                    }
                    else {
                        return new __WEBPACK_IMPORTED_MODULE_4__restaurant_actions__["e" /* LoadRestaurantFailureAction */]();
                    }
                }, function (err) { return new __WEBPACK_IMPORTED_MODULE_4__restaurant_actions__["e" /* LoadRestaurantFailureAction */](); });
            },
            onError: function (action, e) {
                // can add error handling here but for now just return null
                return new __WEBPACK_IMPORTED_MODULE_4__restaurant_actions__["e" /* LoadRestaurantFailureAction */]();
            }
        });
        this.loadNearby$ = this.d.fetch(__WEBPACK_IMPORTED_MODULE_4__restaurant_actions__["k" /* RestaurantActionTypes */].LOAD_NEARBY_RESTAURANTS, {
            run: function (action, state) {
                return _this.restaurantService.getNearBy(action.payload.radius, action.payload.latitude, action.payload.longitude, action.payload.idsInMem)
                    .map(function (data) { return new __WEBPACK_IMPORTED_MODULE_4__restaurant_actions__["c" /* LoadNearByRestaurantsSuccessAction */](data); }, function (err) { return new __WEBPACK_IMPORTED_MODULE_4__restaurant_actions__["b" /* LoadNearByRestaurantsFailureAction */](); });
            },
            onError: function (action, e) {
                // can add error handling here but for now just return null
                return new __WEBPACK_IMPORTED_MODULE_4__restaurant_actions__["b" /* LoadNearByRestaurantsFailureAction */]();
            }
        });
        this.loadInBounds$ = this.d.fetch(__WEBPACK_IMPORTED_MODULE_4__restaurant_actions__["k" /* RestaurantActionTypes */].LOAD_RESTAURANTS_IN_BOUNDS, {
            run: function (action, state) {
                return _this.restaurantService.getInBounds(action.payload)
                    .map(function (data) { return new __WEBPACK_IMPORTED_MODULE_4__restaurant_actions__["i" /* LoadRestaurantsInBoundsSuccessAction */](data); }, function (err) { return new __WEBPACK_IMPORTED_MODULE_4__restaurant_actions__["h" /* LoadRestaurantsInBoundsFailureAction */](); });
            },
            onError: function (action, e) {
                // can add error handling here but for now just return null
                return new __WEBPACK_IMPORTED_MODULE_4__restaurant_actions__["h" /* LoadRestaurantsInBoundsFailureAction */]();
            }
        });
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], RestaurantEffects.prototype, "loadRestaurant$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], RestaurantEffects.prototype, "loadNearby$", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], RestaurantEffects.prototype, "loadInBounds$", void 0);
    RestaurantEffects = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["a" /* Actions */],
            __WEBPACK_IMPORTED_MODULE_5__core_services_restaurant_service__["a" /* RestaurantService */],
            __WEBPACK_IMPORTED_MODULE_2__nrwl_nx__["a" /* DataPersistence */]])
    ], RestaurantEffects);
    return RestaurantEffects;
}());

//# sourceMappingURL=restaurant.effects.js.map

/***/ }),

/***/ 480:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocationEffects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_effects__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__nrwl_nx__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__location_actions__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_location_service__ = __webpack_require__(127);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LocationEffects = /** @class */ (function () {
    function LocationEffects(
    // private actions$: Actions,
    locationService, d) {
        var _this = this;
        this.locationService = locationService;
        this.d = d;
        this.saveLocationSucess$ = this.d.fetch(__WEBPACK_IMPORTED_MODULE_3__location_actions__["a" /* LocationActionTypes */].SAVE_LOCATION, {
            run: function (action, state) {
                return _this.locationService.updateRestaurantsDistanes(action.payload);
            },
            onError: function (action, e) {
                // can add error handling here but for now just return null
                return null;
            }
        });
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ngrx_effects__["b" /* Effect */])(),
        __metadata("design:type", Object)
    ], LocationEffects.prototype, "saveLocationSucess$", void 0);
    LocationEffects = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__core_services_location_service__["a" /* LocationService */],
            __WEBPACK_IMPORTED_MODULE_2__nrwl_nx__["a" /* DataPersistence */]])
    ], LocationEffects);
    return LocationEffects;
}());

//# sourceMappingURL=location.effects.js.map

/***/ }),

/***/ 481:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BrowseEffects; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__nrwl_nx__ = __webpack_require__(66);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BrowseEffects = /** @class */ (function () {
    /* @Effect() saveLocationSucess$ = this.d.fetch(LocationActionTypes.SAVE_LOCATION, {
        run: (action: SaveLocationAction, state: ApplicationState) => {
            return this.locationService.updateRestaurantsDistanes();
        },
        onError: (action: SaveLocationAction, e: any) => {
            // can add error handling here but for now just return null
            return null;
        }
    }); */
    function BrowseEffects(d) {
        this.d = d;
    }
    BrowseEffects = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__nrwl_nx__["a" /* DataPersistence */]])
    ], BrowseEffects);
    return BrowseEffects;
}());

//# sourceMappingURL=browse.effects.js.map

/***/ }),

/***/ 482:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CoreModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_restaurant_service__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_trending_service__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_location_service__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_specials_service__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pipes_browse_sort_pipe__ = __webpack_require__(483);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pipes_distance_pipe__ = __webpack_require__(484);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


/* Services */






var providers = [
    __WEBPACK_IMPORTED_MODULE_3__services_trending_service__["a" /* TrendingService */],
    __WEBPACK_IMPORTED_MODULE_2__services_restaurant_service__["a" /* RestaurantService */],
    __WEBPACK_IMPORTED_MODULE_4__services_location_service__["a" /* LocationService */],
    __WEBPACK_IMPORTED_MODULE_5__services_specials_service__["a" /* SpecialsService */],
];
var CoreModule = /** @class */ (function () {
    function CoreModule(parentModule) {
        if (parentModule) {
            throw new Error(parentModule + " has already been loaded. Import Core Module into the AppModule only");
        }
    }
    CoreModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpClientModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__pipes_browse_sort_pipe__["a" /* BrowseSortPipe */],
                __WEBPACK_IMPORTED_MODULE_7__pipes_distance_pipe__["a" /* DistancePipe */]
            ],
            providers: providers,
            exports: [
                __WEBPACK_IMPORTED_MODULE_6__pipes_browse_sort_pipe__["a" /* BrowseSortPipe */],
                __WEBPACK_IMPORTED_MODULE_7__pipes_distance_pipe__["a" /* DistancePipe */]
            ]
        }),
        __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Optional */])()), __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* SkipSelf */])()),
        __metadata("design:paramtypes", [CoreModule])
    ], CoreModule);
    return CoreModule;
}());

//# sourceMappingURL=core.module.js.map

/***/ }),

/***/ 483:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BrowseSortPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/*
 * Sort the items on the browse page
 * Takes in an array of restaurants and sorts them based on the browse options attributes.
*/
var BrowseSortPipe = /** @class */ (function () {
    function BrowseSortPipe() {
    }
    BrowseSortPipe.prototype.transform = function (restaurants, sortBy, distance, searchTerm) {
        var temp = new Array();
        var specials = new Array();
        var nonSpecials = new Array();
        try {
            /*
             * The incoming 'restaurants' is immutable, so we need to make a copy and then modify that and
             * pass that back.
             */
            restaurants.forEach(function (r) {
                temp.push(r);
            });
            /*
             * For ease of use, lowercase everything for comparison.
             */
            var st = searchTerm.toLowerCase();
            /*
             * Loop through each restaurant and see if the search word is contained in either its:
             * 1) name
             * 2) any of it's tags (which will be a string of space delimited tags)
             */
            temp = temp.filter(function (r) {
                var matchFound = false;
                var searchTerms = searchTerm.split(" ");
                /*
                 * If it's too far away, then nope to this restaurant
                 */
                if (r.distance > distance)
                    return false;
                searchTerms.forEach(function (term) {
                    if (!term) {
                        term = "";
                    }
                    term = term.toString().toLowerCase();
                    if (searchTerms.length == 1 || (term != "" && searchTerms.length > 1)) {
                        var name_1 = r.name.toString().toLowerCase().indexOf(term) > -1;
                        var tag = r.tags.toString().toLowerCase().indexOf(term) > -1;
                        matchFound = name_1 || tag;
                        if (matchFound)
                            return;
                    }
                });
                return matchFound;
            });
            /*
             * Split the list into two, those that have specials to display, and those that don't.
             * We do still want to display the restaurants that don't have specials so that the user knows that that we know
             * about the restaurant, it just doesn't have a special.
             */
            temp.forEach(function (r) {
                if (r.specials.length != 0)
                    specials.push(r);
                else
                    nonSpecials.push(r);
            });
            /*
             * Sort the newly filtered list based on
             */
            switch (sortBy) {
                case 'High To Low': {
                    specials.sort(function (a, b) { return b.expense_rating - a.expense_rating; });
                    nonSpecials.sort(function (a, b) { return b.expense_rating - a.expense_rating; });
                    break;
                }
                case 'Low To High': {
                    specials.sort(function (a, b) { return a.expense_rating - b.expense_rating; });
                    nonSpecials.sort(function (a, b) { return a.expense_rating - b.expense_rating; });
                    break;
                }
                default: {
                    specials.sort(function (a, b) { return a.distance - b.distance; });
                    nonSpecials.sort(function (a, b) { return a.distance - b.distance; });
                }
            }
        }
        catch (err) {
            throw new Error(err);
        }
        finally {
            /*
             * Tack nonSpecials on to the end of specials so that it displays below it.
             */
            return specials.concat(nonSpecials);
        }
    };
    BrowseSortPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Pipe */])({ name: 'browseSort', pure: true })
    ], BrowseSortPipe);
    return BrowseSortPipe;
}());

//# sourceMappingURL=browse-sort.pipe.js.map

/***/ }),

/***/ 484:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DistancePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/*
 * Sort the items on the browse page
 * Takes in an array of restaurants and sorts them based on the browse options attributes.
*/
var DistancePipe = /** @class */ (function () {
    function DistancePipe() {
    }
    DistancePipe.prototype.transform = function (distance) {
        var dist = Math.round(distance * 100) / 100;
        return dist.toFixed(2);
    };
    DistancePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Pipe */])({ name: 'distancePipe', pure: true })
    ], DistancePipe);
    return DistancePipe;
}());

//# sourceMappingURL=distance.pipe.js.map

/***/ }),

/***/ 486:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomHttpInterceptor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_timeout__ = __webpack_require__(241);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_observable_ErrorObservable__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_observable_ErrorObservable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_observable_ErrorObservable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CustomHttpInterceptor = /** @class */ (function () {
    function CustomHttpInterceptor() {
    }
    CustomHttpInterceptor.prototype.intercept = function (req, next) {
        var _this = this;
        console.log("intercepting");
        return next.handle(req)
            .retry(3)
            .catch(function (err) {
            return _this.handleError(err);
        });
    };
    CustomHttpInterceptor.prototype.handleError = function (error) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        }
        else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error("Backend returned code " + error.status + ", " +
                ("body was: " + error.error));
        }
        // return an ErrorObservable with a user-facing error message
        return new __WEBPACK_IMPORTED_MODULE_2_rxjs_observable_ErrorObservable__["ErrorObservable"]('Something bad happened; please try again later.');
    };
    CustomHttpInterceptor = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], CustomHttpInterceptor);
    return CustomHttpInterceptor;
}());

//# sourceMappingURL=http.interceptor.js.map

/***/ }),

/***/ 487:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

// import { NavComponent } from './nav/nav.component';

var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
            //NavComponent
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormsModule */]
            ],
            exports: [
            //NavComponent
            ]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 488:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__assets_images_icons_nav_trending_2_0_svg__ = __webpack_require__(489);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__assets_images_icons_nav_map_svg__ = __webpack_require__(490);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__assets_images_icons_nav_store_svg__ = __webpack_require__(491);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_analytics__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(ga, navCtrl, platform, navParams, sanitizer, elRef, events) {
        var _this = this;
        this.ga = ga;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.navParams = navParams;
        this.sanitizer = sanitizer;
        this.elRef = elRef;
        this.events = events;
        this.hideSearchBar = true;
        this.searchTerm = "";
        this.activePageName = "";
        /* this.platform.ready().then(() => {
  
          this.ga.startTrackerWithId(googleAnalyticsTrackingId)
          .then(() => { */
        if (this.platform.is("android")) {
            this.browseSvg = this.sanitizer.bypassSecurityTrustHtml(__WEBPACK_IMPORTED_MODULE_5__assets_images_icons_nav_store_svg__["a" /* store */]);
            this.trendingSvg = this.sanitizer.bypassSecurityTrustHtml(__WEBPACK_IMPORTED_MODULE_3__assets_images_icons_nav_trending_2_0_svg__["a" /* trending */]);
            this.mapSvg = this.sanitizer.bypassSecurityTrustHtml(__WEBPACK_IMPORTED_MODULE_4__assets_images_icons_nav_map_svg__["a" /* map */]);
        }
        else {
            this.browseSvg = this.sanitizer.bypassSecurityTrustHtml(__WEBPACK_IMPORTED_MODULE_5__assets_images_icons_nav_store_svg__["a" /* store */]);
            this.trendingSvg = this.sanitizer.bypassSecurityTrustHtml(__WEBPACK_IMPORTED_MODULE_3__assets_images_icons_nav_trending_2_0_svg__["a" /* trending */]);
            this.mapSvg = this.sanitizer.bypassSecurityTrustHtml(__WEBPACK_IMPORTED_MODULE_4__assets_images_icons_nav_map_svg__["a" /* map */]);
        }
        this.events.subscribe('search:term', function (searchTerm) {
            _this.searchTerm = searchTerm;
            if (_this.searchTerm == "")
                _this.hideSearchBar = true;
        });
        /* })
      }); */
    }
    HeaderComponent.prototype.ngOnInit = function () {
        this.activePageName = this.navCtrl.getActive().name;
        this.hideSearchBar = true;
    };
    HeaderComponent.prototype.onSearchInput = function () {
        this.events.publish("search:navigate", this.searchTerm);
    };
    HeaderComponent.prototype.setMapRoot = function () {
        this.events.publish("nav:click", "map");
    };
    HeaderComponent.prototype.setTrendingRoot = function () {
        this.events.publish("nav:click", "trending");
    };
    HeaderComponent.prototype.setBrowseRoot = function () {
        this.events.publish("nav:click", "browse");
    };
    HeaderComponent.prototype.titleClicked = function () {
        this.events.publish("nav:click", "trending");
    };
    HeaderComponent.prototype.onSearch = function () {
        this.hideSearchBar = false;
        if (this.platform.is("android")) {
            this.searchbar.setFocus();
        }
    };
    HeaderComponent.prototype.onSearchCancel = function () {
        this.hideSearchBar = true;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])("searchbar"),
        __metadata("design:type", Object)
    ], HeaderComponent.prototype, "searchbar", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])("mapTab"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], HeaderComponent.prototype, "mapTab", void 0);
    HeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: "f-header",template:/*ion-inline-start:"C:\Users\Pidgeon\Desktop\Codes\src\app\pages\header\header.component.html"*/'<ion-title [ngClass]="{\'hidden\': !hideSearchBar}" (click)="titleClicked()" tappable>\n\n  Fuego\n\n</ion-title>\n\n<div [ngClass]="{\'hidden\': !hideSearchBar}" class="nav-icons">\n\n  <a (click)="setMapRoot()" class="nav-icon map">\n\n    <div class="svg-wrapper" [innerHtml]="mapSvg" [ngClass]="{\'active\': activePageName == \'MapPage\'}" tappable></div>\n\n    <span [ngClass]="{\'active\': activePageName == \'MapPage\'}" class="nav-title" tappable>Map</span>\n\n  </a>\n\n  <a (click)="setTrendingRoot()" class="nav-icon trending">\n\n    <div class="svg-wrapper" [innerHtml]="trendingSvg" [ngClass]="{\'active\': activePageName == \'TrendingPage\'}" tappable></div>\n\n    <span [ngClass]="{\'active\': activePageName == \'TrendingPage\'}" class="nav-title" tappable>Trending</span>\n\n  </a>\n\n  <a (click)="setBrowseRoot()" class="nav-icon browse">\n\n    <div class="svg-wrapper" [innerHtml]="browseSvg" [ngClass]="{\'active\': activePageName == \'BrowsePage\'}" tappable></div>\n\n    <span [ngClass]="{\'active\': activePageName == \'BrowsePage\'}" class="nav-title" tappable>Near By</span>\n\n  </a>\n\n  <a (click)="onSearch()" [ngClass]="{\'hidden\': activePageName == \'SpecialsPage\'}" class="nav-icon search">\n\n    <div class="svg-wrapper"></div>\n\n  </a>\n\n</div>\n\n<ion-searchbar #searchbar [ngClass]="{\'zerowidth\': hideSearchBar}" placeholder="Search For Specials" [(ngModel)]="searchTerm" \n\n  showCancelButton="true" (search)="onSearchInput($event)" (ionCancel)="onSearchCancel()">\n\n</ion-searchbar>\n\n'/*ion-inline-end:"C:\Users\Pidgeon\Desktop\Codes\src\app\pages\header\header.component.html"*/,
            styleUrls: ["/app/pages/header/header.component.scss"]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_analytics__["a" /* GoogleAnalytics */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Events */]])
    ], HeaderComponent);
    return HeaderComponent;
}());

//# sourceMappingURL=header.component.js.map

/***/ }),

/***/ 489:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return trending; });
var trending = "<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"viewBox=\"0 0 100 100\" style=\"enable-background:new 0 0 100 100;\" xml:space=\"preserve\"><style type=\"text/css\">.st0{fill:none;stroke-width:4;stroke-linecap:round;stroke-miterlimit:10;}</style><g id=\"Layer_1_1_\"><g><g id=\"Layer_9_2_\"><line class=\"st0\" x1=\"78.4\" y1=\"76.4\" x2=\"18.5\" y2=\"76.4\"/><path class=\"st0\" d=\"M18.5,76.4c0,4.3,2.1,7.9,4.7,7.9\"/><path class=\"st0\" d=\"M78.4,76.4c0,4.3-2.1,7.9-4.7,7.9\"/><line class=\"st0\" x1=\"23.5\" y1=\"84.3\" x2=\"73.5\" y2=\"84.3\"/></g><g id=\"Layer_4_3_\"><line class=\"st0\" x1=\"19.1\" y1=\"58.2\" x2=\"77.8\" y2=\"58.2\"/><path class=\"st0\" d=\"M19.1,58.2c5.2-10.9,17.2-18,30.3-17.6c12.4,0.3,23.6,7.2,28.5,17.6\"/></g><line class=\"st0\" x1=\"16.7\" y1=\"67.4\" x2=\"81.3\" y2=\"67.4\"/></g></g><g id=\"Layer_2\"><path class=\"st0\" d=\"M25.6,35.9c0,0-10.7-8.6,4.5-16.5c0,0-1.3,6.5,6.4,5c6.2-1.2,4-7.5,2.9-9.9c-0.5-1-0.6-2.1-0.2-3c0.7-1.7,3.9-3.8,9.9-4.2c0,0-5.2,4.5,2.5,6.5c11.4,2.9,15.9,13.1,9.6,16.6c0,0,4.2,0.2,6.8-3.4s1.7-6.2,1.7-6.2s11.7,6.6,0.3,16.6\"/></g></svg>";
//# sourceMappingURL=trending_2.0.svg.js.map

/***/ }),

/***/ 490:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return map; });
var map = "<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"viewBox=\"0 0 100 100\" style=\"enable-background:new 0 0 100 100;\" xml:space=\"preserve\"><style type=\"text/css\">.st0{fill:none;stroke-width:5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}.st1{fill:none;stroke-width:5;stroke-linecap:round;stroke-miterlimit:10;}.st2{}</style><g><g><path class=\"st0\" d=\"M58,53.9l-9.1,21.6L58,53.9z\"/><path class=\"st1\" d=\"M59.5,60l4.9,2.1c1,0.4,2.3-0.1,2.7-1.1l10.7-25.4c1.2-2.8-0.1-6.2-3-7.4l-1.6-0.7c-1.9-0.8-4.2,0.1-5,2L57.5,55C56.7,57,57.6,59.2,59.5,60z\"/></g><path class=\"st0\" d=\"M8.2,90.3c-0.3,0.1-0.6-0.1-0.6-0.5V18.5c0-0.3,0.2-0.6,0.6-0.7V90.3z\"/><g><path class=\"st0\" d=\"M36.2,49.8L48.6,76L36.2,49.8z\"/><path class=\"st2\" d=\"M36.5,29.1l-3.6,1.7l4.2,8.8c0.4,0.8,0,1.8-0.8,2.2s-1.8,0-2.2-0.8l-4.2-8.8L26.2,34l4.2,8.8c0.4,0.8,0,1.8-0.8,2.2l0,0c-0.8,0.4-1.8,0-2.2-0.8l-4.2-8.8L19.8,37c5.6,11.9,7.2,17.6,16.5,13.1C46.2,45.5,42.2,40.9,36.5,29.1z\"/><ellipse transform=\"matrix(0.9035 -0.4287 0.4287 0.9035 -13.4326 12.7489)\" class=\"st2\" cx=\"21.6\" cy=\"36.2\" rx=\"2\" ry=\"1.9\"/><ellipse transform=\"matrix(0.9035 -0.4287 0.4287 0.9035 -11.4655 15.2777)\" class=\"st2\" cx=\"28.2\" cy=\"33.1\" rx=\"2.1\" ry=\"2\"/><ellipse transform=\"matrix(0.9954 -9.569609e-02 9.569609e-02 0.9954 -2.7113 3.4587)\" class=\"st2\" cx=\"34.7\" cy=\"30\" rx=\"2\" ry=\"1.9\"/></g><g><path class=\"st0\" d=\"M91.6,80.7V9.2c0.3-0.1,0.6,0.1,0.6,0.5V80C92.1,80.3,91.9,80.6,91.6,80.7z\"/><line class=\"st0\" x1=\"7.7\" y1=\"17.8\" x2=\"35.8\" y2=\"9.1\"/><line class=\"st0\" x1=\"60.1\" y1=\"17.8\" x2=\"35.8\" y2=\"9.1\"/><line class=\"st0\" x1=\"7.7\" y1=\"90.5\" x2=\"35.8\" y2=\"81.8\"/><line class=\"st0\" x1=\"60.1\" y1=\"90.5\" x2=\"35.8\" y2=\"81.8\"/><line class=\"st0\" x1=\"60.1\" y1=\"17.8\" x2=\"92.1\" y2=\"9.1\"/><line class=\"st0\" x1=\"60.1\" y1=\"90.5\" x2=\"91.1\" y2=\"80.8\"/></g></g></svg>";
//# sourceMappingURL=map.svg.js.map

/***/ }),

/***/ 491:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return store; });
var store = "<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"viewBox=\"0 0 100 100\" style=\"enable-background:new 0 0 100 100;\" xml:space=\"preserve\"><style type=\"text/css\">.st0{fill:none;stroke-width:4;stroke-linecap:round;stroke-miterlimit:10;}.st1{fill:none;stroke-width:5;stroke-miterlimit:10;}</style><path class=\"st0\" d=\"M87,89H14c-1.1,0-2-0.9-2-2V13c0-1.1,0.9-2,2-2h73c1.1,0,2,0.9,2,2v74C89,88.1,88.1,89,87,89z\"/><path class=\"st1\" d=\"M76.5,69.5h-23c-1.7,0-3-1.4-3-3V45c0-1.7,1.4-3,3-3h23c1.7,0,3,1.4,3,3v21.4C79.5,68.1,78.1,69.5,76.5,69.5z\"/><path class=\"st1\" d=\"M36.5,88.5h-12c-1.7,0-3-1.4-3-3v-41c0-1.7,1.4-3,3-3h12c1.7,0,3,1.4,3,3v41C39.5,87.1,38.1,88.5,36.5,88.5z\"/><path class=\"st1\" d=\"M21.5,30.5L21.5,30.5c-5,0-9-4-9-9v-7c0-1.6,1.4-3,3-3h13c1.7,0,3,1.3,3,3v6C31.5,26,27,30.5,21.5,30.5z\"/><path class=\"st1\" d=\"M40.5,30.5L40.5,30.5c-5,0-9-4-9-9v-7c0-1.6,1.3-3,3-3h13c1.7,0,3,1.3,3,3v6C50.5,26,46,30.5,40.5,30.5z\"/><path class=\"st1\" d=\"M59.5,30.5L59.5,30.5c-5,0-9-4-9-9v-7c0-1.6,1.3-3,3-3h13c1.7,0,3,1.3,3,3v6C69.5,26,65,30.5,59.5,30.5z\"/><path class=\"st1\" d=\"M78.5,30.5L78.5,30.5c-5,0-9-4-9-9v-7c0-1.6,1.3-3,3-3h13c1.7,0,3,1.3,3,3v6C88.5,26,84,30.5,78.5,30.5z\"/></svg>";
//# sourceMappingURL=store.svg.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestaurantPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__state_restaurant_restaurant_facade__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_call_number__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_analytics__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__core_objects_google_analytics_tracking_id__ = __webpack_require__(63);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var RestaurantPage = /** @class */ (function () {
    function RestaurantPage(ga, platform, navParams, navCtrl, viewCtrl, restaurantFacade, callNumber) {
        var _this = this;
        this.ga = ga;
        this.platform = platform;
        this.navParams = navParams;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.restaurantFacade = restaurantFacade;
        this.callNumber = callNumber;
        this.ga.startTrackerWithId(__WEBPACK_IMPORTED_MODULE_6__core_objects_google_analytics_tracking_id__["a" /* googleAnalyticsTrackingId */])
            .then(function () {
            console.log('Google analytics is ready now');
            _this.ga.trackView("Trending Page");
            // Tracker is ready
            // You can now track pages or set additional information such as AppVersion or UserId
        })
            .catch(function (e) { return console.log('Error starting GoogleAnalytics', e); });
        var restaurantId = this.navParams.get('restaurant_id');
        this.restaurant$ = this.restaurantFacade.
            checkForRestaurant(restaurantId).
            map(function (restaurants) {
            return restaurants.filter(function (r) { return r.id === restaurantId; })[0];
        });
    }
    RestaurantPage.prototype.ionViewWillLeave = function () {
        this.viewCtrl.dismiss();
    };
    Object.defineProperty(RestaurantPage.prototype, "loading", {
        get: function () {
            return this.restaurantFacade.getLoading();
        },
        enumerable: true,
        configurable: true
    });
    RestaurantPage.prototype.openMenuFile = function () {
        var _this = this;
        /*
         * Log the View Menu button click with GA and then bring up the menu image.
         */
        this.ga.trackEvent('Restaurant', 'button click', 'Menu', this.r.id)
            .then(function () {
            _this.restaurant$.map(function (r) {
                window.open(r.menu_file_url, '_system', 'location=yes');
            });
        });
    };
    RestaurantPage.prototype.dirs = function () {
        var _this = this;
        /*
        * Log the Get Directions to Restaurant button click with GA and then go to Maps or Google Maps for directions
        */
        this.ga.trackEvent('Restaurant', 'intent', 'Directions')
            .then(function () {
            _this.restaurant$.map(function (r) {
                var fullAddress = r.address + " " + r.address2 + " " + r.city + " " + r.state + ", " + r.zip;
                window.open("geo:0,0?q=" + fullAddress, "_system");
            });
        });
    };
    RestaurantPage.prototype.tele = function (phone) {
        var _this = this;
        this.ga.trackEvent('Restaurant', 'intent', 'Phone')
            .then(function () {
            _this.callNumber.callNumber(phone, true)
                .then(function () { return console.log('Launched dialer!'); })
                .catch(function () { return console.log('Error launching dialer'); });
        });
    };
    RestaurantPage.prototype.site = function (id, website) {
        this.ga.trackEvent('Restaurant', 'intent', 'Website', id)
            .then(function () {
            window.open(website, '_system', 'location=yes');
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('searchbar'),
        __metadata("design:type", Object)
    ], RestaurantPage.prototype, "searchbar", void 0);
    RestaurantPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-restaurant',template:/*ion-inline-start:"C:\Users\Pidgeon\Desktop\Codes\src\app\pages\restaurant\restaurant.html"*/'<ion-header no-border>\n\n  <ion-navbar color="offWhite">\n\n      <f-header></f-header>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content class="outer-content">\n\n  <!-- <ng-container *ngIf="restaurant$ | async; let r; else loadingResaurant"> -->\n\n    <ion-card class="restaurant-name-card">\n\n      <ng-container *ngIf="restaurant$ | async; let r; else loadimage">\n\n        <ng-container *ngIf="r.name.indexOf(\'Timeout\') == -1; else imagetimeout">\n\n          <div class="restaurant-header" [ngStyle]="{\'background-image\': \'url(\' + (r.banner_image != \'\'  ? r.banner_image : \'assets/images/banners/default.jpg\') + \')\'}">\n\n            <div class="restaurant-name">{{r.name}}</div>\n\n            <div class="restaurant-desc">{{r.description}}</div>\n\n          </div>\n\n        </ng-container>\n\n      </ng-container>\n\n      <ng-template #loadimage>\n\n        <div class="restaurant-header" style="background-image: url(\'assets/images/banners/default.jpg\')">\n\n          <div class="restaurant-name">Loading...</div>\n\n        </div>\n\n      </ng-template>\n\n      <ng-template #imagetimeout>\n\n        <div class="restaurant-header" style="background-image: url(\'assets/images/banners/default.jpg\')">\n\n          <div class="restaurant-name"></div>\n\n        </div>\n\n      </ng-template>\n\n      <div class="action-buttons item item-input-inset">\n\n        <a href="#" (click)="dirs()" class="">\n\n          <ion-icon class="btn-icon" name="navigate"></ion-icon>\n\n          <span class="tab-text">Directions</span>\n\n        </a>\n\n        <a href="#" (click)="tele(r.phone)" class="">\n\n          <ion-icon class="btn-icon" name="call"></ion-icon>\n\n          <span class="tab-text">Call</span>\n\n        </a>\n\n        <a href="#" (click)="openMenuFile()" class="">\n\n          <ion-icon class="btn-icon" name="list-box"></ion-icon>\n\n          <span class="tab-text">Menu</span>\n\n        </a>\n\n        <a href="#" (click)="site(r.id, r.website)" class="">\n\n            <ion-icon class="btn-icon" name="browsers"></ion-icon>\n\n            <span class="tab-text">Website</span>\n\n        </a>\n\n      </div>\n\n      <div class="menus">\n\n        <ng-container *ngIf="restaurant$ | async; let r; else noitems">\n\n          <ng-container *ngIf="r.name.indexOf(\'Timeout\') == -1; else itemstimout">\n\n            <ng-container *ngIf="r.menus && r.menus.length > 0; else noitems">\n\n              <ion-item-group *ngFor="let menu of r.menus; let i = index;">\n\n                <div class="list-header">\n\n                    <span>\n\n                        {{menu.title}}\n\n                    </span>\n\n                </div>\n\n                <ng-container *ngFor="let item of menu.items; let odd=odd">\n\n                  <ion-item>\n\n                    <div class="special">\n\n                      <div class="special-content">\n\n                        <div class="item-container">\n\n                          <span class="item-name">\n\n                              {{item.title}}\n\n                          </span>\n\n                        </div>                    \n\n                        <span class="middot">&#8226;</span>\n\n                        <span class="price">\n\n                            {{item.price}}\n\n                        </span>\n\n                      </div>\n\n                    </div>\n\n                  </ion-item>\n\n                </ng-container>\n\n              </ion-item-group>\n\n            </ng-container>\n\n          </ng-container>\n\n        </ng-container>\n\n        <ng-template #noitems>\n\n          <div class="no-specials-container">\n\n            <span>No Specials</span>\n\n            <span>To Display</span>\n\n          </div>\n\n        </ng-template>\n\n        <ng-template #itemstimout>\n\n          <div class="no-results-container">\n\n            <span>We had an issue</span>\n\n            <span>hacking the mainframe</span>\n\n            <span>Please try again</span>\n\n            <ion-icon name="sad"></ion-icon>\n\n          </div>\n\n        </ng-template>\n\n      </div>\n\n    </ion-card>\n\n  <!-- </ng-container> -->\n\n  <ng-template #loadingResaurant>\n\n    <ng-container *ngIf="loading | async; let loading; else noresults">\n\n      <ng-container *ngIf="loading">\n\n          <ion-spinner  name="dots"></ion-spinner>\n\n      </ng-container>\n\n    </ng-container>\n\n    <ng-template #noresults>\n\n      <div class="no-results-container">\n\n          <span>We had an issue</span>\n\n          <span>hacking the mainframe</span>\n\n          <ion-icon name="sad"></ion-icon>\n\n      </div>\n\n    </ng-template>\n\n  </ng-template>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Pidgeon\Desktop\Codes\src\app\pages\restaurant\restaurant.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_native_google_analytics__["a" /* GoogleAnalytics */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__state_restaurant_restaurant_facade__["a" /* RestaurantFacade */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_call_number__["a" /* CallNumber */]])
    ], RestaurantPage);
    return RestaurantPage;
}());

//# sourceMappingURL=restaurant.js.map

/***/ }),

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "k", function() { return RestaurantActionTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return LoadRestaurantAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return LoadRestaurantSuccessAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return LoadRestaurantFailureAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadNearByRestaurantsAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return LoadNearByRestaurantsSuccessAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return LoadNearByRestaurantsFailureAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "j", function() { return ResaveRestaurantsSuccessAction; });
/* unused harmony export ResaveRestaurantsFailureAction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return LoadRestaurantsInBoundsAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return LoadRestaurantsInBoundsSuccessAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return LoadRestaurantsInBoundsFailureAction; });
var RestaurantActionTypes;
(function (RestaurantActionTypes) {
    RestaurantActionTypes["LOAD_RESTAURANT"] = "[RESTAURANT] Load Restaurant Action";
    RestaurantActionTypes["LOAD_RESTUARANT_SUCCESS"] = "[RESTAURANT] Load Restaurant Success";
    RestaurantActionTypes["LOAD_RESTAURANT_FAILURE"] = "[RESTAURANT] Load Restaurant Failure";
    RestaurantActionTypes["LOAD_NEARBY_RESTAURANTS"] = "[RESTAURANT] Load NearBy Restuarants Action";
    RestaurantActionTypes["LOAD_NEARBY_RESTAURANTS_SUCCESS"] = "[RESTAURANT] Load NearBy Restuarants Success";
    RestaurantActionTypes["LOAD_NEARBY_RESTAURANTS_FAILURE"] = "[RESTAURANT] Load NearBy Restuarants Failure";
    RestaurantActionTypes["RESAVE_RESTAURANTS_SUCCESS"] = "[RESTAURANT] Resave Restuarants Success";
    RestaurantActionTypes["RESAVE_RESTAURANTS_FAILURE"] = "[RESTAURANT] Resave Restuarants Failure";
    RestaurantActionTypes["LOAD_RESTAURANTS_IN_BOUNDS"] = "[RESTAURANT] Load In Bounds Restuarants Action";
    RestaurantActionTypes["LOAD_RESTAURANTS_IN_BOUNDS_SUCCESS"] = "[RESTAURANT] Load In Bounds Restuarants Success";
    RestaurantActionTypes["LOAD_RESTAURANTS_IN_BOUNDS_FAILURE"] = "[RESTAURANT] Load In Bounds Restuarants Failure";
})(RestaurantActionTypes || (RestaurantActionTypes = {}));
var LoadRestaurantAction = /** @class */ (function () {
    function LoadRestaurantAction(payload) {
        this.payload = payload;
        this.type = RestaurantActionTypes.LOAD_RESTAURANT;
    }
    return LoadRestaurantAction;
}());

var LoadRestaurantSuccessAction = /** @class */ (function () {
    function LoadRestaurantSuccessAction(payload) {
        this.payload = payload;
        this.type = RestaurantActionTypes.LOAD_RESTUARANT_SUCCESS;
    }
    return LoadRestaurantSuccessAction;
}());

var LoadRestaurantFailureAction = /** @class */ (function () {
    function LoadRestaurantFailureAction() {
        this.type = RestaurantActionTypes.LOAD_RESTAURANT_FAILURE;
    }
    return LoadRestaurantFailureAction;
}());

var LoadNearByRestaurantsAction = /** @class */ (function () {
    function LoadNearByRestaurantsAction(payload) {
        this.payload = payload;
        this.type = RestaurantActionTypes.LOAD_NEARBY_RESTAURANTS;
    }
    return LoadNearByRestaurantsAction;
}());

var LoadNearByRestaurantsSuccessAction = /** @class */ (function () {
    function LoadNearByRestaurantsSuccessAction(payload) {
        this.payload = payload;
        this.type = RestaurantActionTypes.LOAD_NEARBY_RESTAURANTS_SUCCESS;
    }
    return LoadNearByRestaurantsSuccessAction;
}());

var LoadNearByRestaurantsFailureAction = /** @class */ (function () {
    function LoadNearByRestaurantsFailureAction() {
        this.type = RestaurantActionTypes.LOAD_NEARBY_RESTAURANTS_FAILURE;
    }
    return LoadNearByRestaurantsFailureAction;
}());

var ResaveRestaurantsSuccessAction = /** @class */ (function () {
    function ResaveRestaurantsSuccessAction(payload) {
        this.payload = payload;
        this.type = RestaurantActionTypes.RESAVE_RESTAURANTS_SUCCESS;
    }
    return ResaveRestaurantsSuccessAction;
}());

var ResaveRestaurantsFailureAction = /** @class */ (function () {
    function ResaveRestaurantsFailureAction() {
        this.type = RestaurantActionTypes.RESAVE_RESTAURANTS_FAILURE;
    }
    return ResaveRestaurantsFailureAction;
}());

var LoadRestaurantsInBoundsAction = /** @class */ (function () {
    function LoadRestaurantsInBoundsAction(payload) {
        this.payload = payload;
        this.type = RestaurantActionTypes.LOAD_RESTAURANTS_IN_BOUNDS;
    }
    return LoadRestaurantsInBoundsAction;
}());

var LoadRestaurantsInBoundsSuccessAction = /** @class */ (function () {
    function LoadRestaurantsInBoundsSuccessAction(payload) {
        this.payload = payload;
        this.type = RestaurantActionTypes.LOAD_RESTAURANTS_IN_BOUNDS_SUCCESS;
    }
    return LoadRestaurantsInBoundsSuccessAction;
}());

var LoadRestaurantsInBoundsFailureAction = /** @class */ (function () {
    function LoadRestaurantsInBoundsFailureAction() {
        this.type = RestaurantActionTypes.LOAD_RESTAURANTS_IN_BOUNDS_FAILURE;
    }
    return LoadRestaurantsInBoundsFailureAction;
}());

//# sourceMappingURL=restaurant.actions.js.map

/***/ }),

/***/ 61:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export RESTAURANT_INITIAL_STATE */
/* harmony export (immutable) */ __webpack_exports__["b"] = restaurantReducer;
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return restaurantQueries; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__restaurant_actions__ = __webpack_require__(60);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};

var RESTAURANT_INITIAL_STATE = {
    restaurants: new Array(),
    loading: false
};
function restaurantReducer(state, action) {
    if (state === void 0) { state = RESTAURANT_INITIAL_STATE; }
    switch (action.type) {
        case __WEBPACK_IMPORTED_MODULE_0__restaurant_actions__["k" /* RestaurantActionTypes */].LOAD_RESTUARANT_SUCCESS: {
            var temp_1 = new Map();
            state.restaurants.forEach(function (r) {
                temp_1.set(r.id, r);
            });
            temp_1.set(action.payload.id, action.payload);
            return __assign({}, state, { restaurants: Array.from(temp_1.values()), loading: false });
        }
        case __WEBPACK_IMPORTED_MODULE_0__restaurant_actions__["k" /* RestaurantActionTypes */].LOAD_RESTAURANTS_IN_BOUNDS_SUCCESS:
        case __WEBPACK_IMPORTED_MODULE_0__restaurant_actions__["k" /* RestaurantActionTypes */].LOAD_NEARBY_RESTAURANTS_SUCCESS: {
            var temp_2 = new Map();
            /*
             * Convert the restsaurants to a map with the `id` being the key.
             */
            state.restaurants.forEach(function (r) {
                temp_2.set(r.id, r);
            });
            /*
             * For each restaurant returned in the response, add it to the map. Maps inherintly overwrite objects that share the same key
             * with the new object that is being brought in. Thus allowing us to update any new attributes of the restaurant since the last
             * time that it was loaded, such as the distance (which is sent to us by the server).
             */
            action.payload.forEach(function (r) {
                temp_2.set(r.id, r);
            });
            var newRestaurantsArray = Array.from(temp_2.values());
            return __assign({}, state, { restaurants: newRestaurantsArray, loading: false });
        }
        case __WEBPACK_IMPORTED_MODULE_0__restaurant_actions__["k" /* RestaurantActionTypes */].RESAVE_RESTAURANTS_SUCCESS: {
            /*
             * Do the same thing as LOAD_NEARBY_RESTAURANTS_SUCCESS, except don't set `loading` back to false
             */
            var temp_3 = new Map();
            state.restaurants.forEach(function (r) {
                temp_3.set(r.id, r);
            });
            action.payload.forEach(function (r) {
                temp_3.set(r.id, r);
            });
            var newRestaurantsArray = Array.from(temp_3.values());
            return __assign({}, state, { restaurants: newRestaurantsArray });
        }
        case __WEBPACK_IMPORTED_MODULE_0__restaurant_actions__["k" /* RestaurantActionTypes */].LOAD_RESTAURANT: {
            return __assign({}, state, { loading: true });
        }
        case __WEBPACK_IMPORTED_MODULE_0__restaurant_actions__["k" /* RestaurantActionTypes */].LOAD_NEARBY_RESTAURANTS: {
            return __assign({}, state, { loading: true });
        }
        case __WEBPACK_IMPORTED_MODULE_0__restaurant_actions__["k" /* RestaurantActionTypes */].LOAD_RESTAURANT_FAILURE: {
            return __assign({}, state, { loading: false });
        }
        case __WEBPACK_IMPORTED_MODULE_0__restaurant_actions__["k" /* RestaurantActionTypes */].LOAD_NEARBY_RESTAURANTS_FAILURE: {
            return __assign({}, state, { loading: false });
        }
        case __WEBPACK_IMPORTED_MODULE_0__restaurant_actions__["k" /* RestaurantActionTypes */].LOAD_RESTAURANTS_IN_BOUNDS_FAILURE: {
            return __assign({}, state, { loading: false });
        }
        default: {
            return state;
        }
    }
}
var restaurantQueries;
(function (restaurantQueries) {
    restaurantQueries.getRestuarants = function (state) { return state.restaurantState.restaurants; };
    restaurantQueries.loading = function (state) { return state.restaurantState.loading; };
})(restaurantQueries || (restaurantQueries = {}));
//# sourceMappingURL=restaurant.reducer.js.map

/***/ }),

/***/ 63:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return googleAnalyticsTrackingId; });
var googleAnalyticsTrackingId = 'UA-68685781-3';
//# sourceMappingURL=google-analytics-tracking-id.js.map

/***/ }),

/***/ 75:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestaurantFacade; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__restaurant_actions__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__restaurant_reducer__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__location_location_facade__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__location_location_reducer__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__browse_browse_facade__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_services_restaurant_service__ = __webpack_require__(79);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var RestaurantFacade = /** @class */ (function () {
    function RestaurantFacade(store, locationFacade, browseFacade, restaurantService) {
        this.store = store;
        this.locationFacade = locationFacade;
        this.browseFacade = browseFacade;
        this.restaurantService = restaurantService;
        this.restaurants$ = this.store.select(__WEBPACK_IMPORTED_MODULE_3__restaurant_reducer__["a" /* restaurantQueries */].getRestuarants);
        this.currentLocation$ = this.store.select(__WEBPACK_IMPORTED_MODULE_5__location_location_reducer__["a" /* locationQueries */].getCurrentLocation);
        this.loading$ = this.store.select(__WEBPACK_IMPORTED_MODULE_3__restaurant_reducer__["a" /* restaurantQueries */].loading);
    }
    RestaurantFacade.prototype.checkForRestaurant = function (id) {
        var rest;
        /*
         * Check to see if the restuarant exists
         */
        this.restaurants$.subscribe(function (restaurants) {
            rest = restaurants.filter(function (r) { return r.id === id; })[0];
        }).unsubscribe();
        /*
         * If it doesn't go fetch it.
         */
        if (!(!!rest))
            this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__restaurant_actions__["d" /* LoadRestaurantAction */](id));
        /*
         * Return the full restaurant array so that if the restaurant didn't exist in memory, then it can be
         * asynchronously loaded when the restaurant comes back from the network call.
         */
        return this.restaurants$;
    };
    RestaurantFacade.prototype.loadNearBy = function (loc) {
        this.locationFacade.loadLocation(loc);
        return this.restaurants$.map(function (restaurants) {
            return restaurants;
        });
    };
    RestaurantFacade.prototype.loadInBounds = function (bounds) {
        this.store.dispatch(new __WEBPACK_IMPORTED_MODULE_2__restaurant_actions__["g" /* LoadRestaurantsInBoundsAction */](bounds));
        var restaurantsInRadius = new Array();
        return this.restaurants$.map(function (restaurants) {
            return restaurants;
        });
    };
    RestaurantFacade.prototype.getBrowseCards = function () {
        var restaurantsInRadius = new Array();
        // return this.restaurants$.map((data: Map<number, Restaurant>) => {
        return this.restaurantService.getNearBy(null, null, null, null).map(function (data) {
            console.log("returning full array: ", data);
            return data;
        });
    };
    RestaurantFacade.prototype.refresh = function (loc) {
        this.locationFacade.refreshLocation(loc);
        var restaurantsInRadius = new Array();
        return this.restaurants$.map(function (restaurants) {
            return restaurants;
        });
    };
    RestaurantFacade.prototype.getLoading = function () {
        return this.loading$;
    };
    RestaurantFacade = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["h" /* Store */],
            __WEBPACK_IMPORTED_MODULE_4__location_location_facade__["a" /* LocationFacade */],
            __WEBPACK_IMPORTED_MODULE_6__browse_browse_facade__["a" /* BrowseFacade */],
            __WEBPACK_IMPORTED_MODULE_7__core_services_restaurant_service__["a" /* RestaurantService */]])
    ], RestaurantFacade);
    return RestaurantFacade;
}());

//# sourceMappingURL=restaurant.facade.js.map

/***/ }),

/***/ 77:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export LOCATION_INITIAL_STATE */
/* harmony export (immutable) */ __webpack_exports__["b"] = locationReducer;
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return locationQueries; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__location_actions__ = __webpack_require__(126);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};

var LOCATION_INITIAL_STATE = {
    currentLocation: null
};
function locationReducer(state, action) {
    if (state === void 0) { state = LOCATION_INITIAL_STATE; }
    switch (action.type) {
        case __WEBPACK_IMPORTED_MODULE_0__location_actions__["a" /* LocationActionTypes */].SAVE_LOCATION: {
            return __assign({}, state, { currentLocation: action.payload });
        }
        default: {
            return state;
        }
    }
}
var locationQueries;
(function (locationQueries) {
    locationQueries.getCurrentLocation = function (state) { return state.locationState.currentLocation; };
})(locationQueries || (locationQueries = {}));
//# sourceMappingURL=location.reducer.js.map

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestaurantService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_of__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_of__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_delay__ = __webpack_require__(352);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_delay___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_delay__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngrx_store__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__state_restaurant_restaurant_reducer__ = __webpack_require__(61);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var RestaurantService = /** @class */ (function () {
    function RestaurantService(http, store) {
        this.http = http;
        this.store = store;
        this.restuarants$ = this.store.select(__WEBPACK_IMPORTED_MODULE_6__state_restaurant_restaurant_reducer__["a" /* restaurantQueries */].getRestuarants);
        this.apiUrl = "https://beta.fuegospecials.com/api";
    }
    RestaurantService.prototype.getById = function (id) {
        // return Observable.of(restaurants.filter(r => r.id === id)[0]).delay(2000);
        try {
            return this.http.get(this.apiUrl + "/restaurant/" + id)
                .timeout(3000)
                .retry(3)
                .catch(function (err) {
                var timeout = err;
                timeout.id = id;
                return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].of(timeout);
            });
        }
        catch (err) {
            throw new Error();
        }
    };
    RestaurantService.prototype.getNearBy = function (radius, latitude, longitude, idsInMem) {
        /* let temp = restaurants.filter((r) => { return idsInMem.indexOf(r.id) == -1 })
        return Observable.of(temp); */
        var exemptIds = new Array();
        this.restuarants$.subscribe(function (restaurants) {
            restaurants.forEach(function (r) {
                exemptIds.push(r.id);
            });
        });
        var body = {
            "latitude": latitude,
            "longitude": longitude,
            "radius": Number(radius),
            "exempt_ids": exemptIds,
            "start": 0
        };
        try {
            return this.http.post(this.apiUrl + "/restaurants/radius", JSON.stringify(body))
                .timeout(3000)
                .retry(3);
        }
        catch (err) {
            throw new Error();
        }
    };
    RestaurantService.prototype.getInBounds = function (bounds) {
        /* let temp = restaurants.filter((r) => {
            return (
                (r.latitude < bounds.northeast.lat && r.latitude > bounds.southwest.lat)
                &&
                (r.longitude < bounds.northeast.lng && r.longitude > bounds.southwest.lng)
            )}
        );
        temp.forEach(r => {
            console.log(`returning: ${r.name}`)
        })

        return Observable.of(temp); */
        /* let exemptIds = new Array<number>();
        this.restuarants$.subscribe(restaurants => {getInBounds(bounds: VisibleRegion) {
        let temp = restaurants.filter((r) => {
            return (
                (r.latitude < bounds.northeast.lat && r.latitude > bounds.southwest.lat)
                &&
                (r.longitude < bounds.northeast.lng && r.longitude > bounds.southwest.lng)
            )}
        );
        temp.forEach(r => {
            console.log(`returning: ${r.name}`)
        })

        return Observable.of(temp); */
        var exemptIds = new Array();
        this.restuarants$.subscribe(function (restaurants) {
            restaurants.forEach(function (r) {
                exemptIds.push(r.id);
            });
        });
        var body = {
            "latitude": 35.227190,
            "longitude": -80.843097,
            "top_right": {
                "latitude": bounds.northeast.lat,
                "longitude": bounds.northeast.lng
            },
            "bottom_left": {
                "latitude": bounds.southwest.lat,
                "longitude": bounds.southwest.lng
            },
            "exempt_ids": exemptIds,
            "start": 0
        };
        try {
            return this.http.post(this.apiUrl + "/restaurants/bounds", JSON.stringify(body))
                .timeout(3000)
                .retry(3);
        }
        catch (err) {
            throw new Error();
        }
    };
    RestaurantService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_5__ngrx_store__["h" /* Store */]])
    ], RestaurantService);
    return RestaurantService;
}());

//# sourceMappingURL=restaurant.service.js.map

/***/ }),

/***/ 83:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return splash; });
var splash = "<svg class=\"logo\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" viewBox=\"0 0 250 250\" style=\"enable-background:new 0 0 250 250;\" xml:space=\"preserve\"><g><g id=\"Layer_9_2_\"><line class=\"burger\" x1=\"230.1\" y1=\"184.5\" x2=\"18\" y2=\"184.5\"></line><path class=\"burger\" d=\"M18,184.5c0,13.9,7.3,25.2,16.5,25.2\"></path><path class=\"burger\" d=\"M230.1,184.5c0,13.9-7.3,25.2-16.5,25.2\"></path><line class=\"burger\" x1=\"35.9\" y1=\"209.7\" x2=\"212.6\" y2=\"209.7\"></line></g><g id=\"Layer_6_2_\"><g><path class=\"burger\" d=\"M25.6,123c16.6,0,16.6,10.8,33.2,10.8S75.4,123,92,123s16.6,10.8,33.2,10.8s16.6-10.8,33.2-10.8s16.6,10.8,33.2,10.8s16.6-10.8,33.2-10.8\"></path></g></g><g id=\"Layer_5_2_\"><path class=\"burger\" d=\"M223.4,169.8H24.7c-6,0-11-4.9-11-11l0,0c0-6.1,5-11,11-11h198.7c6,0,11,4.9,11,11l0,0C234.4,164.9,229.6,169.8,223.4,169.8z\"></path></g><g id=\"Layer_4_3_\"><line class=\"burger\" x1=\"20.2\" y1=\"110.2\" x2=\"228.1\" y2=\"110.2\"></line><path class=\"burger\" d=\"M20.2,110.2C38.4,67,81.1,39.3,127.3,40.6c43.9,1.2,83.5,28.5,100.7,69.6\"></path></g></g><line class=\"burger\" x1=\"86.5\" y1=\"67.2\" x2=\"99\" y2=\"63.9\"></line><line class=\"burger\" x1=\"141.2\" y1=\"62.5\" x2=\"153.4\" y2=\"58.1\"></line><line class=\"burger\" x1=\"109.7\" y1=\"54.1\" x2=\"122.7\" y2=\"54.1\"></line><line class=\"burger\" x1=\"121\" y1=\"67.6\" x2=\"133.6\" y2=\"70.5\"></line><path class=\"taco\" d=\"M12.9,165.4c11.8-60.5,70.5-99.9,130.9-88.1s99.9,70.5,88.1,130.9C232,208.5,12.9,165.7,12.9,165.4z\"></path><path class=\"taco\" d=\"M23.9,109.9c-1.1,5.4,2.1,10.7,7,12.6c4-5.7,8.4-11,13.4-15.9c-0.1-0.2-0.2-0.3-0.3-0.5c0.2,0.1,0.4,0.1,0.6,0.2c25.3-24.8,62-37,99.5-29.8c40.8,8,72,37.1,84.3,74c4.1-1,7.5-4.2,8.3-8.7c1.2-6-2.7-11.7-8.6-13c0.8-1.3,1.3-2.8,1.6-4.3c1-5.2-0.8-10.2-4.2-13.6c1.3-2.1,2.2-4.3,2.7-6.8c2.2-11-5-21.7-16-23.8c-4.8-1-9.4-0.1-13.3,1.9c-2.4-3-5.8-5.2-9.9-6c-3.2-0.6-6.5-0.3-9.3,0.9c0.4-11.2-7.3-21.2-18.5-23.4c-7.4-1.4-14.5,0.8-19.7,5.4c-3.1-8.3-10.4-14.8-19.7-16.7c-11.9-2.4-23.5,3.8-28.7,14.1c-1.7-0.8-3.5-1.4-5.4-1.7c-11.5-2.3-22.6,4.2-26.5,14.8c-1.9-1.2-4.2-2.2-6.7-2.6c-11-2.2-21.7,5-23.8,16c-1.3,6.8,1,13.5,5.4,18.1C30.4,100.2,25,104.1,23.9,109.9z\"></path><line class=\"taco\" x1=\"126.6\" y1=\"116.9\" x2=\"68.2\" y2=\"147.5\"></line><line class=\"taco\" x1=\"188.6\" y1=\"132.1\" x2=\"130\" y2=\"163\"></line><path class=\"sushi\" d=\"M220.7,84.9c-10.1-2.4-19.5-1.6-24.8,1.6C180.1,76.1,147.3,69,109.3,69C56,69,12.7,83.1,12.7,100.4s43.2,31.4,96.6,31.4c38.9,0,72.3-7.5,87.6-18.2c1.8,2.8,4,5.6,6.8,8.3c10.3,10.3,22.9,14.4,28.1,9.3c3.7-3.7,2.7-11.2-1.9-18.9c7.2-0.7,12.6-3.6,13.7-8C245.2,97,235,88.3,220.7,84.9z\"></path><line class=\"sushi\" x1=\"44.2\" y1=\"77.2\" x2=\"74.3\" y2=\"129.6\"></line><line class=\"sushi\" x1=\"158.8\" y1=\"73.5\" x2=\"184.4\" y2=\"120\"></line><line class=\"sushi\" x1=\"96.9\" y1=\"69.4\" x2=\"131.2\" y2=\"129.4\"></line><path class=\"sushi\" d=\"M210.2,135.9c0-4.9-1.8-9.3-4.5-11.8V124c-0.6-0.8-1.3-1.5-2.1-2.2c-2.7-2.7-4.9-5.5-6.8-8.3c-15.4,10.7-48.8,18.2-87.6,18.2c-44.9,0-82.6-10-93.4-23.4c-0.4,0.9-0.7,1.9-1.1,2.8c-5.3,0.2-9.6,5.9-9.6,12.9c0,5.2,2.3,9.6,5.7,11.7c-1.4,3.6-1.5,8.2,0.2,12.7c2.3,5.9,7,9.9,11.8,10.7c-0.2,1-0.4,2.1-0.4,3.2c0,6.8,4.7,12.3,10.6,12.3c2.8,0,5.3-1.2,7.2-3.3c0.8,4.7,4.8,8.2,9.7,8.2c3.8,0,7.2-2.2,8.8-5.4c2.1,4,8.2,6.9,15.4,6.9c7.1,0,13.2-2.8,15.4-6.7c3.3,3.2,9.1,5.3,15.7,5.3c6,0,11.4-1.8,14.8-4.5c2.8,3.5,8.8,6,15.7,6c8.1,0,14.9-3.3,16.9-7.9c2.9,2.1,7.1,3.4,11.9,3.4c6.1,0,11.3-2.2,13.9-5.3c1.6,1.2,3.6,1.9,5.8,1.9c3.7,0,6.9-2,8.6-5c1.4,1,2.9,1.6,4.6,1.6c5.3,0,9.7-5.8,9.7-12.9c0-3.2-0.9-6.2-2.4-8.4C208,146.1,210.2,141.4,210.2,135.9z\"></path><circle class=\"donut\" cx=\"126.8\" cy=\"125\" r=\"112.3\"></circle><circle class=\"donut\" cx=\"126.8\" cy=\"125\" r=\"33.8\"></circle><g><path class=\"donut\" d=\"M228.7,125c0,10.5-10.2,19.3-13.3,28.7c-3.2,9.8-0.2,22.9-6.1,31.1c-6,8.3-19.5,9.3-27.7,15.4c-8.2,6-13.4,18.4-23.2,21.6c-9.5,3.1-21-3.9-31.4-3.9s-21.9,7-31.4,3.9c-9.8-3.2-15-15.7-23.2-21.6c-8.3-6-21.7-7.1-27.7-15.4c-6-8.2-2.9-21.2-6.1-31.1c-3.1-9.5-13.3-18.2-13.3-28.7s10.2-19.3,13.3-28.7c3.2-9.8,0.2-22.9,6.1-31.1c6-8.3,19.5-9.3,27.7-15.4c8.2-6,13.4-18.4,23.2-21.6c9.5-3.1,21,3.9,31.4,3.9s21.9-7,31.4-3.9c9.8,3.2,15,15.7,23.2,21.6c8.3,6,21.7,7.1,27.7,15.4c6,8.2,2.9,21.2,6.1,31.1C218.3,105.8,228.7,114.5,228.7,125z\"></path></g><path class=\"donut\" d=\"M169.6,37.5c-12.9,1.2-80.4,47.6-129.1,42.8\"></path><path class=\"donut\" d=\"M80.2,206.6c12.9-1.2,82.4-47.6,131.1-42.8\"></path><path class=\"donut\" d=\"M160.6,125c0,0,17.7,0,31.7-6c15-6.4,23.1-22.7,23.1-22.7\"></path><path class=\"donut\" d=\"M92.3,125c0,0-17.7,0-31.7,6c-15,6.4-22.1,22.7-22.1,22.7\"></path><line class=\"donut\" x1=\"171.9\" y1=\"70.3\" x2=\"180.9\" y2=\"79.7\"></line><line class=\"donut\" x1=\"75.6\" y1=\"172.7\" x2=\"84.9\" y2=\"163.7\"></line><line class=\"donut\" x1=\"97.6\" y1=\"56.3\" x2=\"97.8\" y2=\"43.4\"></line><line class=\"donut\" x1=\"148.6\" y1=\"190.3\" x2=\"157.6\" y2=\"199.7\"></line><line class=\"donut\" x1=\"54.6\" y1=\"105.2\" x2=\"67.2\" y2=\"102.3\"></line><line class=\"donut\" x1=\"176.1\" y1=\"149.5\" x2=\"189.1\" y2=\"148.5\"></line></svg>";
//# sourceMappingURL=splash.svg.js.map

/***/ }),

/***/ 90:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpecialsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_restaurant_service__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_services_specials_service__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__restaurant_restaurant__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Observable__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_Subscription__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_Subscription___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_Subscription__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__assets_svgs_splash_svg__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_google_analytics__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__core_jsons_charlotte_location__ = __webpack_require__(139);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












/**
 * Generated class for the SpecialsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SpecialsPage = /** @class */ (function () {
    function SpecialsPage(ga, platform, navCtrl, navParams, restaurantService, specialsService, sanitizer, viewCtrl, geolocation, menuCtrl, events) {
        /* this.ga.startTrackerWithId(googleAnalyticsTrackingId)
        .then(() => {
          console.log('Google analytics is ready now');
              this.ga.trackView("Specials Page");
              // Tracker is ready
              // You can now track pages or set additional information such as AppVersion or UserId
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e)); */
        var _this = this;
        this.ga = ga;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.restaurantService = restaurantService;
        this.specialsService = specialsService;
        this.sanitizer = sanitizer;
        this.viewCtrl = viewCtrl;
        this.geolocation = geolocation;
        this.menuCtrl = menuCtrl;
        this.events = events;
        /*
         * Init variables
         */
        this.searchTerm = "";
        this.distance = 5;
        this.groupBy = 'category';
        this.hideGroupByBtn = true;
        this.hideDistanceBtn = true;
        this.searchSub = new __WEBPACK_IMPORTED_MODULE_7_rxjs_Subscription__["Subscription"]();
        this.loadingSvg = this.sanitizer.bypassSecurityTrustHtml(__WEBPACK_IMPORTED_MODULE_8__assets_svgs_splash_svg__["a" /* splash */]);
        events.subscribe('search:term', function (searchTerm) {
            _this.searchTerm = searchTerm;
            _this.searchedFor = searchTerm;
            _this.search();
        });
    }
    SpecialsPage.prototype.ionViewWillEnter = function () {
        /*
         * Enable correct menu
         */
        this.menuCtrl.enable(true, 'specialsmenu');
        this.menuCtrl.enable(false, 'browsemenu');
        this.menuCtrl.enable(false, 'mapmenu');
    };
    SpecialsPage.prototype.ionViewDidEnter = function () {
        /* this.searchTerm = this.navParams.get('search_term') != undefined ? this.navParams.get('search_term') : "";
        console.log(this.navParams.data); */
    };
    SpecialsPage.prototype.ionViewWillLeave = function () {
        this.menuCtrl.close();
        this.viewCtrl.dismiss();
    };
    SpecialsPage.prototype.restaurant = function (id) {
        var _this = this;
        // Going to the Restaurant Page, and logging it with GA.
        this.ga.trackEvent('Specials', 'page-load', 'Restaurant', id)
            .then(function () {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__restaurant_restaurant__["a" /* RestaurantPage */], { "restaurant_id": id });
        });
    };
    SpecialsPage.prototype.changeDistance = function (event) {
        var _this = this;
        this.hideDistanceBtn = false;
        this.ga.trackEvent('Specials', 'filter-change', 'distance', this.distance)
            .then(function () {
            if (_this.searchTerm != "") {
                _this.search();
            }
        });
    };
    SpecialsPage.prototype.changeGroupBy = function (event) {
        var _this = this;
        this.hideGroupByBtn = false;
        this.ga.trackEvent('Specials', 'filter-change', 'distance', this.distance)
            .then(function () {
            if (_this.searchTerm != "") {
                _this.search();
            }
        });
    };
    SpecialsPage.prototype.search = function () {
        var _this = this;
        if (this.searchTerm == "") {
            this.specialGroupsList = __WEBPACK_IMPORTED_MODULE_6_rxjs_Observable__["Observable"].of(new Array());
        }
        else {
            this.ga.trackEvent('Specials', 'search', this.searchTerm)
                .then(function () {
                _this.searchedFor = _this.searchTerm;
                _this.geolocation.getCurrentPosition().then(function (loc) {
                    _this.specialGroupsList = _this.specialsService.getCategorizedSpecials(loc.coords.latitude, loc.coords.longitude, _this.searchTerm, _this.distance, _this.groupBy);
                }).catch(function (err) {
                    _this.specialGroupsList = _this.specialsService.getCategorizedSpecials(__WEBPACK_IMPORTED_MODULE_11__core_jsons_charlotte_location__["a" /* tradeAndTryonLocation */].coords.latitude, __WEBPACK_IMPORTED_MODULE_11__core_jsons_charlotte_location__["a" /* tradeAndTryonLocation */].coords.longitude, _this.searchTerm, _this.distance, _this.groupBy);
                });
            });
        }
    };
    SpecialsPage.prototype.disableDistanceFilterBtn = function () {
        this.hideDistanceBtn = true;
        this.distance = 5;
    };
    SpecialsPage.prototype.disableGroupByFilterBtn = function () {
        this.hideGroupByBtn = true;
        this.groupBy = "category";
    };
    SpecialsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-specials',template:/*ion-inline-start:"C:\Users\Pidgeon\Desktop\Codes\src\app\pages\specials\specials.html"*/'<ion-menu class="right-menu" side="right" [content]="specialscontent" type="overlay" id="specialsmenu">\n\n  <ion-header>\n\n    <ion-navbar>\n\n      <ion-title>Filters</ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  <ion-content class="outer-content">\n\n    <ion-list>\n\n      <ion-list-header>\n\n          Distance\n\n      </ion-list-header>\n\n      <ion-list radio-group [(ngModel)]="distance" (ngModelChange)="changeDistance(event$)">\n\n        <ion-item>\n\n          <ion-label>1 mi</ion-label>\n\n          <ion-radio value="1"></ion-radio>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label>5 mi</ion-label>\n\n          <ion-radio value="5"></ion-radio>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label>10 mi</ion-label>\n\n          <ion-radio value="10"></ion-radio>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label>20 mi</ion-label>\n\n          <ion-radio value="20"></ion-radio>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label>30 mi</ion-label>\n\n          <ion-radio value="30"></ion-radio>\n\n        </ion-item>\n\n      </ion-list>\n\n      <ion-item></ion-item>\n\n      <ion-list-header>\n\n        Group By\n\n      </ion-list-header>\n\n      <ion-list radio-group [(ngModel)]="groupBy" (ngModelChange)="changeGroupBy(event$)">\n\n          <ion-item>\n\n            <ion-label>Category</ion-label>\n\n            <ion-radio value="category"></ion-radio>\n\n          </ion-item>\n\n          <ion-item>\n\n            <ion-label>Restaurant</ion-label>\n\n            <ion-radio value="restaurant"></ion-radio>\n\n          </ion-item>\n\n          <ion-item>\n\n            <ion-label>Special</ion-label>\n\n            <ion-radio value="special"></ion-radio>\n\n          </ion-item>\n\n        </ion-list>\n\n    </ion-list>\n\n  </ion-content>\n\n</ion-menu>\n\n\n\n<ion-header no-border>\n\n    <ion-navbar color="offWhite">\n\n        <f-header></f-header>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content  class="outer-content" #specialscontent>\n\n  <div class="options-container">\n\n    <ion-searchbar class="search-bar" [(ngModel)]="searchTerm" (search)="search()"></ion-searchbar>\n\n    <a class="options" menuToggle href="#" [ngClass]="{\'hide\': searchTerm.length != 0 }">\n\n      <ion-icon class="options" name="options"></ion-icon>\n\n    </a>\n\n  </div>\n\n  <div class="filters-list" [ngClass]="{\'hide\': hideGroupByBtn && hideDistanceBtn }">\n\n    <div class="filters-list-title-container">\n\n      <span class="filters-list-title">Filters: </span>\n\n    </div>\n\n    <button class="filter-btn filter-distance-btn" (click)="disableDistanceFilterBtn()" \n\n    [ngClass]="{\'hide\': hideDistanceBtn }" ion-button round outline>\n\n      {{distance}} mi \n\n      <button class="filter-btn-close" ion-button round>x</button>\n\n    </button>\n\n    <button class="filter-btn filter-group-by-btn" (click)="disableGroupByFilterBtn()" \n\n    [ngClass]="{\'hide\': hideGroupByBtn }" ion-button round outline>\n\n      {{groupBy}} \n\n      <button class="filter-btn-close" ion-button round>x</button>\n\n    </button>\n\n  </div>\n\n  <ng-container *ngIf="specialGroupsList | async; let list; else loading">\n\n    <!-- If we have have results, print them, otherwise go to #noresults -->\n\n    <ng-container *ngIf="list.length > 0; else noresults">\n\n      <!-- If we have a 502 or 404 or whatever, the it will return as a JSON that we handle here -->\n\n      <ng-container *ngIf="list.length == 1 && list[0].specials.length == 0; else specialslist">\n\n        <ng-container *ngIf="list[0].title == \'502\'">\n\n          <!-- If we have a 502 handle it here -->\n\n          <div class="start-searching-container">\n\n            <span>Out Of Service ... :/</span>\n\n          </div>\n\n        </ng-container>\n\n        <!-- If we have a 404 handle it here -->\n\n        <ng-container *ngIf="list[0].title == \'404\'">\n\n            <div class="start-searching-container">\n\n              <span>404, Not Found ... :/</span>\n\n            </div>\n\n          </ng-container>\n\n      </ng-container>\n\n      <ng-template #specialslist>\n\n        <ng-container *ngFor="let group of list; let oddSection = odd">\n\n          <ion-card class="list-header">\n\n              <ion-item class="section-header light-item" [ngClass]="{\'light-category-header\': !oddSection}">\n\n                  <span>\n\n                      {{group.title}}\n\n                  </span>\n\n              </ion-item>\n\n          </ion-card>\n\n          <ion-card class="list">\n\n              <ng-container *ngFor="let item of group.specials; let odd = odd">\n\n                  <ion-item (click)="toGoRestaurant(item.restaurant_id)" [ngClass]="{\'light-item\': !odd}">\n\n                      <span class="item-name">\n\n                          {{item.title}}\n\n                      </span>\n\n                      <span class="price">\n\n                          {{item.price}}\n\n                      </span>\n\n                  </ion-item>\n\n              </ng-container>\n\n          </ion-card>\n\n        </ng-container>\n\n      </ng-template>\n\n    </ng-container>\n\n    <ng-template #noresults>\n\n      <ng-container *ngIf="!init; else startsearching">\n\n        <div class="start-searching-container">\n\n            <span>No results for: "{{searchedFor}}"</span>\n\n        </div>\n\n      </ng-container>\n\n    </ng-template>\n\n  </ng-container>\n\n  <ng-template #loading>\n\n    <ng-container *ngIf="!init; else startsearching">\n\n      <div class="loading-svg" [innerHtml]="loadingSvg" ></div>\n\n    </ng-container>\n\n  </ng-template>\n\n  <!-- This is the message for on initial page load -->\n\n  <ng-template #startsearching>\n\n    <div class="start-searching-container">\n\n        <span>Happy Searching</span>\n\n      <ion-icon name="happy"></ion-icon>\n\n    </div>\n\n  </ng-template>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\Pidgeon\Desktop\Codes\src\app\pages\specials\specials.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_10__ionic_native_google_analytics__["a" /* GoogleAnalytics */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__core_services_restaurant_service__["a" /* RestaurantService */],
            __WEBPACK_IMPORTED_MODULE_4__core_services_specials_service__["a" /* SpecialsService */],
            __WEBPACK_IMPORTED_MODULE_9__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Events */]])
    ], SpecialsPage);
    return SpecialsPage;
}());

//# sourceMappingURL=specials.js.map

/***/ })

},[312]);
//# sourceMappingURL=main.js.map