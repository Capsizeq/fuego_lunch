export class ListItem {
    restaurant_name: string;
    restaurant_id: number;
    menu_item_name: string;
    price: string;
}