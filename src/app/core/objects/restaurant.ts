import { Menu } from "./menu";
import { Special } from "./special";

export class Restaurant {
    id: number;
    name: string;
    description: string;
    tags: string;
    franchise_id: number;
    address: string;
    address2: string;
    city: string;
    state: string;
    zip: number;
    phone: string;
    expense_rating: number;
    logo_image: string;
    banner_image: string;
    pin_image: string;
    latitude: number;
    longitude: number;
    distance: number;
    website: string;
    menu_file_url: string;
    menus: Array<Menu>;
    specials: Array<Special>;
}