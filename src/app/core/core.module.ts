import { NgModule, SkipSelf, Optional } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MockHttpInterceptor } from '../core/http/mock-http.interceptor';

/* Services */
import { RestaurantService } from "./services/restaurant.service";
import { TrendingService } from "./services/trending.service";
import { LocationService } from "./services/location.service";
import { SpecialsService } from "./services/specials.service";
import { BrowseSortPipe } from "./pipes/browse-sort.pipe";
import { DistancePipe } from "./pipes/distance.pipe";

const providers: any[] = [
    TrendingService,
    RestaurantService,
    LocationService,
    SpecialsService,
];

@NgModule({
    imports: [
        HttpClientModule
    ],
    declarations: [
        BrowseSortPipe,
        DistancePipe
    ],
    providers: providers,
    exports: [
        BrowseSortPipe,
        DistancePipe
    ]
})
export class CoreModule {

    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
        if(parentModule) {
            throw new Error(`${parentModule} has already been loaded. Import Core Module into the AppModule only`);
        }
    }
}