import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';

import { ApplicationState } from '../app.state';
import { Store } from '@ngrx/store';

@Injectable()
export class BrowseEffects {

    /* @Effect() saveLocationSucess$ = this.d.fetch(LocationActionTypes.SAVE_LOCATION, {
        run: (action: SaveLocationAction, state: ApplicationState) => {
            return this.locationService.updateRestaurantsDistanes();
        },
        onError: (action: SaveLocationAction, e: any) => {
            // can add error handling here but for now just return null
            return null;
        }
    }); */

    constructor(
        private d: DataPersistence<ApplicationState>
    ) { }
}
