import { CarouselItem } from "./carousel-item";
import { ListItem } from "./list-item";

export class List {
    header: string;
    listItems: Array<ListItem>;
}