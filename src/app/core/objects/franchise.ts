export class Franchise {
    id: number;
    name: string;
    tag: string;
    image: string;
}