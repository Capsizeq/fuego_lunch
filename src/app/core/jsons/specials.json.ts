export const specials = [
    {
      category_name: "Fitzgerald's", 
      specials: 
      [
        {
          id: 0,
          menu_id: 1,
          title: "Specials 1",
          restaurant_id: 0,
          description: "",
          tag: "",
          price: "1.99",
          is_on_special: true
        },{
          id: 0,
          menu_id: 1,
          title: "Specials 2",
          restaurant_id: 0,
          description: "",
          tag: "",
          price: "1.99",
          is_on_special: true
        },{
          id: 0,
          menu_id: 1,
          title: "Specials 3",
          restaurant_id: 0,
          description: "",
          tag: "",
          price: "1.99",
          is_on_special: true
        },
      ]
    },{
      category_name: "Category 2",
      specials: 
      [
        {
          id: 0,
          menu_id: 1,
          title: "Specials 1",
          restaurant_id: 0,
          description: "",
          tag: "",
          price: "1.99",
          is_on_special: true
        },
      ]
    },{
      category_name: "Category 3", 
      specials: 
      [
        {
          id: 0,
          menu_id: 1,
          title: "Specials 1",
          restaurant_id: 4,
          description: "",
          tag: "",
          price: "1.99",
          is_on_special: true
        },{
          id: 0,
          menu_id: 1,
          title: "Specials 2",
          restaurant_id: 2,
          description: "",
          tag: "",
          price: "1.99",
          is_on_special: true
        },
      ]
    },
    {
      category_name: "Fitzgerald's", 
      specials: 
      [
        {
          id: 0,
          menu_id: 1,
          title: "Specials 1",
          restaurant_id: 0,
          description: "",
          tag: "",
          price: "1.99",
          is_on_special: true
        },{
          id: 0,
          menu_id: 1,
          title: "Specials 2",
          restaurant_id: 0,
          description: "",
          tag: "",
          price: "1.99",
          is_on_special: true
        },{
          id: 0,
          menu_id: 1,
          title: "Specials 3",
          restaurant_id: 0,
          description: "",
          tag: "",
          price: "1.99",
          is_on_special: true
        },
      ]
    },{
      category_name: "Category 2",
      specials: 
      [
        {
          id: 0,
          menu_id: 1,
          title: "Specials 1",
          restaurant_id: 0,
          description: "",
          tag: "",
          price: "1.99",
          is_on_special: true
        },
      ]
    },{
      category_name: "Category 3", 
      specials: 
      [
        {
          id: 0,
          menu_id: 1,
          title: "Specials 1",
          restaurant_id: 4,
          description: "",
          tag: "",
          price: "1.99",
          is_on_special: true
        },{
          id: 0,
          menu_id: 1,
          title: "Specials 2",
          restaurant_id: 2,
          description: "",
          tag: "",
          price: "1.99",
          is_on_special: true
        },
      ]
    }
  ];