import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";

import { NavController, NavParams, Events } from "ionic-angular";
import { SpecialsPage } from "../../pages/specials/specials";
import { Platform } from "ionic-angular";
import { DomSanitizer } from "@angular/platform-browser";
import { trending } from "../../../assets/images/icons/nav/trending_2.0.svg";
import { map } from "../../../assets/images/icons/nav/map.svg";
import { store } from "../../../assets/images/icons/nav/store.svg";
import { MapPage } from "../map/map";
import { TrendingPage } from "../trending/trending";
import { BrowsePage } from "../browse/browse";
import { googleAnalyticsTrackingId } from "../../core/objects/google-analytics-tracking-id";
import { GoogleAnalytics } from "@ionic-native/google-analytics";

@Component({
  selector: "f-header",
  templateUrl: "./header.component.html",
  styleUrls: ["/app/pages/header/header.component.scss"]
})
export class HeaderComponent implements OnInit {

  @ViewChild("searchbar") searchbar;
  @ViewChild("mapTab") mapTab: ElementRef;

  private searchTerm: string;
  private mapSvg: any;
  private trendingSvg: any;
  private browseSvg: any;
  private activePageName: string;
  private hideSearchBar: boolean;

  constructor(
    private ga: GoogleAnalytics,
    private navCtrl: NavController,
    private platform: Platform,
    public navParams: NavParams,
    private sanitizer: DomSanitizer,
    private elRef: ElementRef,
    private events: Events) {

      this.hideSearchBar = true;
      this.searchTerm = "";
      this.activePageName = "";

      /* this.platform.ready().then(() => {

        this.ga.startTrackerWithId(googleAnalyticsTrackingId)
        .then(() => { */

          if(this.platform.is("android")) {
            this.browseSvg = this.sanitizer.bypassSecurityTrustHtml(store);
            this.trendingSvg = this.sanitizer.bypassSecurityTrustHtml(trending);
            this.mapSvg = this.sanitizer.bypassSecurityTrustHtml(map);
          } else {
            this.browseSvg = this.sanitizer.bypassSecurityTrustHtml(store);
            this.trendingSvg = this.sanitizer.bypassSecurityTrustHtml(trending);
            this.mapSvg = this.sanitizer.bypassSecurityTrustHtml(map);
          }

          this.events.subscribe('search:term', (searchTerm: string) => {
            this.searchTerm = searchTerm;
            if(this.searchTerm == "")
              this.hideSearchBar = true;
          });
          
        /* })
      }); */
  }

  ngOnInit() {
    this.activePageName = this.navCtrl.getActive().name;
    this.hideSearchBar = true;
  }

  onSearchInput() {
    this.events.publish("search:navigate", this.searchTerm);
  }

  setMapRoot() {
    this.events.publish("nav:click", "map");
  }

  setTrendingRoot() {
    this.events.publish("nav:click", "trending");
  }

  setBrowseRoot() {
    this.events.publish("nav:click", "browse");
  }

  titleClicked() {
    this.events.publish("nav:click", "trending");
  }

  onSearch() {
    this.hideSearchBar = false;
    if(this.platform.is("android")) {
      this.searchbar.setFocus();
    }
  }

  onSearchCancel() {
    this.hideSearchBar = true;
  }
  

}
