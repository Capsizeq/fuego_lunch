import { Injectable } from '@angular/core';
import { Trending } from '../objects/trending/trending';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { trending } from './../jsons/trending/trending.json';
import { trending502 } from './../jsons/trending/trending502.json';
import { trending404 } from './../jsons/trending/trending404.json';
import { trendingEmpty } from './../jsons/trending/trendingEmpty.json';

import 'rxjs/add/operator/switchMap';

@Injectable()
export class TrendingService {
  apiUrl= "https://beta.fuegospecials.com/api";

  constructor(private http: HttpClient) { }

  getTrending(): Observable<Trending> {
    // return Observable.of(trending);

    try {
      return this.http.get<Trending>(this.apiUrl + "/trending")
      .catch((err: HttpErrorResponse) => {
        console.log("error loading trending");
        // simple logging, but you can do a lot more, see below
        console.error('An error occurred:', err.error.contina);
        if(err.error.toString().indexOf('404 Not Found') > -1) {
          console.log("404 loading trending");
          return  Observable.of(trending404);
        } else if(err.error.toString().indexOf('502 Bad Gateway') > -1) {
          console.log("502 loading trending");
          return  Observable.of(trending502);
        } 
        return  Observable.of(trendingEmpty);
      });
    } catch (err) {
      return  Observable.of(trendingEmpty);
    }
  }

  getWeekdays() {
    let weekdays = new Array(7);
    weekdays[0] = "Sunday";
    weekdays[1] = "Monday";
    weekdays[2] = "Tuesday";
    weekdays[3] = "Wednesday";
    weekdays[4] = "Thursday";
    weekdays[5] = "Friday";
    weekdays[6] = "Saturday";

    return weekdays;
  }

}
