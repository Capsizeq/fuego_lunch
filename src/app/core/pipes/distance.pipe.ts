import { Pipe, PipeTransform } from '@angular/core';
/*
 * Sort the items on the browse page
 * Takes in an array of restaurants and sorts them based on the browse options attributes.
*/
@Pipe({name: 'distancePipe', pure: true})
export class DistancePipe implements PipeTransform {
  transform(distance: number): string {
    let dist = Math.round(distance * 100) / 100;
    return dist.toFixed(2);
  }
}
