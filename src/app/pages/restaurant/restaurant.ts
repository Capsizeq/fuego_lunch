import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { RestaurantService } from '../../core/services/restaurant.service';
import { MapPage } from '../map/map';
import { Restaurant } from '../../core/objects/restaurant';
import { RestaurantFacade } from '../../+state/restaurant/restaurant.facade';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { CallNumber } from '@ionic-native/call-number';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { googleAnalyticsTrackingId } from '../../core/objects/google-analytics-tracking-id';
import { SpecialsPage } from '../specials/specials';

@IonicPage()
@Component({
  selector: 'page-restaurant',
  templateUrl: 'restaurant.html',
})
export class RestaurantPage {
  @ViewChild('searchbar') searchbar;
  
  restaurant$: Observable<Restaurant>;
  r: Restaurant;
  headerSearchTerm: string;

  constructor(
    private ga: GoogleAnalytics,
    private platform: Platform,
    public navParams: NavParams,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    private restaurantFacade: RestaurantFacade,
    private callNumber: CallNumber) {

      this.ga.startTrackerWithId(googleAnalyticsTrackingId)
      .then(() => {
        console.log('Google analytics is ready now');
            this.ga.trackView("Trending Page");
            // Tracker is ready
            // You can now track pages or set additional information such as AppVersion or UserId
      })
      .catch(e => console.log('Error starting GoogleAnalytics', e));

      const restaurantId = this.navParams.get('restaurant_id');

      this.restaurant$ = this.restaurantFacade.
      checkForRestaurant(restaurantId).
      map((restaurants) =>
        restaurants.filter(r => r.id === restaurantId)[0]
      );
  }

  ionViewWillLeave() {
    this.viewCtrl.dismiss();
  }

  get loading() {
    return this.restaurantFacade.getLoading();
  }

  openMenuFile() {
    /*
     * Log the View Menu button click with GA and then bring up the menu image.
     */
    this.ga.trackEvent('Restaurant', 'button click', 'Menu', this.r.id)
    .then(() => {
      this.restaurant$.map((r: Restaurant) => {
        window.open(r.menu_file_url, '_system', 'location=yes');
      });
    });
  }

  dirs() {
     /*
     * Log the Get Directions to Restaurant button click with GA and then go to Maps or Google Maps for directions
     */
    this.ga.trackEvent('Restaurant', 'intent', 'Directions')
    .then(() => {
      this.restaurant$.map((r: Restaurant) => {
        const fullAddress = r.address + " " + r.address2 + " " + r.city + " " + r.state + ", " + r.zip;
        window.open("geo:0,0?q="+ fullAddress, "_system");
      });
    });
  }

  tele(phone: string) {
    this.ga.trackEvent('Restaurant', 'intent', 'Phone')
    .then(() => {
      this.callNumber.callNumber(phone, true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
    });
  }

  site(id: number, website: string) {
    this.ga.trackEvent('Restaurant', 'intent', 'Website', id)
    .then(() => {
      window.open(website, '_system', 'location=yes');
    });
  }

}
