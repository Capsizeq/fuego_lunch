import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { RestaurantPage } from '../restaurant/restaurant';
import { RestaurantFacade } from '../../+state/restaurant/restaurant.facade';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationFacade } from '../../+state/location/location.facade';
import { CardInfo } from '../../core/objects/browse/card-info';
import { Platform } from 'ionic-angular';
import { Restaurant } from '../../core/objects/restaurant';
import { BrowseFacade } from '../../+state/browse/browse.facade';
import { MenuItem } from '../../core/objects/menu-item';
import { tradeAndTryonLocation } from '../../core/jsons/charlotte-location';
import { googleAnalyticsTrackingId } from '../../core/objects/google-analytics-tracking-id';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { SpecialsPage } from '../specials/specials';
import { splash } from '../../../assets/svgs/splash.svg';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'page-browse',
  templateUrl: 'browse.html'
})
export class BrowsePage {
  @ViewChild('searchbar') searchbar;

  private restaurants$: Observable<Array<Restaurant>>;
  private loading$: Observable<boolean>;
  private loadingRestaurants: boolean;
  private selectedId: number;
  private noResults: boolean;
  private distance: number;
  private sortBy: string;
  private searchTerm: string;
  private searchTermBinding: string;
  private costLabel: string;
  private distanceLabel: string;
  private nameSearch: boolean;
  private descriptionSearch: boolean;
  private tagSearch: boolean;
  private hideDistanceBtn: boolean;
  private hideSortBtn: boolean;
  private headerSearchTerm: string;
  private loadingSvg: any;

  constructor(
    private ga: GoogleAnalytics,
    public navCtrl: NavController,
    private restaurantFacade: RestaurantFacade,
    private browseFacade: BrowseFacade,
    private platform: Platform,
    private geolocation: Geolocation,
    private sanitizer: DomSanitizer,
    private menuCtrl: MenuController) {

      /*
       * Init varibles
       */
      this.selectedId = -1;
      this.noResults = true;
      this.distance = 5;
      this.sortBy = "Proximity";
      this.searchTerm = "";
      this.nameSearch = true;
      this.descriptionSearch = false;
      this.tagSearch = false;
      this.hideDistanceBtn = true;
      this.hideSortBtn = true;
      this.loadingSvg = this.sanitizer.bypassSecurityTrustHtml(splash);

      this.platform.ready().then(() => {

        this.ga.startTrackerWithId(googleAnalyticsTrackingId)
        .then(() => {
          console.log('Google analytics is ready now');
          this.ga.trackView("Browse Page");
          
          this.loading$ = this.restaurantFacade.loading$; 

          this.geolocation.getCurrentPosition().then((loc) => {
            this.restaurants$ = this.restaurantFacade.loadNearBy(loc);
          }).catch(err => {
            this.restaurants$ = this.restaurantFacade.loadNearBy(tradeAndTryonLocation);
          });
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));

        
      });
  }

  ionViewWillEnter() {
    /*
     * Enable correct menu
     */
    this.menuCtrl.enable(false, 'specialsmenu');
    this.menuCtrl.enable(true, 'browsemenu');
    this.menuCtrl.enable(false, 'mapmenu');
  }

  ionViewWillLeave() {
    this.menuCtrl.close();
  }

  selectCard(id: number) {
    this.ga.trackEvent('Browse', 'card select', 'Restaurant', id)
    .then(() => {
      if(this.selectedId == id) {
        this.selectedId = -1;
      } else {
        this.selectedId = id;
      }
    });
    
  }

  restaurant(id: number) {
    this.ga.trackEvent('Browse', 'page-load', 'Restaurant', id)
    .then(() => {
      this.navCtrl.push(RestaurantPage, {"restaurant_id": id});
    });
  }

  doRefresh(refresher) {
    /* this.ga.trackEvent('Browse', 'refresh', 'Refresher')
    .then(() => {
      this.geolocation.getCurrentPosition().then((loc) => {
        this.restaurants$ = this.restaurantFacade.refresh(loc);
      }).catch(err => {
        this.restaurants$ = this.restaurantFacade.refresh(tradeAndTryonLocation);
      });
      
      this.loading$.subscribe((loading: boolean) => {
        if(!loading)
          refresher.complete();
      });
    }); */
    refresher.complete();
  }

  distanceChange(val: any) {
    this.ga.trackEvent('Browse', 'filter', 'distance', val)
    .then(() => {
      this.hideDistanceBtn = false;

      this.geolocation.getCurrentPosition().then((loc) => {
        this.browseFacade.saveRadius(this.distance, loc);
      }).catch(err => {
        this.browseFacade.saveRadius(this.distance, tradeAndTryonLocation);
      });
    });
  }

  sortByChange(event) {
    this.ga.trackEvent('Browse', 'filter', this.sortBy)
    .then(() => {
      this.hideSortBtn = false;
    });
  }

  disableDistanceFilterBtn() {
    this.hideDistanceBtn = true;
    this.distance = 5;
  }

  disableSortFilterBtn() {
    this.hideSortBtn = true;
    this.sortBy = "Proximity";
  }

  filterItems(event: any) {
    this.ga.trackEvent('Browse', 'search', this.searchTermBinding)
    .then(() => {
      this.searchTerm = this.searchTermBinding;
    });
  }

  onSearchInput() {
    this.navCtrl.push(SpecialsPage, {"search_term": this.headerSearchTerm});
  }

  onSearchSelected() {
    this.searchbar.setFocus();
  }

}
