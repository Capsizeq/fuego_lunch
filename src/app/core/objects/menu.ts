import { MenuItem } from "./menu-item";

export class Menu {
    id: number;
    restaurant_id: number;
    title: string;
    description: string;
    items: Array<MenuItem>;
    special_only: boolean;
    tag: string;
}