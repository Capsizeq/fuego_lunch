import { Injectable } from '@angular/core';
import { ApplicationState } from '../../+state/app.state';
import { Store } from '@ngrx/store';
import { SaveLocationAction } from '../../+state/location/location.actions';
import { LoadNearByRestaurantsSuccessAction, LoadNearByRestaurantsAction, ResaveRestaurantsSuccessAction } from '../../+state/restaurant/restaurant.actions';
import { locationQueries } from '../../+state/location/location.reducer';
import { restaurantQueries } from '../../+state/restaurant/restaurant.reducer';
import { LatLng, Spherical } from '@ionic-native/google-maps';
import { Restaurant } from '../objects/restaurant';
import { conversions } from '../../+state/conversions';
import { browseQueries } from '../../+state/browse/browse.reducer';

import { Geolocation, Geoposition } from '@ionic-native/geolocation';

@Injectable()
export class LocationService {
    currentLocation$ = this.store.select(locationQueries.getCurrentLocation);
    radius$ = this.store.select(browseQueries.getRadius);
    restuarants$ = this.store.select(restaurantQueries.getRestuarants);

    constructor(
        private geolocation: Geolocation,
        private store: Store<ApplicationState>) { }

    updateRestaurantsDistanes(loc: Geoposition) {
        this.restuarants$.subscribe((restaurants) => {
            let tempRestaurants = new Array<Restaurant>();
            let inMemoryNearByRestaurantIdList = new Array<number>();
            /*
            * For each restaurant, update it's distance from the user's phone
            */
            restaurants.forEach(r => {
                const restaurantLatLng = new LatLng(r.latitude, r.longitude);

                const currentLatLng = new LatLng(loc.coords.latitude, loc.coords.longitude);

                /*
                * The computeDistanceBetween function returns a value in meters, so we convert to miles
                */
                const distance = conversions.toWholeMile(Spherical.computeDistanceBetween(restaurantLatLng, currentLatLng));
                tempRestaurants.push({...r, distance: distance});
                inMemoryNearByRestaurantIdList.push(r.id);
            });

            this.radius$.subscribe((radius) => {
                this.store.dispatch(new LoadNearByRestaurantsAction(
                    {"radius": radius, 
                    "latitude": loc.coords.latitude, 
                    "longitude": loc.coords.longitude, 
                    "idsInMem": inMemoryNearByRestaurantIdList})
                );
            }).unsubscribe();

            this.store.dispatch(new ResaveRestaurantsSuccessAction(tempRestaurants));
        }).unsubscribe();
    }

}
