export const trending404 = {
    carouselItems: [
      {
        image: "",
        restaurant_name: "",
        restaurant_id: -1,
        menu_item_name: "",
        price: ""
      }
    ],
    lists: [
      {
        header: "404",
        listItems: [
          {restaurant_name: "", restaurant_id: -1, menu_item_name: "", price: ""}
        ]
      }
    ]
  };
  