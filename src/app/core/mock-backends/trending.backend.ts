import { HttpRequest, HttpResponse, HttpHeaderResponse, HttpEvent } from '@angular/common/http';
import { ApplicationState } from "../../+state/app.state";
import { Store } from "@ngrx/store";
import { Trending } from '../../core/objects/trending/trending';
import { pairs } from '../mock-backend-jsons/trending-pairs.json';
import { Observable } from 'rxjs/Observable';

export function trendingBackend(url: string, method: string, request: HttpRequest<any>/* , store: Store<ApplicationState> */): Observable<HttpEvent<any>> {
    console.log("got to the trending back end");
    
    const _pairs: Trending = JSON.parse(localStorage.getItem('trending-pairs')) || pairs;

    if(url.endsWith("trending")) {
        return new Observable(resp => {
            resp.next(new HttpResponse({
                status: 200,
                body: _pairs
            }));
            resp.complete();
        });
    }
}