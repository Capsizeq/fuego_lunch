export interface Special {
    title: string;
    value: string;
}