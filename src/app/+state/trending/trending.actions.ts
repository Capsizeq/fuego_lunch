import { Action } from '@ngrx/store';
import { TrendingState } from './trending.reducer';
import { Trending } from '../../core/objects/trending/trending';

export enum TrendingActionTypes {
    LOAD_TRENDING = '[TRENDING] Load Trending Action',
    LOAD_TRENDING_SUCCESS = '[TRENDING] Load Trending Success',
    LOAD_TRENDING_FAILURE = '[TRENDING] Load Trending Failure'
}

export class LoadTrendingAction implements Action {
    readonly type = TrendingActionTypes.LOAD_TRENDING;
}

export class LoadTrendingSuccessAction implements Action {
    readonly type = TrendingActionTypes.LOAD_TRENDING_SUCCESS;

    constructor(public payload: Trending) { }
}

export class LoadTrendingFailureAction implements Action {
    readonly type = TrendingActionTypes.LOAD_TRENDING_FAILURE;
}

export type TrendingAction
  = LoadTrendingAction
  | LoadTrendingSuccessAction
  | LoadTrendingFailureAction;
