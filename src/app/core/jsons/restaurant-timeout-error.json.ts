import { Restaurant } from "../objects/restaurant";

export const err: Restaurant = {
    id: -1,
    franchise_id: 0,
    name: "timeout",
    description: "",
    tags: "",
    address: "",
    address2: "",
    city: "",
    state: "",
    zip: 2,
    phone: "",
    expense_rating: 3,
    logo_image: "",
    latitude: 0,
    longitude: 0,
    distance: 1,
    banner_image: "",
    pin_image: "",
    website: "",
    menu_file_url: "",
    menus: [],
    specials: []
  }