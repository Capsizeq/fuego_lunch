import { Injectable } from "@angular/core";
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { ApplicationState } from "../../+state/app.state";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs/Observable";
import { trendingBackend } from '../mock-backends/trending.backend';

@Injectable()
export class MockHttpInterceptor implements HttpInterceptor {
    
    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const url = request.url;
        const method = request.method;

        console.log("intercepting");

        return trendingBackend(url, method, request/* , this.store */) ||
            next.handle(request);
    }

}