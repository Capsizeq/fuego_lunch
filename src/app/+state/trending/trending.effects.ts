import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map';

import { TrendingService } from '../../core/services/trending.service';
import { LoadTrendingAction, LoadTrendingFailureAction, LoadTrendingSuccessAction, TrendingAction, TrendingActionTypes } from './trending.actions';
import { ApplicationState } from '../app.state';
import { TrendingState, TRENDING_INITAL_STATE } from './trending.reducer';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Trending } from '../../core/objects/trending/trending';

@Injectable()
export class TrendingEffects {

    private getAuthStateSuccessMessage = 'Authenication Complete!';
    private getAuthStateErrorMessage = 'Failed to authenticate.';

    @Effect() doAuth$ = this.d.fetch(TrendingActionTypes.LOAD_TRENDING, {
        run: (action: LoadTrendingAction, state: ApplicationState) => {

            let trendingState: TrendingState = TRENDING_INITAL_STATE;

            // Run the back-end initial trending paris grab
            return this.trendingService.getTrending()

            // set the Trending Trending list to the store.
            .map(
                (data: Trending) => new LoadTrendingSuccessAction(data)
            )
        },
        onError: (action: LoadTrendingAction, e: any) => {
            // can add error handling here but for now just return null
            return null;
        }
    });

    constructor(
        private actions$: Actions,
        private trendingService: TrendingService,
        private d: DataPersistence<ApplicationState>,
    ) { }
}
